namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Raycasts from origin towards direction using a layer mask and max distance </summary>
    [NodeInfo("execute", "Raycast", "Physics/Raycast")]
    public class RaycastNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if(runner.DataNullCheck())
            {
                var Data = runner.GetData();

                Vector3 origin = (Vector3)Data[0];
                Vector3 direction = (Vector3)Data[1];
                LayerMask hitMask = (LayerMask)Data[2];
                float distance = (float)Data[3];

                bool hitValid = Physics.Raycast(origin, direction, out RaycastHit hit, distance, hitMask, QueryTriggerInteraction.Ignore);

                foreach (var output in dataPorts[1].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node target = tree.GetNodeByGuid(output.guid);
                    target.SetData(output.index, hitValid, nextRunner);
                }

                if (hitValid)
                {
                    foreach (var output in dataPorts[0].outputs)
                    {
                        NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                        Node target = tree.GetNodeByGuid(output.guid);
                        target.SetData(output.index, hit.point, nextRunner);
                    }

                    float hitDistance = Vector3.Distance(origin, hit.point);

                    foreach (var output in dataPorts[2].outputs)
                    {
                        NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                        Node target = tree.GetNodeByGuid(output.guid);
                        target.SetData(output.index, hitDistance, nextRunner);
                    }
                }

                OnStop(runner);

                ExecuteChilds(runner);
            }
            else
            {
                Debug.LogError($"Raycast needs data!!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

        }

        public override void InitData(NodeData runner) { runner.InitData(4); }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            if (dataPorts == null || dataPorts.Count != 3)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
                dataPorts.Add(new NodePort());
                dataPorts.Add(new NodePort());
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2, 3 };

            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Origin", typeof(Vector3)),
                new PortInfo(string.Empty, "Direction", typeof(Vector3)) ,
                new PortInfo(string.Empty, "LayerMask", typeof(LayerMask)),
                new PortInfo(string.Empty, "MaxDistance", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            switch (index)
            {
                case 0:
                    portInfo = new PortInfo(string.Empty, "Hit Point", typeof(Vector3));
                    break;
                case 1:
                    portInfo = new PortInfo(string.Empty, "Hit Valid", typeof(bool));
                    break;
                case 2:
                    portInfo = new PortInfo(string.Empty, "Distance To Point", typeof(float));
                    break;
                default:
                    base.GetOutputInfo(index, out portInfo);
                    break;
            }
        }
    }
}