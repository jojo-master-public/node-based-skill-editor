using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "Vector", "Variables/Vector", false, false)]
    public class VectorNode : DataNode<Vector3> { }
}