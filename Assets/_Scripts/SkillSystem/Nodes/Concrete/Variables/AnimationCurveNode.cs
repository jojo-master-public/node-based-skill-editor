using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "Curve", "Variables/Curve", false, false)]
    public class AnimationCurveNode : DataNode<AnimationCurve> { }
}