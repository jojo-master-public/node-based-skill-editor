using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "Rotation", "Variables/Rotation", false, false)]
    public class RotationNode : DataNode<Vector3>
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            Vector3 data;

            if (runner.DataNullCheck())
                data = (Vector3)runner[0];
            else
                data = value;

            if (dataPorts[0].outputs.Count > 0)
            {
                foreach (var output in dataPorts[0].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node outputNode = tree.GetNodeByGuid(output.guid);
                    SendData(outputNode, Quaternion.Euler(data), output.index, nextRunner);
                }
            }

            OnStop(runner);
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo("value", "Value", typeof(Quaternion));
        }
    }
}