using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "LayerMask", "Variables/LayerMask", false, false)]
    public class LayerMaskNode : DataNode<LayerMask> { }
}