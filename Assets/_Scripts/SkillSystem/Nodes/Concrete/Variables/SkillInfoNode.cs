using Project.Skills;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "Skill Info", "Variables/Skill Info", false, false)]
    public class SkillInfoNode : DataNode<FieldInfo[]>
    {
        private FieldInfo[] Properties
        {
            get
            {
                if (value == null || value.Length == 0)
                {
                    FieldInfo[] properties = typeof(Skill).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

                    properties = properties
                        .Where(x =>
                        {
                            SkillInfoAttribute info = (SkillInfoAttribute)x.GetCustomAttribute(typeof(SkillInfoAttribute), true);
                            return info != null && info.DisplayNode;
                        }).ToArray();

                    value = properties;
                }

                return value;
            }
        }



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            for (int i = 0; i < dataPorts.Count; i++)
            {
                foreach (var output in dataPorts[i].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node outputNode = tree.GetNodeByGuid(output.guid);
                    SendData(outputNode, Properties[i].GetValue(runner.targetSkill), output.index, nextRunner);
                }
            }

            OnStop(runner);
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, Skill.GetPropertyName(Properties[index].Name), Properties[index].FieldType);
        }

        public override void InitData(NodeData runner) { runner.InitData(Properties.Length); }

        protected override void InitOutputs()
        {
            if (dataPorts == null)
                dataPorts = new List<NodePort>();

            if(dataPorts.Count != Properties.Length)
            {
                int missingPorts = Properties.Length - dataPorts.Count;

                for (int i = 0; i < missingPorts; i++)
                {
                    dataPorts.Add(new NodePort());
                }
            }
        }
    }
}