namespace Project.SkillSystem
{
    [NodeInfo("variable", "Float", "Variables/Float", false, false)]
    public class FloatNode : DataNode<float> { }
}