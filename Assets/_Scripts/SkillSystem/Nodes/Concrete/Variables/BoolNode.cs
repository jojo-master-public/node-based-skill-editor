namespace Project.SkillSystem
{
    [NodeInfo("variable", "Bool", "Variables/Bool", false, false)]
    public class BoolNode : DataNode<bool> { }
}