using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "Prefab", "Variables/Prefab", false, false)]
    public class PrefabNode : DataNode<GameObject> { }
}