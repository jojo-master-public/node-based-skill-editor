namespace Project.SkillSystem
{
    [NodeInfo("variable", "Int", "Variables/Int", false, false)]
    public class IntNode : DataNode<int> { }
}