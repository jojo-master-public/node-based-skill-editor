using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("variable", "String", "Variables/String", false, false)]
    public class StringNode : DataNode<string> { }
}