namespace Project.SkillSystem
{
    /// <summary> Plays an animation on a target gameObject </summary>
    [NodeInfo("execute", "Play Animation", "Animation/Play", true, true)]
    public class PlayAnimationNode : Node
    {
        public string animationName;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            runner.skillCaster.PlayAnimation(animationName);

            OnStop(runner);
            ExecuteChilds(runner);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo("animationName", "Animation", typeof(string)) };
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }
    }
}