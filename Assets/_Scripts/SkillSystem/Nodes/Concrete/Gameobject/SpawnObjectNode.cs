namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Instantiates a gameObject at target position/rotation </summary>
    [NodeInfo("execute", "SpawnPrefab", "Gameobject/Instantiate")]
    public class SpawnObjectNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if(runner.DataNullCheck())
            {
                var Data = runner.GetData();

                GameObject outputData = (GameObject)Data[0];
                Vector3 position = (Vector3)Data[1];
                Quaternion rotation = (Quaternion)Data[2];

                GameObject instance = Instantiate(outputData, position, rotation);

                if (dataPorts[0].outputs.Count > 0)
                {
                    foreach (var output in dataPorts[0].outputs)
                    {
                        NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                        Node outputNode = tree.GetNodeByGuid(output.guid);

                        outputNode.SetData(output.index, instance, nextRunner);
                    }
                }
            }
            else
            {
                Debug.LogError("Some data is null!!");
                return;
            }

            ExecuteChilds(runner);
            OnStop(runner);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Prefab", typeof(GameObject)),
                new PortInfo(string.Empty, "Position", typeof(Vector3)),
                new PortInfo(string.Empty, "Rotation", typeof(Quaternion))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Instance", typeof(GameObject));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            if (dataPorts == null || dataPorts.Count == 0)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }
    }
}