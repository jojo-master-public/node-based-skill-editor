namespace Project.SkillSystem
{
    using UnityEngine;  

    /// <summary> Constructs a quaternion from Quaternion.Euler(x, y, z) using inputs </summary>
    [NodeInfo("variable", "Euler", "Math/Quaternion/Euler", false, false)]
    public class EulerNode : SingleOutputNode
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if (runner.DataNullCheck())
            {
                var Data = runner.GetData();

                float x = (float)Data[0];
                float y = (float)Data[1];
                float z = (float)Data[2];

                Quaternion result = Quaternion.Euler(x, y, z);

                foreach (var output in dataPorts[0].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node outputNode = tree.GetNodeByGuid(output.guid);
                    outputNode.SetData(output.index, result, nextRunner);
                }
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "x", typeof(float)),
                new PortInfo(string.Empty, "y", typeof(float)),
                new PortInfo(string.Empty, "z", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Output", typeof(Quaternion));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }
    }
}