namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Returns a Quaternion.LookRotation from forward and up vector inputs </summary>
    [NodeInfo("variable", "Look Rotation", "Math/Look Rotation", false, false)]
    public class LookRotation : Node
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if(runner.DataNullCheck())
            {
                var Data = runner.GetData();

                Vector3 forward = (Vector3)Data[0];
                Vector3 up = (Vector3)Data[1];

                Quaternion result = Quaternion.LookRotation(forward, up);

                foreach (var output in dataPorts[0].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node target = tree.GetNodeByGuid(output.guid);
                    target.SetData(output.index, result, nextRunner); 
                }
            }
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }

        protected override void InitOutputs()
        {
            if (dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Forward", typeof(Vector3)),
                new PortInfo(string.Empty, "Up", typeof(Vector3))
            };

        }
        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(Quaternion));
        }
    }
}