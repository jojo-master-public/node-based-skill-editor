namespace Project.SkillSystem
{
    using UnityEngine;

    public enum MathOperation
    {
        ADD,
        SUBTRACT,
        MULTIPLY,
        DIVIDE,
        SQRT,
        POWER
    }

    /// <summary> Performs a math operation on inputs and outputs a result </summary>
    [NodeInfo("variable", "Math", "Math/Float/Math", false, false)]
    public class MathNode : SingleOutputNode
    {
        public MathOperation mathOperation;



        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if (runner.DataNullCheck())
            {
                var Data = runner.GetData();

                float a = (float)Data[0];
                float b = (float)Data[1];
                float result = Calculate(a, b, mathOperation, out bool error);

                if(error)
                {
                    Debug.LogError("Error during calculation!");
                    runner.currentState = NodeData.State.ERROR;
                    return;
                }

                OutputData(0, result, runner);
            }
            else
            {
                runner.currentState = NodeData.State.ERROR;
                return;
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "A", typeof(float)) ,
                new PortInfo(string.Empty, "B", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(float));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }

        private float Calculate(float a, float b, MathOperation operation, out bool error)
        {
            switch (operation)
            {
                case MathOperation.ADD:
                    error = false;
                    return a + b;
                case MathOperation.SUBTRACT:
                    error = false;
                    return a - b;
                case MathOperation.MULTIPLY:
                    error = false;
                    return a * b;
                case MathOperation.DIVIDE:
                    if(Mathf.Approximately(b, 0))
                    {
                        Debug.LogError("Division by ZERO");
                        error = true;
                        return 0;
                    }
                    else
                    {
                        error = false;
                        return a / b;
                    }
                case MathOperation.SQRT:
                    if(a < 0)
                    {
                        Debug.LogError("Square root of negative number!");
                        error = true;
                        return 0;
                    }
                    else
                    {
                        error = false;
                        return Mathf.Sqrt(a);
                    }
                case MathOperation.POWER:
                    error = false;
                    return Mathf.Pow(a, b);
                default:
                    error = false;
                    return 0;
            }
        }
    }
}