namespace Project.SkillSystem
{
    /// <summary> Multiplies inputs and outputs a result </summary>
    [NodeInfo("variable", "Multiply", "Math/Float/Multiply", false, false)]
    public class MultiplyNode : SingleOutputNode
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if (runner.DataNullCheck())
            {
                var Data = runner.GetData();

                float a = (float)Data[0];
                float b = (float)Data[1];
                float result = a * b;
                OutputData(0, result, runner);
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "A", typeof(float)) ,
                new PortInfo(string.Empty, "B", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(float));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }
    }
}