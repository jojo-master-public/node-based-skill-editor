namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Constructs and outputs a vector from x, y, z input floats </summary>
    [NodeInfo("variable", "Create", "Math/Vector/Create", false, false)]
    public class CombineVectorNode : SingleOutputNode
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if (runner.DataNullCheck())
            {
                var Data = runner.GetData();

                float x = (float)Data[0];
                float y = (float)Data[1];
                float z = (float)Data[2];

                Vector3 result = new Vector3(x, y, z);
                OutputData(0, result, runner);
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2};
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "X", typeof(float)),
                new PortInfo(string.Empty, "Y", typeof(float)),
                new PortInfo(string.Empty, "Z", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(Vector3));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }
    }
}