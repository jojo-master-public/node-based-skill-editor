namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Returns a direction from point a to point b </summary>
    [NodeInfo("variable", "Direction", "Math/Vector/Direction", false, false)]
    public class DirectionNode : SingleOutputNode
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if (runner.DataNullCheck())
            {
                var Data = runner.GetData();

                Vector3 a = (Vector3)Data[0];
                Vector3 b = (Vector3)Data[1];
                Vector3 result = b - a;
                OutputData(0, result, runner);
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "From", typeof(Vector3)) ,
                new PortInfo(string.Empty, "To", typeof(Vector3))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(Vector3));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }
    }
}