namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Scales vector by a factor </summary>
    [NodeInfo("variable", "Scale Vector", "Math/Vector/Scale", false, false)]
    public class ScaleVectorNode : SingleOutputNode
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if(runner.DataNullCheck())
            {
                var Data = runner.GetData();

                Vector3 a = (Vector3)Data[0];
                float targetScale = (float)Data[1];

                Vector3 result = a * targetScale;
                OutputData(0, result, runner);
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Vector", typeof(Vector3)) ,
                new PortInfo(string.Empty, "Scale", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(Vector3));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }
    }
}