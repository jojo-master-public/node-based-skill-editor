namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Gets info about x, y, z from a vector </summary>
    [NodeInfo("variable", "Split", "Math/Vector/Split", false, false)]
    public class SplitVectorNode : Node
    {
        public override void Execute(NodeData runner) { }
        public override void InitData(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            Vector3 input = NodeDataUtils.Convert<Vector3>(data);

            for (int i = 0; i < 3; i++)
            {
                foreach (var output in dataPorts[i].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node target = tree.GetNodeByGuid(output.guid);
                    target.SetData(output.index, input[i], nextRunner);
                }
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo(string.Empty, "Input", typeof(Vector3)) };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            switch (index)
            {
                case 0:
                    portInfo = new PortInfo(string.Empty, "X", typeof(float));
                    break;
                case 1:
                    portInfo = new PortInfo(string.Empty, "Y", typeof(float));
                    break;
                case 2:
                    portInfo = new PortInfo(string.Empty, "Z", typeof(float));
                    break;
                default:
                    base.GetOutputInfo(index, out portInfo);
                    break;
            }
        }

        protected override void InitOutputs()
        {
            if (dataPorts != null)
                return;

            dataPorts = new List<NodePort>();
            dataPorts.Add(new NodePort());
            dataPorts.Add(new NodePort());
            dataPorts.Add(new NodePort());
        }
    }
}