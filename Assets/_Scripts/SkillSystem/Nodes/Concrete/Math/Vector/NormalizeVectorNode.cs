namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Normalizes vector before outputting it </summary>
    [NodeInfo("variable", "Normalize", "Math/Vector/Normalize", false, false)]
    public class NormalizeVectorNode : SingleOutputNode
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            Vector3 input = NodeDataUtils.Convert<Vector3>(data);

            OutputData(0, input.normalized, runner);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Input", typeof(Vector3))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Output", typeof(Vector3));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }
    }
}