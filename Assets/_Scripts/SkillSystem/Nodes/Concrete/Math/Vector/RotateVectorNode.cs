namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Rotates vector by quaternion </summary>
    [NodeInfo("variable", "Rotate", "Math/Vector/Rotate", false, false)]
    public class RotateVectorNode : Node
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if (runner.DataNullCheck())
            {
                var Data = runner.GetData();

                Vector3 v = (Vector3)Data[0];
                Quaternion r = (Quaternion)Data[1];

                Vector3 result = r * v;

                OutputData(0, result, runner);
            }
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }

        protected override void InitOutputs()
        {
            if(dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Vector", typeof(Vector3)),
                new PortInfo(string.Empty, "Rotation", typeof(Quaternion))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(Vector3));
        }
    }
}
