namespace Project.SkillSystem
{
    using System;
    using UnityEngine;

    public enum OutputType
    {
        Float,
        Int,
        Vector3,
        Quaternion,
        AnimationCurve,
        String,
        GameObject
    }

    [Serializable]
    public class Output
    {
        public string outputName;
        public OutputType outputType;
    }

    /// <summary> Outputs data from a subtree up the execution/data chain </summary>
    [NodeInfo("execute", "Output", "SubTree/Output", true, false)]
    public class OutputNode : Node
    {
        // fired to transfer execution flow from this node up the execution chain
        public event Action<OutputNode, NodeData> onExecutionFinished;
        // fired to transfer data from this node up the data chain
        public event Action<OutputNode, int, object, NodeData> onSendData;

        public Output[] outputs = new Output[0];



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if(outputs.Length > 0)
            {
                if (runner.DataNullCheck())
                {
                    for (int i = 0; i < outputs.Length; i++)
                    {
                        onSendData?.Invoke(this, i, runner[i], runner);
                    }

                    OnStop(runner);
                    onExecutionFinished?.Invoke(this, runner);
                }
                else
                {
                    Debug.LogError("Some data is null!!");
                    runner.currentState = NodeData.State.ERROR;
                    return;
                }
            }
            else
            {
                OnStop(runner);
                onExecutionFinished?.Invoke(this, runner);
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[outputs.Length];

            for (int i = 0; i < index.Length; i++)
            {
                int newIndex = i;
                index[i] = newIndex;
            }

            portInfo = new PortInfo[outputs.Length];

            for (int i = 0; i < outputs.Length; i++)
            {
                portInfo[i] = new PortInfo(string.Empty, outputs[i].outputName, GetOutputType(outputs[i].outputType));
            }
        }

        public void GetInputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, outputs[index].outputName, GetOutputType(outputs[index].outputType));
        }

        public static Type GetOutputType(OutputType t)
        {
            switch (t)
            {
                case OutputType.Float:
                    return typeof(float);
                case OutputType.Int:
                    return typeof(int);
                case OutputType.Vector3:
                    return typeof(Vector3);
                case OutputType.Quaternion:
                    return typeof(Quaternion);
                case OutputType.AnimationCurve:
                    return typeof(AnimationCurve);
                case OutputType.String:
                    return typeof(string);
                case OutputType.GameObject:
                    return typeof(GameObject);
                default:
                    return null;
            }
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(outputs.Length);
        }
    }
}