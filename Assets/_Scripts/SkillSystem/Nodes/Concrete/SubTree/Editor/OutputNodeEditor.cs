namespace Project.SkillSystem
{
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(OutputNode))]
    public class OutputNodeEditor : Editor
    {
        private SerializedProperty outputs;



        private void OnEnable()
        {
            try
            {
                if(serializedObject != null)
                    outputs = serializedObject.FindProperty("outputs");    
            }
            catch (System.Exception ex)
            {

            }
        }

        public override void OnInspectorGUI()
        {
            int arrayCount = outputs.arraySize;

            EditorGUILayout.Space(5);
            EditorGUILayout.LabelField("Outputs");
            EditorGUILayout.Space(5);

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add"))
            {
                outputs.InsertArrayElementAtIndex(arrayCount);
                arrayCount++;

            }

            if (GUILayout.Button("Remove"))
            {
                if(arrayCount > 0)
                {
                    outputs.DeleteArrayElementAtIndex(arrayCount - 1);
                    arrayCount--;
                }
            }

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < arrayCount; i++)
            {
                SerializedProperty nameProp = outputs.GetArrayElementAtIndex(i).FindPropertyRelative("outputName");
                SerializedProperty typeProp = outputs.GetArrayElementAtIndex(i).FindPropertyRelative("outputType");

                EditorGUILayout.BeginHorizontal();

                string outputName = EditorGUILayout.TextField(nameProp.stringValue);
                OutputType outputType = (OutputType)EditorGUILayout.EnumPopup((OutputType)typeProp.enumValueIndex);

                nameProp.stringValue = outputName;
                typeProp.enumValueIndex = (int)outputType;

                EditorGUILayout.EndHorizontal();
            }

            if(EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();

                ((OutputNode)target).ValidateNode();
            }
        }
    }
}