namespace Project.SkillSystem
{
    using UnityEditor;


    [CustomEditor(typeof(SubGraphNode))]
    public class SubGraphNodeEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            base.OnInspectorGUI();

            if(EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
                ((SubGraphNode)target).ValidateNode();
            }
        }
    }
}