using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("input", "Prefab", "SubTree/Input/Prefab", false, false)]
    public class InputPrefabNode : InputDataNode<GameObject> { }
}