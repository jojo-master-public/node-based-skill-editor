using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("input", "Rotation", "SubTree/Input/Rotation", false, false)]
    public class InputRotationNode : InputDataNode<Quaternion> { }
}