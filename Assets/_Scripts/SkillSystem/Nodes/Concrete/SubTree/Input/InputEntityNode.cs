namespace Project.SkillSystem
{
    using RPG.Core;

    [NodeInfo("input", "Entity", "SubTree/Input/Entity", false, false)]
    public class InputEntityNode : InputDataNode<GameEntity> { }
}