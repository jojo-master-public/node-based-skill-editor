using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("input", "String", "SubTree/Input/String", false, false)]
    public class InputStringNode : InputDataNode<string> { }
}