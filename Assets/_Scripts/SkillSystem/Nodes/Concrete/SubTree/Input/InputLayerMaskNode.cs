using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("input", "LayerMask", "SubTree/Input/LayerMask", false, false)]
    public class InputLayerMaskNode : InputDataNode<LayerMask> { }
}