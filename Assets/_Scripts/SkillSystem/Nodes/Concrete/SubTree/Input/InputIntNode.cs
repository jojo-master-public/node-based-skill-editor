namespace Project.SkillSystem
{
    [NodeInfo("input", "Int", "SubTree/Input/Int", false, false)]
    public class InputIntNode : InputDataNode<int> { }
}