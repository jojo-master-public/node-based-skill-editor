namespace Project.SkillSystem
{
    [NodeInfo("input", "Uint", "SubTree/Input/Uint", false, false)]
    public class InputUintNode : InputDataNode<uint> { }
}