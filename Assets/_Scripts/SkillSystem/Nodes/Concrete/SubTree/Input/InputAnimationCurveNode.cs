using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("input", "Curve", "SubTree/Input/Curve", false, false)]
    public class InputAnimationCurveNode : InputDataNode<AnimationCurve> { }
}