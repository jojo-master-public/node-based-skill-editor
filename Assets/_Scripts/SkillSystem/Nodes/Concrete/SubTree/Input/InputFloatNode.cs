namespace Project.SkillSystem
{
    [NodeInfo("input", "Float", "SubTree/Input/Float", false, false)]
    public class InputFloatNode : InputDataNode<float> { }
}