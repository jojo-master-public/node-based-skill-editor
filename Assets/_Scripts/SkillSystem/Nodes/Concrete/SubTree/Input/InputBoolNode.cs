namespace Project.SkillSystem
{
    [NodeInfo("input", "Bool", "SubTree/Input/Bool", false, false)]
    public class InputBoolNode : InputDataNode<bool> { }
}