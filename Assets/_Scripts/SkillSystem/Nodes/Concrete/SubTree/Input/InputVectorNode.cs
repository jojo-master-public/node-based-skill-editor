using UnityEngine;

namespace Project.SkillSystem
{
    [NodeInfo("input", "Vector", "SubTree/Input/Vector", false, false)]
    public class InputVectorNode : InputDataNode<Vector3> { }
}