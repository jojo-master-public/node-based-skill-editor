namespace Project.SkillSystem
{
    using System.Collections.Generic;

    /// <summary> Node for inputing data into a sub tree </summary>
    public abstract class InputDataNode : DataNode 
    {
        public string inputName;
    }

    public abstract class InputDataNode<T> : InputDataNode
    {
        public T value;



        public void SetData(Node target, object data, int index, NodeData nextRunner)
        {
            target.SetData(index, data, nextRunner);
        }

        public override void Execute(NodeData nodeData)
        {
            OnStart(nodeData);

            T data;

            if (nodeData.DataNullCheck())
                data = (T)nodeData[0];
            else
                data = value;

            if (dataPorts[0].outputs.Count > 0)
            {
                foreach (var output in dataPorts[0].outputs)
                {
                    NodeData nextRunner = nodeData.skillCaster.GetDataForNextNode(nodeData, output.guid);
                    Node outputNode = tree.GetNodeByGuid(output.guid);
                    SetData(outputNode, data, output.index, nextRunner);
                }
            }
            OnStop(nodeData);
        }

        public override void InitData(NodeData nodeData)
        {
            nodeData.InitData(1);
        }

        protected override void InitOutputs()
        {
            if (dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>(1);
                dataPorts.Add(new NodePort());
            }
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Value", typeof(T));
        }

        public override void SetData(int index, object data, NodeData nodeData)
        {
            InitData(nodeData);
            var converted = NodeDataUtils.Convert<T>(data);
            nodeData.SetData(index, converted);
        }
    }
}