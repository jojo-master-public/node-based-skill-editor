namespace Project.SkillSystem
{
    using System;
    using System.Collections.Generic;

    /// <summary> Container for data and actions that are received by the SubGraphNode when OutputNode has finished executing </summary>
    public class RunnerFinishedObject
    {
        public NodeData sourceData;

        public Action<NodeData> onRunnerFinished;
        public Action<int, object, NodeData> onSendData;



        public void OnRunnerSendingData(OutputNode node, int index, object data, NodeData finishingRunner)
        {
            node.onSendData -= OnRunnerSendingData;
            onSendData?.Invoke(index, data, sourceData);
        }

        public void OnRunnerFinished(OutputNode node, NodeData finishingRunner)
        {
            node.onExecutionFinished -= OnRunnerFinished;
            onRunnerFinished.Invoke(sourceData);
        }
    }

    /// <summary> Executes a target sub skill tree </summary>
    [NodeInfo("sub-graph", "Sub Tree", "SubTree/Node", true, true)]
    public class SubGraphNode : Node
    {
        public SubSkillTree targetTree;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            NodeData treeRunner = runner.skillCaster.GetDataForNextNode(runner, Guid.NewGuid().ToString());
            treeRunner.subTreeGuid = guid;
            targetTree.Init();

            var inputNodes = targetTree.FindInputNodes();

            GetInputInfo(out int[] index, out PortInfo[] portInfo);

            foreach (var input in inputNodes)
            {
                int portIndex = -1;

                for (int i = 0; i < portInfo.Length; i++)
                {
                    if (portInfo[i].displayName == input.inputName)
                    {
                        portIndex = i;
                        break;
                    }
                }

                NodeData nextRunner = runner.skillCaster.GetDataForNextNode(treeRunner, input.guid);

                input.SetData(0, runner[index[portIndex]], nextRunner);
                input.Execute(nextRunner);
            }

            RunnerFinishedObject eventObject = new RunnerFinishedObject();
            eventObject.sourceData = runner;
            eventObject.onSendData = OnSendData;
            eventObject.onRunnerFinished = OnExecuteFinished;

            OutputNode output = targetTree.FindOutputNode();
            output.onSendData += eventObject.OnRunnerSendingData;
            output.onExecutionFinished += eventObject.OnRunnerFinished;

            targetTree.Execute(treeRunner);
        }

        // runner that finished execution of the tree (OutputNode)
        private void OnExecuteFinished(NodeData runner)
        {
            OnStop(runner);
            ExecuteChilds(runner);
        }

        private void OnSendData(int index, object data, NodeData runner)
        {
            foreach (var output in dataPorts[index].outputs)
            {
                NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                Node outputNode = tree.GetNodeByGuid(output.guid);
                outputNode.SetData(index, data, nextRunner);
            }
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            if (targetTree == null)
                base.GetInputInfo(out index, out portInfo);
            else
            {
                var inputNodes = targetTree.FindInputNodes();

                index = new int[inputNodes.Count];

                for (int i = 0; i < inputNodes.Count; i++)
                {
                    int inputIndex = i;
                    index[i] = inputIndex;
                }

                portInfo = new PortInfo[inputNodes.Count];

                for (int i = 0; i < inputNodes.Count; i++)
                {
                    inputNodes[i].GetOutputInfo(0, out PortInfo pInfo);
                    portInfo[i] = new PortInfo(string.Empty, inputNodes[i].inputName, pInfo.type);
                }
            }
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            if (targetTree != null)
            {
                OutputNode outputNode = targetTree.FindOutputNode();

                outputNode.GetInputInfo(index, out portInfo);
            }
            else
                base.GetOutputInfo(index, out portInfo);
        }

        public override void InitData(NodeData runner)
        {
            if (targetTree != null)
            {
                var inputNodes = targetTree.FindInputNodes();

                if (inputNodes != null)
                    runner.InitData(inputNodes.Count);
            }
        }

        public void InitOutputPorts()
        {
            InitOutputs();
        }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            OutputNode outputNode = null;

            if (targetTree != null)
                outputNode = targetTree.FindOutputNode();

            if(outputNode != null)
                if(dataPorts == null || dataPorts.Count != outputNode.outputs.Length)
                {
                    dataPorts = new List<NodePort>();
                
                    for (int i = 0; i < outputNode.outputs.Length; i++)
                    {
                        dataPorts.Add(new NodePort());
                    }
                }
        }

        public override void ValidateNode()
        {
            executionPorts = null;
            dataPorts = null;

            InitOutputs();

            base.ValidateNode();
        }
    }
}