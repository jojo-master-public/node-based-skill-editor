namespace Project.SkillSystem
{
    using System.Collections;
    using UnityEngine;

    /// <summary> Lerps gameObject's position to targetPosition over duration using an animation curve for smoothing in/out </summary>
    [NodeInfo("execute", "Lerp Position", "Translate/Lerp Position")]
    public class LerpPositionNode : SingleOutputNode
    {
        public Vector3 targetPosition;
        public float duration;
        public AnimationCurve curve;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var Data = runner.GetData();

            if (Data == null || Data[0] == null)
            {
                Debug.LogError("The target input is NULL");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            GameObject targetObject = (GameObject)Data[0];
            Vector3 targetLerpPosition = (Data[1] != null) ? (Vector3)Data[1] : targetPosition;
            float targetDuration = (Data[2] != null) ? (float)Data[2] : duration;
            AnimationCurve targetCurve = (Data[3] != null) ? (AnimationCurve)Data[3] : curve;

            runner.skillCaster.StartCoroutine(CoroutineLerp(targetObject.transform, targetLerpPosition, targetDuration, runner, targetCurve));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(4);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2, 3 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Target", typeof(GameObject)) ,
                new PortInfo("targetPosition", "Position", typeof(Vector3)),
                new PortInfo("duration", "Duration", typeof(float)),
                new PortInfo("curve", "Curve", typeof(AnimationCurve))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Moved Object", typeof(GameObject));
        }

        private IEnumerator CoroutineLerp(Transform target, Vector3 targetPosition, float duration, NodeData runner, AnimationCurve curve = null)
        {
            Vector3 sourcePosition = target.position;
            float timer = 0;

            while (timer < duration)
            {
                timer += Time.deltaTime;

                target.position = Vector3.Lerp(sourcePosition, targetPosition, (curve != null) ? curve.Evaluate(timer / duration) : timer / duration);

                yield return null;
            }

            target.position = targetPosition;

            OnStop(runner);
            OutputData(0, target, runner);
            ExecuteChilds(runner);
        }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);
        }
    }
}