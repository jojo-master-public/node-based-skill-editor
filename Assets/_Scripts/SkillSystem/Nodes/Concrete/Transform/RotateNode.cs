namespace Project.SkillSystem
{
    using System.Collections;
    using UnityEngine;

    /// <summary> Rotates a gameObject around "rotation" for the "duration" using Quaternion.Euler or input </summary>
    [NodeInfo("execute", "Rotate", "Translate/Rotate")]
    public class RotateNode : SingleOutputNode
    {
        public Vector3 rotation;
        public float duration;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var Data = runner.GetData();

            if (Data == null || Data[0] == null)
            {
                Debug.LogError("The target or Data is not set!!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            GameObject targetObject = (GameObject)Data[0];
            Quaternion deltaRotation = (Data[1] != null) ? (Quaternion)Data[1] : Quaternion.Euler(rotation);
            float targetDuration = (Data[2] != null) ? (float)Data[2] : duration;

            runner.skillCaster.StartCoroutine(CoroutineRotate(targetObject.transform, deltaRotation, targetDuration, runner));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[]
            {
            new PortInfo(string.Empty, "Target", typeof(GameObject)),
            new PortInfo("rotation", "Rotation", typeof(Vector3)),
            new PortInfo("duration", "Duration", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Moved Object", typeof(GameObject));
        }

        private IEnumerator CoroutineRotate(Transform target, Quaternion rotationDelta, float duration, NodeData runner)
        {
            float timer = 0;

            while (timer < duration)
            {
                timer += Time.deltaTime;

                target.Rotate(rotationDelta.eulerAngles * Time.deltaTime);

                yield return null;
            }

            OnStop(runner);
            OutputData(0, runner[0], runner);
            ExecuteChilds(runner);
        }
    }
}