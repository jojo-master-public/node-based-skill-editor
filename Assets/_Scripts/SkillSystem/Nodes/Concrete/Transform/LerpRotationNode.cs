namespace Project.SkillSystem
{
    using System.Collections;
    using UnityEngine;

    /// <summary> Lerps gameObject's rotation to "rotation" over duration </summary>
    [NodeInfo("execute", "Lerp Rotation", "Translate/Lerp Rotation")]
    public class LerpRotationNode : SingleOutputNode
    {
        public Vector3 rotation;
        public float duration;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var Data = runner.GetData();

            if (Data == null || Data[0] == null)
            {
                Debug.LogError("The target input is NULL");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            GameObject targetObject = (GameObject)Data[0];
            Quaternion targetRotation = (Data[1] != null) ? (Quaternion)Data[1] : Quaternion.Euler(rotation);
            float targetDuration = (Data[2] != null) ? (float)Data[2] : duration;

            runner.skillCaster.StartCoroutine(CoroutineLerp(targetObject.transform, targetRotation, targetDuration, runner));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[]
            {
            new PortInfo(string.Empty, "Target", typeof(GameObject)) ,
            new PortInfo("rotation", "Rotation", typeof(Vector3)),
            new PortInfo("duration", "Duration", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Moved Object", typeof(GameObject));
        }

        private IEnumerator CoroutineLerp(Transform target, Quaternion targetRotation, float duration, NodeData runner)
        {
            Quaternion sourceRotation = target.rotation;

            float timer = 0;

            while (timer < duration)
            {
                timer += Time.deltaTime;

                target.rotation = Quaternion.Lerp(sourceRotation, targetRotation, timer / duration);

                yield return null;
            }
            target.rotation = targetRotation;

            OnStop(runner);
            OutputData(0, runner[0], runner);
            ExecuteChilds(runner);
        }
    }
}