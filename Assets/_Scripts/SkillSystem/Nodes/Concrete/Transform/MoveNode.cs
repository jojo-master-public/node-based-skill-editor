namespace Project.SkillSystem
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Moves gameObject in target direction using speed and duration </summary>
    [NodeInfo("execute", "Move", "Translate/Move")]
    public class MoveNode : SingleOutputNode
    {
        public Vector3 direction;
        public float duration;
        public float speed;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var Data = runner.GetData();

            if (Data == null || Data[0] == null)
            {
                Debug.LogError("The target input is NULL");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            GameObject targetObject = (GameObject)Data[0];
            Vector3 targetDirection = (Data[1] != null) ? (Vector3)Data[1] : direction;
            float targetDuration = (Data[2] != null) ? (float)Data[2] : duration;
            float targetSpeed = (Data[3] != null) ? (float)Data[3] : speed;

            runner.skillCaster.StartCoroutine(CoroutineMove(targetObject.transform, targetDirection, targetDuration, targetSpeed, runner));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(4);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2, 3 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Target", typeof(GameObject)) ,
                new PortInfo("direction", "Direction", typeof(Vector3)),
                new PortInfo("duration", "Duration", typeof(float)),
                new PortInfo("speed", "Speed", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Moved Object", typeof(GameObject));
        }

        public override void GetOutputExecutionInfo(int index, out PortInfo portInfo)
        {
            if (index == 0)
                portInfo = new PortInfo(string.Empty, "On Finished", null);
            else
                portInfo = new PortInfo(string.Empty, "On Step", null);
        }

        protected override void InitOutputs()
        {
            if(executionPorts == null || executionPorts.Count != 2)
            {
                executionPorts = new List<NodePort>();
                executionPorts.Add(new NodePort());
                executionPorts.Add(new NodePort());
            }

            if (dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }

        private IEnumerator CoroutineMove(Transform target, Vector3 direction, float duration, float speed, NodeData runner)
        {
            float timer = 0;

            while (timer < duration)
            {
                timer += Time.deltaTime;

                if(target != null)
                    target.position += direction * speed * Time.deltaTime;
                else
                {
                    OnStop(runner);
                    yield break;
                }

                OutputData(0, runner[0], runner);
                ExecuteChilds(runner, 1);
                yield return null;
            }

            OnStop(runner);
            OutputData(0, runner[0], runner);
            ExecuteChilds(runner, 0);
        }
    }
}