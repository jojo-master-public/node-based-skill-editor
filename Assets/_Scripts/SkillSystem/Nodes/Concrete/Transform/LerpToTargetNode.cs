namespace Project.SkillSystem
{
    using System.Collections;
    using UnityEngine;

    /// <summary> Lerps gameObject's position to target transform.position using speed </summary>
    [NodeInfo("execute", "Lerp To Target", "Translate/Lerp To Target")]
    public class LerpToTargetNode : Node
    {
        public float speed;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var Data = runner.GetData();

            if (Data == null || Data[0] == null || Data[1] == null)
            {
                Debug.LogError("The target inputs are NULL");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            GameObject targetObject = (GameObject)Data[0];
            Transform target = (Transform)Data[1];
            float targetSpeed = (Data[2] != null) ? (float)Data[2] : speed;

            runner.skillCaster.StartCoroutine(CoroutineLerp(targetObject.transform, target, targetSpeed, runner));
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Source", typeof(GameObject)),
                new PortInfo(string.Empty, "Target", typeof(Transform)),
                new PortInfo("speed", "Speed", typeof(float))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Moved Object", typeof(GameObject));
        }

        public override void GetOutputExecutionInfo(int index, out PortInfo portInfo)
        {
            if (index == 0)
                portInfo = new PortInfo(string.Empty, "On Finished", typeof(Transform));
            else
                portInfo = new PortInfo(string.Empty, "On Moved", typeof(Transform));
        }

        protected override void InitOutputs()
        {
            if(executionPorts == null || executionPorts.Count != 2)
            {
                executionPorts = new System.Collections.Generic.List<NodePort>();
                executionPorts.Add(new NodePort());
                executionPorts.Add(new NodePort());
            }

            if(dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new System.Collections.Generic.List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }

        private IEnumerator CoroutineLerp(Transform source, Transform target, float speed, NodeData runner, AnimationCurve curve = null)
        {
            while (Vector3.Distance(source.position, target.position) > 0.1f)
            {
                source.position = Vector3.MoveTowards(source.position, target.position, speed * Time.deltaTime);

                OutputData(0, source, runner);
                ExecuteChilds(runner, 1);

                yield return null;
            }

            source.position = target.position;

            OnStop(runner);
            OutputData(0, source, runner);
            ExecuteChilds(runner);
        }
    }
}