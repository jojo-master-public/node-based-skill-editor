namespace Project.SkillSystem
{
    using System.Collections;
    using UnityEngine;

    /// <summary> Delays skill execution for duration </summary>
    [NodeInfo("execute", "Wait", "Logic/Wait")]
    public class WaitNode : Node
    {
        public float duration;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            float targetDuration = (runner.DataNullCheck()) ? (float)runner[0] : duration;

            runner.skillCaster.StartCoroutine(CoroutineWait(targetDuration, runner));
        }

        private IEnumerator CoroutineWait(float time, NodeData runner)
        {
            yield return new WaitForSeconds(time);

            OnStop(runner);

            ExecuteChilds(runner);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };

            portInfo = new PortInfo[] { new PortInfo("duration", "Duration", typeof(float)) };
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }
    }
}