namespace Project.SkillSystem
{
    using RPG.Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Only perform this node on a subtree!  </summary>
    [NodeInfo("execute", "ForEach", "Logic/ForEach Entity")]
    public class ForEachEntityNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if(runner.DataNullCheck())
            {
                GameEntity[] entities = (GameEntity[])runner[0];

                foreach (GameEntity entity in entities)
                {
                    Dictionary<string, NodeData> augmentedRunners = new Dictionary<string, NodeData>();

                    foreach (var dataOutput in dataPorts[0].outputs)
                    {
                        NodeData original = runner.skillCaster.GetDataForNextNode(runner, dataOutput.guid);
                        NodeData nextRunner = AugmentRunner(original);

                        augmentedRunners.Add(dataOutput.guid, nextRunner);

                        Node targetNode = tree.GetNodeByGuid(dataOutput.guid);
                        targetNode.SetData(dataOutput.index, entity, nextRunner);
                    }

                    foreach (var output in executionPorts[0].outputs)
                    {
                        Node targetNode = tree.GetNodeByGuid(output.guid);

                        if (targetNode == null)
                        {
                            Debug.LogError("Output guid contains non-existant node!");

                            continue;
                        }

                        if (targetNode.GetType() != typeof(SubGraphNode))
                        {
                            Debug.LogError("RepeatNode can only execute SubTrees right now!");
                            continue;
                        }
                        else
                        {
                            NodeData nextRunner;

                            if (augmentedRunners.ContainsKey(output.guid))
                                nextRunner = augmentedRunners[output.guid];
                            else
                            {
                                NodeData original = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                                nextRunner = AugmentRunner(original);
                            }

                            targetNode.Execute(nextRunner);
                        }
                    }
                }
            }
            else
            {
                Debug.LogError("Data is null!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            OnStop(runner);
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo(string.Empty, "Entities", typeof(GameEntity[])) };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Entity", typeof(GameEntity));
        }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            if(dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }

        private NodeData AugmentRunner(NodeData original)
        {
            NodeData augmentedRunner = new NodeData();
            augmentedRunner.containerGuid = original.containerGuid;
            augmentedRunner.subTreeGuid = original.subTreeGuid + Guid.NewGuid().ToString();
            augmentedRunner.skillCaster = original.skillCaster;
            augmentedRunner.targetSkill = original.targetSkill;
            var data = original.GetData();
            augmentedRunner.InitData(data.Length);

            for (int i = 0; i < data.Length; i++)
            {
                augmentedRunner.SetData(i, data[i]);
            }

            return augmentedRunner;
        }
    }
}