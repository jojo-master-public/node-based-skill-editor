namespace Project.SkillSystem
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Repeats execution of target nodes for "repeatTimes" times with each repeat delayed by "repeatDelay" seconds </summary>
    [NodeInfo("execute", "Repeat", "Logic/Repeat")]
    public class RepeatNode : Node
    {
        public uint repeatTimes = 1;
        public float repeatDelay = 0;



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var data = runner.GetData();

            uint count = (data == null || data[0] == null) ? repeatTimes : (uint)data[0];
            float delay = (data == null || data[1] == null) ? repeatDelay : (float)data[1];

            runner.skillCaster.StartCoroutine(CoroutineRepeat(count, delay, runner));
        }

        private IEnumerator CoroutineRepeat(uint count, float delay, NodeData runner)
        {
            for (uint i = 0; i < count; i++)
            {
                uint currentIteration = i;

                yield return new WaitForSeconds(delay);

                Dictionary<string, NodeData> augmentedRunners = new Dictionary<string, NodeData>();

                foreach (var dataOutput in dataPorts[0].outputs)
                {
                    NodeData original = runner.skillCaster.GetDataForNextNode(runner, dataOutput.guid);
                    NodeData nextRunner = AugmentRunner(original);

                    augmentedRunners.Add(dataOutput.guid, nextRunner);

                    Node targetNode = tree.GetNodeByGuid(dataOutput.guid);
                    targetNode.SetData(dataOutput.index, count, nextRunner);
                }

                foreach (var dataOutput in dataPorts[1].outputs)
                {
                    NodeData nextRunner = augmentedRunners[dataOutput.guid];

                    Node targetNode = tree.GetNodeByGuid(dataOutput.guid);
                    targetNode.SetData(dataOutput.index, currentIteration, nextRunner);
                }

                foreach (var output in executionPorts[0].outputs)
                {
                    Node targetNode = tree.GetNodeByGuid(output.guid);

                    if (targetNode == null)
                    {
                        Debug.LogError("Output guid contains non-existant node!");

                        continue;
                    }

                    if(targetNode.GetType() != typeof(SubGraphNode))
                    {
                        Debug.LogError("RepeatNode can only execute SubTrees right now!");
                        continue;
                    }
                    else
                    {
                        Debug.Log("This might not work with new system, need to rework RepeatNode!");

                        NodeData nextRunner;

                        if (augmentedRunners.ContainsKey(output.guid))
                            nextRunner = augmentedRunners[output.guid];
                        else
                        {
                            NodeData original = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                            nextRunner = AugmentRunner(original);
                        }

                        targetNode.Execute(nextRunner);
                    }
                }
            }

            OnStop(runner);
        }

        private NodeData AugmentRunner(NodeData original)
        {
            NodeData augmentedRunner = new NodeData();
            augmentedRunner.containerGuid = original.containerGuid;
            augmentedRunner.subTreeGuid = original.subTreeGuid + Guid.NewGuid().ToString();
            augmentedRunner.skillCaster = original.skillCaster;
            augmentedRunner.targetSkill = original.targetSkill;
            var data = original.GetData();
            augmentedRunner.InitData(data.Length);

            for (int i = 0; i < data.Length; i++)
            {
                augmentedRunner.SetData(i, data[i]);
            }

            return augmentedRunner;
        }

        #region INPUT & DATA

        public override void InitData(NodeData runner)
        {
            runner.InitData(2);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1 };

            portInfo = new PortInfo[]
            {
                new PortInfo("repeatTimes", "Repeat Count", typeof(uint)),
                new PortInfo("repeatDelay", "Delay", typeof(float))
            };
        }

        #endregion

        #region OUTPUT

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            switch (index)
            {
                case 0:
                    portInfo = new PortInfo(string.Empty, "TotalCount", typeof(uint));
                    break;
                case 1:
                    portInfo = new PortInfo(string.Empty, "Current Iteration", typeof(uint));
                    break;
                default:
                    base.GetOutputInfo(index, out portInfo);    
                    break;
            }
        }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            if(dataPorts == null || dataPorts.Count != 2)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
                dataPorts.Add(new NodePort());
            }
        }

        #endregion
    }
}