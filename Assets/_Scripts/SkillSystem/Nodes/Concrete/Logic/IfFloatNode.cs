namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Splits data flow based on a condition </summary>
    [NodeInfo("execute", "If Float", "Logic/If Float", true, true)]
    public class IfFloatNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            var Data = runner.GetData();

            if (Data != null)
            {
                if(Data[0] != null)
                {
                    bool dataSwitch = (bool)Data[0];

                    var dataToSend = (dataSwitch) ? Data[1] : Data[2];

                    if(dataToSend == null)
                    {
                        Debug.LogError("Output data is null!");
                        runner.currentState = NodeData.State.ERROR;
                        return;
                    }

                    foreach (var output in dataPorts[0].outputs)
                    {
                        NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                        Node target = tree.GetNodeByGuid(output.guid);

                        target.SetData(output.index, dataToSend, nextRunner);
                    }
                }
                else
                {
                    Debug.LogError("Condition is null!");
                    runner.currentState = NodeData.State.ERROR;
                    return;
                }
            }
            else
            {
                Debug.LogError("Data is completely null!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            ExecuteChilds(runner);
        }

        #region INPUT

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[] 
            {
                new PortInfo(string.Empty, "Condition", typeof(bool)),
                new PortInfo(string.Empty, "True", typeof(float)),
                new PortInfo(string.Empty, "False", typeof(float))
            };
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }

        #endregion

        #region OUTPUT

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "Result", typeof(float));
        }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            if (dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>();
                dataPorts.Add(new NodePort());
            }
        }

        #endregion
    }
}