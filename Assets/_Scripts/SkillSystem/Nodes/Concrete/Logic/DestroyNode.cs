namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Destroys a target gameObject </summary>
    [NodeInfo("execute", "Destroy", "Logic/Destroy")]
    public class DestroyNode : Node
    {
        public override void Execute(NodeData runner)
        {
            if(!runner.DataNullCheck())
            {
                Debug.LogError("Target is null");
                return;
            }

            Destroy((GameObject)runner.GetData()[0]);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Target", typeof(GameObject))
            };
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }
    }
}