namespace Project.SkillSystem
{
    using RPG.Core;
    using RPG.Damage;
    using UnityEngine;

    /// <summary> Fires a damage event to be processed by the damage systems </summary>
    [NodeInfo("execute", "Send Damage Event", "Logic/Send Damage Event", true, false)]
    public class SendDamageEventNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if(runner.DataNullCheck())
            {
                var data = runner.GetData();

                DamageEventArgs args = new DamageEventArgs
                {
                    position = (Vector3)data[0],
                    areaOfEffect = (float)data[1],
                    damage = (int)data[2],
                    source = (GameEntity)data[3],
                    targets = (GameEntity[])data[4],
                };

                DamageCalculationSystem.SendDamageEvent(args);
            }
            else
            {
                Debug.LogError($"Some data is null!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            OnStop(runner);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2, 3, 4 };

            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "Position", typeof(Vector3)),
                new PortInfo(string.Empty, "Area Of Effect", typeof(float)),
                new PortInfo(string.Empty, "Damage", typeof(int)),
                new PortInfo(string.Empty, "Source", typeof(GameEntity)),
                new PortInfo(string.Empty, "Targets", typeof(GameEntity[]))
            };
        }

        protected override void InitOutputs() { }

        public override void InitData(NodeData runner)
        {
            runner.InitData(5);
        }
    }
}