namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Splits execution flow in two ways based if input is NULL or not </summary>
    [NodeInfo("execute", "NullCheck", "Logic/Null Check", true, true)]
    public class NullCheckNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if (runner.GetData() == null)
            {
                Debug.LogError("Data was not inited yet!");
                return;
            }

            int runIndex = -1;

            if (runner.GetData()[0] != null)
                runIndex = 0;
            else
                runIndex = 1;

            OnStop(runner);
            ExecuteChilds(runner, runIndex);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo(string.Empty, "Condition", typeof(GameObject)) };
        }

        public override void GetOutputExecutionInfo(int index, out PortInfo portInfo)
        {
            if (index == 0)
                portInfo = new PortInfo(string.Empty, "On True", null);
            else
                portInfo = new PortInfo(string.Empty, "On False", null);
        }

        protected override void InitOutputs()
        {
            if (executionPorts == null || executionPorts.Count != 2)
            {
                executionPorts = new List<NodePort>();
                executionPorts.Add(new NodePort());
                executionPorts.Add(new NodePort());
            }
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }
    }
}