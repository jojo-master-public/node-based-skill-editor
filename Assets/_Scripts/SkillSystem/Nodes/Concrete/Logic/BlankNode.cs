namespace Project.SkillSystem
{
    /// <summary> Empty node </summary>
    [NodeInfo("execute", "Blank", "Logic/Blank")]
    public class BlankNode : Node
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);
            OnStop(runner);
            ExecuteChilds(runner);
        }

        public override void InitData(NodeData runner) { }
    }
}