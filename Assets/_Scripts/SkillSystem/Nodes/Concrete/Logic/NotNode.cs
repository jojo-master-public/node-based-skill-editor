namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Splits execution flow based on a condition </summary>
    [NodeInfo("execute", "Not", "Logic/Not", true, true)]
    public class NotNode : Node
    {
        public override void Execute(NodeData runner)
        {
            if (!runner.DataNullCheck())
            {
                Debug.LogError("Condition is null");
                return;
            }

            var Data = runner.GetData();

            if (!(bool)Data[0])
                ExecuteChilds(runner, 0);
            else
                ExecuteChilds(runner, 1);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo(string.Empty, "Condition", typeof(bool)) };
        }

        public override void GetOutputExecutionInfo(int index, out PortInfo portInfo)
        {
            if (index == 0)
                portInfo = new PortInfo(string.Empty, "On False", null);
            else 
                portInfo = new PortInfo(string.Empty, "On True", null);
        }

        protected override void InitOutputs()
        {
            if(executionPorts == null || executionPorts.Count != 2)
            {
                executionPorts = new List<NodePort>();
                executionPorts.Add(new NodePort());
                executionPorts.Add(new NodePort());
            }
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }
    }
}