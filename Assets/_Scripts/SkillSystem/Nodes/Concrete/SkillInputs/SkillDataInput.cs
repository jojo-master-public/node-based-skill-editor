namespace Project.SkillSystem
{
    using System.Collections.Generic;

    /// <summary> Inputs data into a skill tree </summary>
    public abstract class SkillDataInput : DataNode
    {
        public string inputName;
    }

    public abstract class SkillDataInput<T> : SkillDataInput
    {
        public T defaultValue;



        public void SendData(Node target, object data, int index, NodeData nextRunner)
        {
            target.SetData(index, data, nextRunner);
        }

        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            T data = runner.targetSkill.GetValue<T>(inputName, out bool inputExists);

            if(!inputExists)
                data = defaultValue;

            if (dataPorts[0].outputs.Count > 0)
            {
                foreach (var output in dataPorts[0].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node outputNode = tree.GetNodeByGuid(output.guid);
                    SendData(outputNode, data, output.index, nextRunner);
                }
            }
            OnStop(runner);
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }

        protected override void InitOutputs()
        {
            if (dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>(1);
                dataPorts.Add(new NodePort());
            }
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo("defaultValue", "Value", typeof(T));
        }
    }
}