namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Returns exact cursor position in world space </summary>
    [NodeInfo("execute", "Cursor Position", "Info/Cursor/Position")]
    public class GetCursorPosition : SingleOutputNode
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            Vector3 position = runner.skillCaster.GetCursorPosition();

            foreach (var output in dataPorts[0].outputs)
            {
                NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);

                Node targetNode = tree.GetNodeByGuid(output.guid);
                
                targetNode?.SetData(output.index, position, nextRunner);
            }

            OnStop(runner);
            ExecuteChilds(runner);
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "position", typeof(Vector3));
        }

        public override void InitData(NodeData nextRunner) { }
    }
}