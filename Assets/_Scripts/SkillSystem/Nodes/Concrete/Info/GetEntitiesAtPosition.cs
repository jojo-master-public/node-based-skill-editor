namespace Project.SkillSystem
{
    using System.Linq;
    using UnityEngine;
    using RPG.Core;

    /// <summary> Returns entities at target position/radius/mask that have any colliders </summary>
    [NodeInfo("execute", "Entities At Pos", "Info/Entities At Position")]
    public class GetEntitiesAtPosition : DoubleOutputNode
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if (runner.DataNullCheck())
            {
                Vector3 position = (Vector3)runner.GetData()[0];
                float aoe = (float)runner.GetData()[1];
                LayerMask mask = (LayerMask)runner.GetData()[2];

                Collider[] entityColliders = Physics.OverlapSphere(position, aoe, mask, QueryTriggerInteraction.Ignore);

                GameEntity[] entities = entityColliders.Where(x => x.GetComponent<GameEntity>() != null).Select(x => x.GetComponent<GameEntity>()).ToArray();
                OutputData(0, entities, runner);
                OutputData(1, entityColliders.Length > 0, runner);
            }
            else
            {
                Debug.LogError("Some data is null!!!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            OnStop(runner);

            ExecuteChilds(runner);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[] 
            { 
                new PortInfo(string.Empty, "TargetPosition", typeof(Vector3)),
                new PortInfo(string.Empty, "Area Of Effect", typeof(float)),
                new PortInfo(string.Empty, "Entity Mask", typeof(LayerMask)),
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            if(index == 0)
                portInfo = new PortInfo(string.Empty, "Entities", typeof(GameEntity[]));
            else 
                portInfo = new PortInfo(string.Empty, "Hit Valid", typeof(bool));
        }

        public override void InitData(NodeData runner) { runner.InitData(3); }
    }
}