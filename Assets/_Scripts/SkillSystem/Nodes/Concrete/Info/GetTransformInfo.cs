namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Gets info about gameObject's transform </summary>
    [NodeInfo("variable", "Transform Info", "Info/Transform", false, false)]
    public class GetTransformInfo : Node
    {
        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData runner)
        {
            base.SetData(index, data, runner);

            if(runner.DataNullCheck())
            {
                var Data = runner.GetData();    
                Transform target = ((GameObject)Data[0]).transform;

                OutputData(0, target.position, runner);
                OutputData(1, target.rotation, runner);
                OutputData(2, target.forward, runner);
            }
        }

        public override void InitData(NodeData runner) { runner.InitData(1); }

        protected override void InitOutputs()
        {
            if (dataPorts != null && dataPorts.Count == 3)
                return;

            dataPorts = new List<NodePort>();
            dataPorts.Add(new NodePort());
            dataPorts.Add(new NodePort());
            dataPorts.Add(new NodePort());
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo(string.Empty, "Target", typeof(GameObject)) };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            switch (index)
            {
                case 0:
                    portInfo = new PortInfo(string.Empty, "Position", typeof(Vector3));
                    break;
                case 1:
                    portInfo = new PortInfo(string.Empty, "Rotation", typeof(Quaternion));
                    break;
                case 2:
                    portInfo = new PortInfo(string.Empty, "Forward", typeof(Vector3));
                    break;
                default:
                    base.GetOutputInfo(index, out portInfo);
                    break;
            }
        }
    }
}