namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using System.Reflection;
    using Project.Skills;
    using System.Linq;

    /// <summary> Gets info about an ISkillMonobehaviour that executes this skill tree using System.Reflection </summary>
    [NodeInfo("execute", "Character Info", "Info/Character", true, true)]
    public class GetCharacterInfo : Node
    {
        private static MethodInfo[] _methods;
        private static MethodInfo[] Methods
        {
            get
            {
                if(_methods == null || _methods.Length == 0)
                {
                    _methods = typeof(ISkillMonobehaviour)
                        .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                        .Where(x => x.ReturnType != typeof(void))
                        .ToArray();

                    List<MethodInfo> temp = new List<MethodInfo>(_methods.Length);

                    foreach (var m in _methods)
                    {
                        MethodSortAttribute sort = (MethodSortAttribute)m.GetCustomAttribute(typeof(MethodSortAttribute), true);

                        if(sort != null)
                            temp.Add(null);
                    }

                    foreach (var m in _methods)
                    {
                        MethodSortAttribute sort = (MethodSortAttribute)m.GetCustomAttribute(typeof(MethodSortAttribute), true);

                        if(sort != null)
                            temp[sort.SortOrder] = m;
                    }

                    _methods = temp.ToArray();
                }

                return _methods;
            }
        }

        public override void Execute(NodeData nodeData)
        {
            OnStart(nodeData);

            for (int i = 0; i < Methods.Length; i++)
            {
                foreach (var output in dataPorts[i].outputs)
                {
                    object data = Methods[i].Invoke(nodeData.skillCaster, null);

                    OutputData(i, data, nodeData);
                }
            }

            OnStop(nodeData);
            ExecuteChilds(nodeData);
        }

        public override void InitData(NodeData data) { }

        protected override void InitOutputs()
        {
            base.InitOutputs();

            if(dataPorts == null || dataPorts.Count != Methods.Length)
            {
                dataPorts = new List<NodePort>();

                foreach (var m in Methods)
                    dataPorts.Add(new NodePort());
            }
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, Methods[index].Name, Methods[index].ReturnType);
        }
    }
}