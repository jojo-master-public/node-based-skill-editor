namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Checks if any collisions are happening 
    /// inside target position/radius using a layer mask </summary>
    [NodeInfo("execute", "CheckCollision", "Collision/CheckCollision")]
    public class CheckCollisionNode : SingleOutputNode
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            if(runner.DataNullCheck())
            {
                Vector3 position = (Vector3)runner[0];
                float entityRadius = (float)runner[1];
                LayerMask collisionMask = (LayerMask)runner[2];

                Collider[] colliders = Physics.OverlapSphere(position, entityRadius, collisionMask, QueryTriggerInteraction.Ignore);

                OutputData(0, colliders != null && colliders.Length > 0, runner);
            }
            else
            {
                Debug.LogError("Inputs are null!");
                runner.currentState = NodeData.State.ERROR;
                return;
            }

            OnStop(runner);
            ExecuteChilds(runner);
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(3);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0, 1, 2 };
            portInfo = new PortInfo[]
            {
                new PortInfo(string.Empty, "TargetPosition", typeof(Vector3)),
                new PortInfo(string.Empty, "EntityRadius", typeof(float)),
                new PortInfo(string.Empty, "CollisionMask", typeof(LayerMask))
            };
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "CollisionValid", typeof(bool));
        }
    }
}