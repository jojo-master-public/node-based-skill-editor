namespace Project.SkillSystem
{
    using UnityEngine;

    /// <summary> Returns vector pointing from a character's gameObject towards player cursor </summary>
    [NodeInfo("execute", "Cursor Direction", "Info/Cursor/Direction")]
    public class GetDirectionToCursor : SingleOutputNode
    {
        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            Vector3 characterPosition = runner.skillCaster.GetCharacterGameobject().transform.position;
            Vector3 cursorPosition = runner.skillCaster.GetCursorPosition();

            Vector3 direction = cursorPosition - characterPosition;
            direction.y = 0;
            direction.Normalize();

            OnStop(runner);

            OutputData(0, direction, runner);
            ExecuteChilds(runner);
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo(string.Empty, "position", typeof(Vector3));
        }

        public override void InitData(NodeData runner) { }
    }
}