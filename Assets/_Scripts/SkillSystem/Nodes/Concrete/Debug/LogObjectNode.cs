using UnityEngine;

namespace Project.SkillSystem
{
    /// <summary> Logs an object to the console when it is received on any input</summary>
    [NodeInfo("execute", "Log Object", "Debug/Log Object", false, false)]
    public class LogObjectNode : Node
    {
        public OutputType objectType;



        public override void Execute(NodeData runner) { }

        public override void SetData(int index, object data, NodeData nodeData)
        {
            nodeData.SetData(index, data);

            Debug.Log($"Got {data} from {index} input");
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }

        public override void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            index = new int[] { 0 };
            portInfo = new PortInfo[] { new PortInfo(string.Empty, "Input", OutputNode.GetOutputType(objectType)) };
        }

        protected override void InitOutputs() { }
    }
}