using UnityEngine;

namespace Project.SkillSystem
{
    /// <summary> Outputs a text message to the console </summary>
    [NodeInfo("execute", "Log", "Debug/Log Message", true, false)]
    public class LogNode : Node
    {
        public string message = "New message";



        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            Debug.Log($"[{name}.Execute] message: {message}");

            OnStop(runner);
            ExecuteChilds(runner);
        }

        public override void InitData(NodeData runner) { }
        protected override void InitOutputs() { }
    }
}