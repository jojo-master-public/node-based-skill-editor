using Project.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Project.SkillSystem
{
    /// <summary>
    /// Editor info about a node, mostly for sorting and quicksearch funcionality
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class NodeInfoAttribute : Attribute
    {
        public readonly string Tag;
        public readonly string NodeDisplayName;
        public readonly string MenuName;
        public readonly bool InputExecution;
        public readonly bool OutputExecution;

        public NodeInfoAttribute(string tag, string nodeDisplayName, string menuName, bool inputExecution = true, bool outputExecution = true)
        {
            this.Tag = tag;
            this.NodeDisplayName = nodeDisplayName;
            this.InputExecution = inputExecution;
            this.OutputExecution = outputExecution;
            this.MenuName = menuName;
        }
    }

    /// <summary>  input/output info about data this port transfers </summary>
    public struct PortInfo
    {
        public readonly Type type;
        public readonly string fieldName;
        public readonly string displayName;

        public PortInfo(string fieldName, string displayName, Type type)
        {
            this.fieldName = fieldName;
            this.displayName = displayName;
            this.type = type;
        }
    }

    /// <summary> info about port index and guid of a node this port connects to </summary>
    [Serializable]
    public class OutputInfo
    {
        public string guid;
        public int index;

        public OutputInfo(string guid, int index)
        {
            this.guid = guid;
            this.index = index;
        }

        public OutputInfo(OutputInfo source)
        {
            this.guid = source.guid;
            this.index = source.index;
        }
    }

    /// <summary> Output node port with one or multiple connections </summary>
    [Serializable]
    public class NodePort
    {
        // nodes this port outputs into
        public List<OutputInfo> outputs;



        public NodePort()
        {
            outputs = new List<OutputInfo>();
        }

        public NodePort(string inputGuid, int inputIndex)
        {
            outputs = new List<OutputInfo>(1);

            OutputInfo output = new OutputInfo(inputGuid, inputIndex);

            outputs.Add(output);
        }

        public NodePort(NodePort port)
        {
            outputs = new List<OutputInfo>();

            foreach (var output in port.outputs)
            {
                outputs.Add(new OutputInfo(output));
            }
        }
    }

    /// <summary>
    /// Main node class, nodes DO NOT hold data on their own, 
    /// each node executes logic and transfers data on targets
    /// </summary>
    public abstract class Node : ScriptableObject
    {
        // fired up when node subgraph ports have changed to recreate ports
        public static event Action<Node> onValidateNode;

        // is this node expanded/collapsed
        [HideInInspector] public bool isExpanded = true;

        // unique node guid
        [HideInInspector] public string guid;

        // node position inside the editor window
        [HideInInspector] public Vector2 position;

        // ports that this node will transfer the execution logic
        [HideInInspector] public List<NodePort> executionPorts;

        // ports that this node will only transfer data to
        [HideInInspector] public List<NodePort> dataPorts;

        // marks this node as main entry point of skill tree
        public bool isRoot;



        #region EXTERNAL DATA

        // skill tree this node is serialized on
        protected SkillTree tree;

        #endregion

        #region PUBLIC METHODS

        public void InitNode(SkillTree tree)
        {
            this.tree = tree;
            InitOutputs();
        }

        public void ConnectExecutePort(Node node, int outputIndex, int inputIndex = 0)
        {
            NodePort output = executionPorts[outputIndex];

            OutputInfo connection = new OutputInfo(node.guid, inputIndex);

            output.outputs.Add(connection);
        }

        public void DisconnectExecutePort(Node node, int outputIndex)
        {
            OutputInfo connection = executionPorts[outputIndex].outputs.First(x => x.guid == node.guid);

            executionPorts[outputIndex].outputs.Remove(connection);
        }

        public void ConnectDataPort(Node node, int outputIndex, int inputIndex)
        {
            NodePort output = dataPorts[outputIndex];

            OutputInfo connection = new OutputInfo(node.guid, inputIndex);

            output.outputs.Add(connection);
        }

        public void DisconnectDataPort(Node node, int outputIndex)
        {
            OutputInfo connection = dataPorts[outputIndex].outputs.First(x => x.guid == node.guid);

            dataPorts[outputIndex].outputs.Remove(connection);
        }

        public bool HasExecuteConnections()
        {
            return executionPorts != null && executionPorts.Count > 0;
        }

        #endregion

        #region PROTECTED METHODS

        // transfers execution logic on a certain port
        protected void ExecuteChilds(NodeData data, int index)
        {
            foreach (var output in executionPorts[index].outputs)
            {
                NodeData nextRunner = data.skillCaster.GetDataForNextNode(data, output.guid);

                Node outputNode = tree.GetNodeByGuid(output.guid);

                outputNode.Execute(nextRunner);
            }
        }

        protected void ExecuteChilds(NodeData data)
        {
            for (int i = 0; i < executionPorts.Count; i++)
            {
                ExecuteChilds(data, i);
            }
        }

        #endregion

        #region ABSTRACT METHODS

        public abstract void Execute(NodeData runner);
        public abstract void InitData(NodeData runner);

        #endregion

        #region VIRTUAL METHODS

        // sets data on this node
        public virtual void SetData(int index, object data, NodeData nodeData)
        { 
            InitData(nodeData);

            GetInputInfo(out int[] indexes, out PortInfo[] ports);

            var convertedData = NodeDataUtils.Convert(data, ports[index].type);

            nodeData.SetData(index, convertedData); 
        }

        // sends data to output data ports
        public virtual void OutputData(int portIndex, object data, NodeData runner)
        {
            foreach (var output in dataPorts[portIndex].outputs)
            {
                NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                Node target = tree.GetNodeByGuid(output.guid);
                target.SetData(output.index, data, nextRunner);
            }
        }

        // override this in child classes to specify INPUT connections
        public virtual void GetInputInfo(out int[] index, out PortInfo[] portInfo)
        {
            // most basic functionality a node may not contain any inputs (eg. comment node)
            index = new int[0];
            portInfo = new PortInfo[0];
        }

        // override this in child classes to specify OUTPUT data connections
        public virtual void GetOutputInfo(int index, out PortInfo portInfo)
        {
            // most basic functionality a node may not contain any data outputs (eg. debug log node)

            portInfo = new PortInfo(string.Empty, string.Empty, null);
        }

        // override this in child classes to specify OUTPUT execution connections
        public virtual void GetOutputExecutionInfo(int index, out PortInfo portInfo)
        {
            // most basic functionality a node may contain one or several execution outputs (eg. switch node)
            portInfo = new PortInfo(string.Empty, "Output", null);
        }

        // validate this node's input/output ports in the editor window
        public virtual void ValidateNode()
        {
            onValidateNode?.Invoke(this);
        }

        // fired up when node STARTS execution
        protected virtual void OnStart(NodeData runner)
        {
            runner.currentState = NodeData.State.RUNNING;
        }

        // fired up when node STOPS execution
        protected virtual void OnStop(NodeData runner)
        {
            runner.currentState = NodeData.State.FINISHED;
        }

        // override this in child classes to specify output ports for serialization
        protected virtual void InitOutputs()
        {
            // most nodes have one execution output connection
            if (executionPorts == null || executionPorts.Count == 0)
            {
                executionPorts = new List<NodePort>();
                executionPorts.Add(new NodePort());
            }
        }

        #endregion
    }
}