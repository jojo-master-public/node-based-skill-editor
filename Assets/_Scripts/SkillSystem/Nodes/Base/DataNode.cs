namespace Project.SkillSystem
{
    using System.Collections.Generic;

    /// <summary>
    /// Node that only contains/transfers data
    /// </summary>
    public abstract class DataNode : Node { }

    public abstract class DataNode<T> : DataNode
    {
        public T value;



        public void SendData(Node target, object data, int index, NodeData nextRunner)
        {
            target.SetData(index, data, nextRunner);
        }

        public override void Execute(NodeData runner)
        {
            OnStart(runner);

            T data;

            if (runner.DataNullCheck())
                data = (T)runner[0];
            else
                data = value;

            if (dataPorts[0].outputs.Count > 0)
            {
                foreach (var output in dataPorts[0].outputs)
                {
                    NodeData nextRunner = runner.skillCaster.GetDataForNextNode(runner, output.guid);
                    Node outputNode = tree.GetNodeByGuid(output.guid);
                    SendData(outputNode, data, output.index, nextRunner);
                }
            }

            OnStop(runner);
        }

        public override void InitData(NodeData runner)
        {
            runner.InitData(1);
        }

        public override void GetOutputInfo(int index, out PortInfo portInfo)
        {
            portInfo = new PortInfo("value", "Value", typeof(T));
        }

        protected override void InitOutputs()
        {
            if (dataPorts == null || dataPorts.Count != 1)
            {
                dataPorts = new List<NodePort>(1);
                dataPorts.Add(new NodePort());
            }
        }
    }
}