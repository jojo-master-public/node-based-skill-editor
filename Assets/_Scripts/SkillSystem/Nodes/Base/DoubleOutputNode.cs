using System.Collections.Generic;

namespace Project.SkillSystem
{
    /// <summary> Node that has exactly TWO output execution connection </summary>
    public abstract class DoubleOutputNode : Node
    {
        protected override void InitOutputs()
        {
            base.InitOutputs();

            if (dataPorts != null && dataPorts.Count == 2)
                return;

            dataPorts = new List<NodePort>();
            dataPorts.Add(new NodePort());
            dataPorts.Add(new NodePort());
        }
    }
}