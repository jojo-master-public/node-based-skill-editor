using System.Collections.Generic;

namespace Project.SkillSystem
{
    /// <summary> Node that has exactly ONE output execution connection </summary>
    public abstract class SingleOutputNode : Node
    {
        protected override void InitOutputs()
        {
            base.InitOutputs();

            if (dataPorts != null && dataPorts.Count == 1)
                return;

            dataPorts = new List<NodePort>();
            dataPorts.Add(new NodePort());
        }
    }
}