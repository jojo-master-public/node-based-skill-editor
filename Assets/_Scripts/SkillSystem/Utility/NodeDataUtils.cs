namespace Project.SkillSystem
{
    using RPG.Core;
    using System;
    using UnityEditor.Experimental.GraphView;
    using UnityEngine;

    /// <summary>
    /// Helper class for converting data structures between nodes (eg. int to float etc.)
    /// </summary>
    public static class NodeDataUtils
    {
        private static CustomNodeAdapter adapter = new CustomNodeAdapter();

        #region CAST CHECKS

        public static bool CanCastToInt(this CustomNodeAdapter adapter, PortSource<float> a, PortSource<int> b)
        {
            return true;
        }

        public static bool CanCastToInt(this CustomNodeAdapter adapter, PortSource<uint> a, PortSource<int> b)
        {
            return true;
        }

        public static bool CanCastToVector(this CustomNodeAdapter adapter, PortSource<float> a, PortSource<Vector3> b)
        {
            return true;
        }

        public static bool CanCastToVector(this CustomNodeAdapter adapter, PortSource<int> a, PortSource<Vector3> b)
        {
            return true;
        }

        public static bool CanCastToVector(this CustomNodeAdapter adapter, PortSource<uint> a, PortSource<Vector3> b)
        {
            return true;
        }

        public static bool CanCastToVector(this CustomNodeAdapter adapter, PortSource<Transform> a, PortSource<Vector3> b)
        {
            return true;
        }

        public static bool CanCastToVector(this CustomNodeAdapter adapter, PortSource<GameObject> a, PortSource<Vector3> b)
        {
            return true;
        }

        public static bool CanCastToVector(this CustomNodeAdapter adapter, PortSource<GameEntity> a, PortSource<Vector3> b)
        {
            return true;
        }

        public static bool CanCastToQuaternion(this CustomNodeAdapter adapter, PortSource<Transform> a, PortSource<Quaternion> b)
        {
            return true;
        }

        public static bool CanCastToQuaternion(this CustomNodeAdapter adapter, PortSource<GameObject> a, PortSource<Quaternion> b)
        {
            return true;
        }

        public static bool CanCastToQuaternion(this CustomNodeAdapter adapter, PortSource<GameEntity> a, PortSource<Quaternion> b)
        {
            return true;
        }

        public static bool CanCastToUint(this CustomNodeAdapter adapter, PortSource<int> a, PortSource<uint> b)
        {
            return true;
        }

        public static bool CanCastToUint(this CustomNodeAdapter adapter, PortSource<float> a, PortSource<uint> b)
        {
            return true;
        }

        public static bool CanCastToFloat(this CustomNodeAdapter adapter, PortSource<int> a, PortSource<float> b)
        {
            return true;
        }

        public static bool CanCastToFloat(this CustomNodeAdapter adapter, PortSource<uint> a, PortSource<float> b)
        {
            return true;
        }

        public static bool CanCastToTransform(this CustomNodeAdapter adapter, PortSource<GameObject> a, PortSource<Transform> b)
        {
            return true;
        }

        public static bool CanCastToTransform(this CustomNodeAdapter adapter, PortSource<GameEntity> a, PortSource<Transform> b)
        {
            return true;
        }

        public static bool CanCastToGameobject(this CustomNodeAdapter adapter, PortSource<GameEntity> a, PortSource<GameObject> b)
        {
            return true;
        }

        public static bool CanCastToGameobject(this CustomNodeAdapter adapter, PortSource<Transform> a, PortSource<GameObject> b)
        {
            return true;
        }

        #endregion

        #region CONVERTERS

        // TODO: Add converters to ALL data types!

        [TypeAdapter]
        public static Vector3 CastToVector(float input)
        {
            return new Vector3(input, input, input);
        }

        [TypeAdapter]
        public static Vector3 CastToVector(int input)
        {
            return new Vector3(input, input, input);
        }

        [TypeAdapter]
        public static Vector3 CastToVector(uint input)
        {
            return new Vector3(input, input, input);
        }

        [TypeAdapter]
        public static Vector3 CastToVector(Transform input)
        {
            return input.position;
        }

        [TypeAdapter]
        public static Vector3 CastToVector(GameObject input)
        {
            return CastToVector(input.transform);
        }

        [TypeAdapter]
        public static Vector3 CastToVector(GameEntity input)
        {
            return CastToVector(input.transform);
        }

        [TypeAdapter]
        public static Quaternion CastToRotation(Transform input)
        {
            return input.rotation;
        }

        [TypeAdapter]
        public static Quaternion CastToRotation(GameObject input)
        {
            return CastToRotation(input.transform);
        }

        [TypeAdapter]
        public static Quaternion CastToRotation(GameEntity input)
        {
            return CastToRotation(input.transform);
        }

        [TypeAdapter]
        public static Transform CastToTransform(GameEntity input)
        {
            return input.transform;
        }

        [TypeAdapter]
        public static Transform CastToTransform(GameObject input)
        {
            return input.transform;
        }

        [TypeAdapter]
        public static GameObject CastToGameobject(GameEntity input)
        {
            return input.gameObject;
        }

        [TypeAdapter]
        public static GameObject CastToGameobject(Transform input)
        {
            return input.gameObject;
        }

        [TypeAdapter]
        public static int CastToInt(float input)
        {
            return Mathf.RoundToInt(input);
        }

        [TypeAdapter]
        public static int CastToInt(uint input)
        {
            return (int)input;
        }

        [TypeAdapter]
        public static uint CastToUint(float input)
        {
            return CastToUint(Mathf.RoundToInt(input));
        }

        [TypeAdapter]
        public static uint CastToUint(int input)
        {
            if (input >= 0)
                return (uint)input;
            else
                return 0;
        }

        [TypeAdapter]
        public static float CastToFloat(uint input)
        {
            return input;
        }

        [TypeAdapter]
        public static float CastToFloat(int input)
        {
            return input;
        }

        public static T Convert<T>(object data)
        {
            return (T)Convert(data, typeof(T));
        }

        public static object Convert(object data, Type type)
        {
            if (data == null || type == null)
                return null;

            if (data.GetType() == type || data.GetType().IsSubclassOf(type))
                return data;

            var ta = adapter.GetTypeAdapter(data.GetType(), type);

            if (ta == null)
                return null;

            return ta.Invoke(adapter, new object[] { data });
        }

        #endregion
    }
}