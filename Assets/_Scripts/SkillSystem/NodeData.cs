using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Skills;
using Project.SkillSystem;
using RPG.Damage;
using System;

public class NodeDataContainer : IPoolable
{
    public event Action<IPoolable> onReturnToPool;

    public string guid;
    public Dictionary<string, Dictionary<string, NodeData>> nodeData = new Dictionary<string, Dictionary<string, NodeData>>();

    public void ReturnToPool() { }

    public NodeData GetData(string nodeGuid, string subTreeGuid)
    {
        if(nodeData.ContainsKey(subTreeGuid))
        {
            if (nodeData[subTreeGuid].ContainsKey(nodeGuid))
                return nodeData[subTreeGuid][nodeGuid];
            else
            {
                NodeData data = new NodeData();
                data.subTreeGuid = subTreeGuid;
                data.containerGuid = guid;
                nodeData[subTreeGuid].Add(nodeGuid, data);

                return data;
            }
        }
        else
        {
            nodeData.Add(subTreeGuid, new Dictionary<string, NodeData>());

            NodeData data = new NodeData();
            data.subTreeGuid = subTreeGuid;
            data.containerGuid = guid;
            nodeData[subTreeGuid].Add(nodeGuid, data);

            return data;
        }
    }
}

public class NodeData
{
    public string containerGuid;
    public string subTreeGuid = string.Empty;
    public ISkillMonobehaviour skillCaster;
    public Skill targetSkill;
    public enum State
    {
        WAITING,
        RUNNING,
        FINISHED,
        ERROR
    }

    public State currentState = State.WAITING;

    private object[] data;

    public object this[int indexer]
    {
        get { return data[indexer]; }
    }



    public bool DataNullCheck()
    {
        if (data == null)
            return false;

        foreach (var d in data)
        {
            if (d == null)
                return false;
        }

        return true;
    }

    public object[] GetData()
    {
        return data;
    }

    public void SetData(int index, object data)
    {
        this.data[index] = data;
    }

    public void InitData(int length)
    {
        if (data == null)
            data = new object[length];
    }
}
