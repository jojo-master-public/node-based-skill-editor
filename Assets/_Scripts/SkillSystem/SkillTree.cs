namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;

    /// <summary>
    /// A containere for logic and data nodes that can be executed on target objects
    /// </summary>
    [CreateAssetMenu(fileName = "SkillTree_", menuName = "SkillSystem/New Node Tree")]
    public class SkillTree : ScriptableObject
    {
        public List<Node> nodes = new List<Node>();



        #region MAIN ENTRY POINT

        // Executes this skill with root data container
        public void Execute(NodeData data)
        {
            List<Node> nodesList = FindDataNodes();

            // Send data from DataNodes first
            foreach (var node in nodesList)
            {
                DataNode dNode = node as DataNode;

                if(dNode != null)
                {
                    if (dNode is InputDataNode)
                        continue;

                    NodeData dataOfNextNode = data.skillCaster.GetDataForNextNode(data, dNode.guid);
                    node.Execute(dataOfNextNode);
                }
            }

            nodesList = FindRootNodes();

            // then start executing logic nodes
            foreach (Node node in nodesList)
            {
                NodeData nextRunner = data.skillCaster.GetDataForNextNode(data, node.guid);
                node.Execute(nextRunner);
            }
        }

        #endregion

        #region PUBLIC

        // initialize every node in this tree
        public void Init()
        {
            foreach (var node in nodes)
            {
                node.InitNode(this);
            }
        }

        public Node GetNodeByGuid(string guid)
        {
            if (nodes.Any(x => x.guid == guid))
                return nodes.First(x => x.guid == guid);

            return null;
        }

        #endregion

        #region PRIVATE

        // find nodes that have no input connections that are marked as "root"
        private List<Node> FindRootNodes()
        {
            List<Node> rootNodes = new List<Node>();

            foreach (var node in nodes)
            {
                if (node.isRoot)
                    rootNodes.Add(node);
            }

            return rootNodes;
        }

        // find nodes that only provide data
        private List<Node> FindDataNodes()
        {
            return nodes.Where(x => x is DataNode).ToList();
        }

        #endregion
    }
}