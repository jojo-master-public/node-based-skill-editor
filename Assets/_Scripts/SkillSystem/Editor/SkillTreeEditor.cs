namespace Project.SkillSystem
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.UIElements;
    using UnityEditor.Callbacks;
    using UnityEditor.UIElements;
    using Project.Skills;
    using System.Collections.Generic;
    using System.Linq;

    public class SkillTreeEditor : EditorWindow
    {
        private SkillTreeView treeView;
        private InspectorView inspectorView;
        private Toolbar toolbar;
        private string containerGuid;
        private string subTreeGuid;
        private static Dictionary<string, Dictionary<string, List<string>>> containers;
        private static ISkillMonobehaviour skillCaster;
        private bool windowLocked;



        [MenuItem("SkillTreeEditor/New Window")]
        public static void OpenWindow()
        {
            SkillTreeEditor wnd = GetWindow<SkillTreeEditor>();

            if (wnd == null)
                return;

            wnd.titleContent = new GUIContent("SkillTreeEditor");
            wnd.position = new Rect(160, 120, 1000, 750);
        }

        [MenuItem("SkillTreeEditor/Reset Node Connections")]
        public static void ResetNodesConnections()
        {
            SkillTree tree = Selection.activeObject as SkillTree;

            if (tree != null)
            {
                tree.nodes.ForEach(x =>
                {
                    foreach (var port in x.executionPorts)
                        port.outputs.Clear();
                    foreach (var port in x.dataPorts)
                        port.outputs.Clear();
                });
            }
        }

        [MenuItem("SkillTreeEditor/Init All Nodes")]
        public static void InitAllNodes()
        {
            string[] treeAssets = AssetDatabase.FindAssets("SkillTree_");
            string[] subTreeAssets = AssetDatabase.FindAssets("SubTree_");

            foreach (var treePath in treeAssets)
            {
                SkillTree loadedTree = AssetDatabase.LoadAssetAtPath<SkillTree>(AssetDatabase.GUIDToAssetPath(treePath));

                if (loadedTree != null)
                    loadedTree.nodes.ForEach(n => n.InitNode(loadedTree));
            }

            foreach (var treePath in subTreeAssets)
            {
                SkillTree loadedTree = AssetDatabase.LoadAssetAtPath<SkillTree>(AssetDatabase.GUIDToAssetPath(treePath));

                if (loadedTree != null)
                    loadedTree.nodes.ForEach(n => n.InitNode(loadedTree));
            }
        }

        [MenuItem("SkillTreeEditor/Regenerate Guids")]
        public static void RegenerateNodeGuids()
        {
            SkillTree tree = Selection.activeObject as SkillTree;

            if (tree == null)
                return;

            Dictionary<string, string> oldGuidToNewGuid = new Dictionary<string, string>();

            foreach (var node in tree.nodes)
            {
                string guid = GUID.Generate().ToString();
                oldGuidToNewGuid.Add(node.guid, guid);
                node.guid = guid;
            }

            foreach (var node in tree.nodes)
            {
                foreach (var executionOutputPort in node.executionPorts)
                {
                    foreach (var executionOutput in executionOutputPort.outputs)
                    {
                        executionOutput.guid = oldGuidToNewGuid[executionOutput.guid];
                    }
                }

                foreach (var dataPort in node.dataPorts)
                {
                    foreach (var output in dataPort.outputs)
                    {
                        output.guid = oldGuidToNewGuid[output.guid];
                    }
                }
            }

            SkillTreeEditor wnd = GetWindow<SkillTreeEditor>();
            SkillTreeView treeView = wnd.rootVisualElement.Q<SkillTreeView>();

            treeView.nodes.ForEach(n =>
            {
                NodeView view = n as NodeView;

                VisualElement debugContainer = view.Q<VisualElement>("debug-container");
                if (debugContainer != null)
                {
                    Label guidLabel = debugContainer.Q<Label>("guid");
                    guidLabel.text = "guid: " + SkillTreeEditor.CropGuid(view.node.guid);
                }
            });
        }



        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceId, int line)
        {
            if (Selection.activeObject is SkillTree || Selection.activeObject is Skill)
            {
                OpenWindow();
                return true;
            }
            return false;
        }

        public void CreateGUI()
        {
            // Each editor window contains a root VisualElement object
            VisualElement root = rootVisualElement;
            root.Clear();

            // Import UXML
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/UXML/SkillTreeEditor.uxml");
            visualTree.CloneTree(root);

            // A stylesheet can be added to a VisualElement.
            // The style will be applied to the VisualElement and all of its children.
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/USS/SkillTreeEditor.uss");
            root.styleSheets.Add(styleSheet);

            treeView = root.Q<SkillTreeView>();
            treeView.Init(this);

            inspectorView = root.Q<InspectorView>();
            toolbar = root.Q<Toolbar>();

            treeView.onNodeSelected += OnNodeSelectionChanged;
            ToolbarToggle debugToggle = toolbar.Q<ToolbarToggle>("debug-toggle");
            debugToggle.RegisterValueChangedCallback(OnDebugToggle);

            ToolbarToggle lockToggle = toolbar.Q<ToolbarToggle>("lock-toggle");
            lockToggle.RegisterValueChangedCallback(OnLockToggled);

            OnSelectionChange();
        }

        public static string CropGuid(string guid)
        {
            return guid.Substring(0, 8);
        }

        public static Node CreateNode(System.Type nodeType, SkillTree tree, Vector2 position)
        {
            Node newNode = CreateInstance(nodeType) as Node;
            newNode.name = nodeType.ToString();
            newNode.guid = GUID.Generate().ToString();
            newNode.position = position;
            newNode.InitNode(tree);

            Undo.RecordObject(tree, "SkillTree (Add Node)");

            tree.nodes.Add(newNode);

            if (!Application.isPlaying)
                AssetDatabase.AddObjectToAsset(newNode, tree);

            Undo.RegisterCreatedObjectUndo(newNode, "SkillTree (Add Node)");
            AssetDatabase.SaveAssets();

            return newNode;
        }

        public static void DeleteNode(Node target, SkillTree tree)
        {
            Undo.RecordObject(tree, "SkillTree (Delete Node)");

            tree.nodes.Remove(target);
            //AssetDatabase.RemoveObjectFromAsset(target);
            Undo.DestroyObjectImmediate(target);

            AssetDatabase.SaveAssets();
        }

        public static void ConnectExecuteNodes(Node from, Node to, int fromIndex, int toIndex)
        {
            Undo.RecordObject(from, "SkillTree (Add Connection)");

            from.ConnectExecutePort(to, fromIndex, toIndex);

            EditorUtility.SetDirty(from);
        }

        public static void DisconnectExecuteNode(Node from, Node to, int fromIndex)
        {
            if (from == null)
                return;

            Undo.RecordObject(from, "SkillTree (Remove Connection)");

            from.DisconnectExecutePort(to, fromIndex);

            EditorUtility.SetDirty(from);
        }

        public static void ConnectDataNodes(Node from, Node to, int fromIndex, int toIndex)
        {
            Undo.RecordObject(from, "SkillTree (Add Connection)");

            from.ConnectDataPort(to, fromIndex, toIndex);

            EditorUtility.SetDirty(from);
        }

        public static void DisconnectDataNode(Node from, Node to, int fromIndex)
        {
            if (fromIndex == -1 || from == null)
                return;

            Undo.RecordObject(from, "SkillTree (Remove Connection)");

            from.DisconnectDataPort(to, fromIndex);

            EditorUtility.SetDirty(from);
        }

        private void OnPlayModeStateChanged(PlayModeStateChange playModeStateChange)
        {
            switch (playModeStateChange)
            {
                case PlayModeStateChange.EnteredEditMode:
                    containers = null;
                    PlayerSkillStateMachine.onContainerAdded -= PlayerSkillStateMachine_onContainerAdded;
                    CreateGUI();
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    containers = new Dictionary<string, Dictionary<string, List<string>>>();

                    OnSelectionChange();

                    PlayerSkillStateMachine.onContainerAdded += PlayerSkillStateMachine_onContainerAdded;
                    CreateGUI();
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    break;
            }
        }

        private void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            CreateGUI();
        }

        private void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
        }

        private void OnSelectionChange()
        {
            PlayerSkillStateMachine treeRunner = null;

            if(Selection.activeGameObject != null) 
                treeRunner = Selection.activeGameObject.GetComponent<PlayerSkillStateMachine>();

            if (treeRunner == null)
            {
                treeRunner = FindObjectOfType<PlayerSkillStateMachine>();
            }

            SkillTree tree = Selection.activeObject as SkillTree;

            if (tree == null)
            {
                if (Selection.activeGameObject)
                {
                    if (treeRunner != null && treeRunner.ActiveTree != null)
                    {
                        tree = treeRunner.ActiveTree;
                        skillCaster = treeRunner;

                        FillContainers();
                    }
                }
                else if (Selection.activeObject)
                {
                    Skill selectedSkill = Selection.activeObject as Skill;

                    if (selectedSkill != null)
                        tree = selectedSkill.TargetSkillTree;
                }
            }

            if (tree != null && !windowLocked)
            {
                if (Application.isPlaying)
                    treeView.PopulateView(tree);

                if (AssetDatabase.CanOpenForEdit(AssetDatabase.GetAssetPath(tree)))
                    treeView.PopulateView(tree);
            }

            if (treeRunner != null)
            {
                skillCaster = treeRunner;

                FillContainers();
            }
        }

        private void OnInspectorUpdate()
        {
            treeView?.UpdateNodeStates(containerGuid, subTreeGuid);
        }

        private void OnDebugToggle(ChangeEvent<bool> callback)
        {
            treeView.SetDebug(callback.newValue);
        }

        private void OnLockToggled(ChangeEvent<bool> callback)
        {
            windowLocked = callback.newValue;
        }

        private void FillContainers()
        {
            ToolbarMenu menu = toolbar.Q<ToolbarMenu>();
            int count = menu.menu.MenuItems().Count;

            for (int i = 0; i < count; i++)
            {
                menu.menu.RemoveItemAt(0);
            }

            if (skillCaster == null)
                return;

            string skillName = treeView.Tree.name;
            string[] containerGuids = GetContainerGuids(skillName);

            foreach (var guid in containerGuids)
            {
                string[] subTreeGuids = GetSubtreeGuids(skillName, guid);

                foreach (var subTreeGuid in subTreeGuids)
                {
                    menu.menu.AppendAction($"Select: {skillName}/{CropGuid(guid)}/{CropGuid(subTreeGuid)}", a => SelectContainer(guid, subTreeGuid));
                }
            }
        }

        private void SelectContainer(string containerGuid, string subTreeGuid)
        {
            this.containerGuid = containerGuid;
            this.subTreeGuid = subTreeGuid;
        }

        private void PlayerSkillStateMachine_onContainerAdded(string skillName, string containerGuid, string subtreeGuid)
        {
            if (!containers.ContainsKey(skillName))
                containers.Add(skillName, new Dictionary<string, List<string>>());

            if (!containers[skillName].ContainsKey(containerGuid))
                containers[skillName].Add(containerGuid, new List<string>());

            containers[skillName][containerGuid].Add(subtreeGuid);

            OnSelectionChange();

            SelectContainer(containerGuid, subtreeGuid);
        }

        public static NodeData GetNodeDataByGuid(string containerGuid, string subTreeGuid, string nodeGuid)
        {
            if (skillCaster == null)
                return null;

            if (string.IsNullOrEmpty(containerGuid) || !skillCaster.DataContainers.ContainsKey(containerGuid))
                return null;

            if (string.IsNullOrEmpty(subTreeGuid) 
                || skillCaster.DataContainers[containerGuid].nodeData == null 
                || !skillCaster.DataContainers[containerGuid].nodeData.ContainsKey(subTreeGuid))
                return null;

            if (string.IsNullOrEmpty(nodeGuid) || !skillCaster.DataContainers[containerGuid].nodeData[subTreeGuid].ContainsKey(nodeGuid))
                return null;

            return skillCaster.DataContainers[containerGuid].nodeData[subTreeGuid][nodeGuid];
        }

        public static string[] GetContainerGuids(string targetSkillName)
        {
            if (!string.IsNullOrEmpty(targetSkillName) && containers != null && containers.ContainsKey(targetSkillName))
                return containers[targetSkillName].Keys.ToArray();
            else
                return new string[0];

        }

        public static string[] GetSubtreeGuids(string targetSkillName, string containerGuid)
        {
            if (!string.IsNullOrEmpty(containerGuid) 
                && !string.IsNullOrEmpty(targetSkillName) 
                && containers != null 
                && containers.ContainsKey(targetSkillName)
                && containers[targetSkillName].ContainsKey(containerGuid))
                return containers[targetSkillName][containerGuid].ToArray();
            else
                return new string[0];

        }

        private void OnNodeSelectionChanged(NodeView selectedNode)
        {
            inspectorView.UpdateSelection(selectedNode);
        }
    }
}