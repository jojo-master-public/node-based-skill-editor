using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Project.SkillSystem.Utility
{
    public class SkillTreeSearchProvider : ScriptableObject, ISearchWindowProvider
    {
        private SkillTreeView view;
        private EditorWindow editor;



        public void Init(SkillTreeView view, EditorWindow editor)
        {
            this.view = view;
            this.editor = editor;
        }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var entries = new List<SearchTreeEntry>();
            entries.Add(new SearchTreeGroupEntry(new GUIContent("Create Node")));

            var allTypes = TypeCache.GetTypesDerivedFrom<Node>();

            HashSet<string> addedGroups = new HashSet<string>();

            foreach (var type in allTypes)
            {
                if (type != typeof(Node) && !type.IsAbstract && type.IsSubclassOf(typeof(Node)))
                {
                    NodeInfoAttribute metadata = (NodeInfoAttribute)type.GetCustomAttributes(typeof(NodeInfoAttribute), true).First(x => x is NodeInfoAttribute);

                    string[] searchEntries = metadata.MenuName.Split('/');

                    for (int i = 0; i < searchEntries.Length - 1; i++)
                    {
                        if (addedGroups.Contains(searchEntries[i]))
                            continue;

                        entries.Add(new SearchTreeGroupEntry(new GUIContent($"{searchEntries[i]}"), i + 1));
                        addedGroups.Add(searchEntries[i]);
                    }

                    entries.Add(new SearchTreeEntry(new GUIContent(searchEntries[searchEntries.Length - 1]))
                    {
                        level = searchEntries.Length,
                        userData = type
                    });
                }
            }

            return entries;
        }

        public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
        {
            var type = searchTreeEntry.userData as Type;

            view.CreateNode(type, context.screenMousePosition);

            return true;
        }
    }
}