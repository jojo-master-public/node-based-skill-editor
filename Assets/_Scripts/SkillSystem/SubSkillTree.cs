using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Project.SkillSystem
{
    [CreateAssetMenu]
    public class SubSkillTree : SkillTree
    {
        public List<InputDataNode> FindInputNodes()
        {
            return nodes.Where(x => x is InputDataNode).Select(x => x as InputDataNode).ToList();
        }

        public OutputNode FindOutputNode()
        {
            if(nodes.Any(x => x is OutputNode))
                return nodes.First(x => x is OutputNode) as OutputNode;

            return null;
        }
    }
}