﻿namespace RPG.Enemy
{
    using System.Collections;
    using UnityEngine;

    public enum EnemyState
    {
        PATROL,
        CHASE,
        PLAYER_LOST,
        ATTACK_PERFORMING,
        ATTACK_READY
    }

    public class WalkingEnemy : EnemyBase
    {
        [SerializeField] private Animator animator;
        [SerializeField] private LayerMask solidObjectsMask;
        [SerializeField] private LayerMask playerMask;
        [SerializeField] private float attackRange;
        [SerializeField] private float attackTime;
        [SerializeField] private float attackRadius;
        [SerializeField] private float attackCooldown;
        [SerializeField] private float patrolApproachRange;
        [SerializeField] private float visionRadius;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float attackRotationSpeed;
        [SerializeField] private Vector3[] patrolPoints;

        private int currentPatrolPoint;
        private int patrolDirection = 1;
        private float currentAttackCooldown;
        private Transform cachedTransform;
        private Transform playerTransform;
        private Vector3 lastPlayerPosition;
        private EnemyState currentState = EnemyState.PATROL;



        protected override void Awake()
        {
            base.Awake();

            cachedTransform = transform;
        }

        private void Update()
        {
            ProcessState();
        }

        #region STATE MACHINE

        private void ProcessState()
        {
            switch (currentState)
            {
                case EnemyState.PATROL:
                    OnPatrol();
                    break;
                case EnemyState.CHASE:
                    OnChase();
                    break;
                case EnemyState.PLAYER_LOST:
                    OnPlayerLost();
                    break;
                case EnemyState.ATTACK_READY:
                    OnAttackReady();
                    break;
                case EnemyState.ATTACK_PERFORMING:
                    OnAttackPerforming();
                    break;
                default:
                    break;
            }
        }

        private void OnPatrol()
        {
            if(currentPatrolPoint > patrolPoints.Length - 1)
            {
                currentPatrolPoint = patrolPoints.Length - 2;
                patrolDirection = -patrolDirection;
            }
            else if (currentPatrolPoint < 0)
            {
                currentPatrolPoint = 1;
                patrolDirection = -patrolDirection;
            }

            MoveToTarget(patrolPoints[currentPatrolPoint]);

            if (CheckDistanceToTarget(patrolPoints[currentPatrolPoint], patrolApproachRange))
            {
                currentPatrolPoint += patrolDirection;
            }

            if (CheckPlayerVisible(out playerTransform))
                currentState = EnemyState.CHASE;
        }

        private void OnChase()
        {
            if(playerTransform != null)
            {
                lastPlayerPosition = playerTransform.position;
                MoveToTarget(lastPlayerPosition);

                if (CheckDistanceToTarget(lastPlayerPosition, attackRange))
                {
                    currentState = EnemyState.ATTACK_READY;
                }
            }
            else
            {
                currentState = EnemyState.PLAYER_LOST;
            }
        }

        private void OnPlayerLost()
        {
            MoveToTarget(lastPlayerPosition);

            if (CheckDistanceToTarget(lastPlayerPosition, patrolApproachRange))
                currentState = EnemyState.PATROL;
        }

        private void OnAttackReady()
        {
            if (CheckPlayerVisible(out playerTransform))
                lastPlayerPosition = playerTransform.position;
            else
            {
                currentState = EnemyState.PLAYER_LOST;
                return;
            }

            // start performing an attack;
            if(CheckDistanceToTarget(lastPlayerPosition, attackRange))
            {
                StartCoroutine(CoroutineAttack(cachedTransform, lastPlayerPosition));
                currentState = EnemyState.ATTACK_PERFORMING;
            }
            else
            {
                currentState = EnemyState.CHASE;
            }
        }

        private void OnAttackPerforming()
        {
            currentAttackCooldown += Time.deltaTime;

            if(currentAttackCooldown > attackCooldown)
            {
                currentAttackCooldown = 0;
                currentState = EnemyState.ATTACK_READY;
            }
        }

        private IEnumerator CoroutineAttack(Transform target, Vector3 destination)
        {
            animator.Play("Attack");
            Vector3 sourcePosition = target.position;

            float timer = 0;

            while(timer < attackTime)
            {
                timer += Time.deltaTime;

                Vector3 direction = destination - target.position;

                if (Mathf.Approximately(direction.sqrMagnitude, 0f))
                    direction = target.forward;

                target.position = Vector3.Lerp(sourcePosition, destination, timer / attackTime);
                cachedTransform.rotation = Quaternion.Lerp(cachedTransform.rotation, 
                    Quaternion.LookRotation(direction, Vector3.up), 
                    attackRotationSpeed * Time.deltaTime);

                // TODO: ADD ATTACK EVENT HERE

                yield return null;
            }
        }

        #endregion

        private bool CheckPlayerVisible(out Transform result)
        {
            Collider[] player = Physics.OverlapSphere(GetCheckPosition(), visionRadius, playerMask, QueryTriggerInteraction.Ignore);

            if(player != null && player.Length > 0)
            {
                result = player[0].transform;
                return true;
            }

            result = null;
            return false;
        }

        private bool CheckDistanceToTarget(Vector3 target, float range)
        {
            return Vector3.Distance(cachedTransform.position, target) < range;
        }

        private void MoveToTarget(Vector3 target)
        {
            cachedTransform.position = Vector3.MoveTowards(cachedTransform.position, target, movementSpeed * Time.deltaTime);
            cachedTransform.rotation = Quaternion.Lerp(cachedTransform.rotation, Quaternion.LookRotation(target - cachedTransform.position, Vector3.up), rotationSpeed * Time.deltaTime);
        }

        private Vector3 GetCheckPosition()
        {
            return transform.position + Vector3.up * 0.5f + transform.forward * visionRadius;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position + Vector3.up * 0.5f + transform.forward * visionRadius, visionRadius);

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, attackRange);

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + Vector3.up * 0.5f + transform.forward * attackRadius, attackRadius);

            if (playerTransform != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, playerTransform.position);
            }

            if(patrolPoints != null && patrolPoints.Length > 0)
            {
                Gizmos.color = Color.yellow;

                foreach (var p in patrolPoints)
                {
                    Gizmos.DrawWireCube(p, 0.2f * Vector3.one);
                }

                for (int i = 1; i < patrolPoints.Length; i++)
                {
                    Gizmos.DrawLine(patrolPoints[i], patrolPoints[i - 1]);
                }
            }
        }
    }
}