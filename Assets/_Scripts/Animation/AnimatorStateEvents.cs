using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorStateEvents : StateMachineBehaviour
{
    public static event Action<int, int> onStateEnter;
    public static event Action<int, int> onStateExit;



    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        onStateEnter?.Invoke(layerIndex, stateInfo.shortNameHash);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);

        onStateExit?.Invoke(layerIndex, stateInfo.shortNameHash);
    }
}