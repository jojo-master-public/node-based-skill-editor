using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator targetAnimator;
    [SerializeField] private InputListener inputListener;

    [SerializeField] private float movementInputLerp = 1;

    private float lastInputX, lastInputY;
    private Quaternion cameraRotation;



    private void OnMovementInput(Vector2 input)
    {
        lastInputX = Mathf.Lerp(lastInputX, input.x, movementInputLerp * Time.deltaTime);
        lastInputY = Mathf.Lerp(lastInputY, input.y, movementInputLerp * Time.deltaTime);

        Vector3 rotatedInput = Quaternion.Inverse(cameraRotation) * (new Vector3(lastInputX, 0, lastInputY));

        targetAnimator.SetFloat("InputX", rotatedInput.x);
        targetAnimator.SetFloat("InputY", rotatedInput.z);
    }

    private void OnCursorDirection(Vector3 direction)
    {
        cameraRotation = Quaternion.LookRotation(direction, Vector3.up);
    }

    private void OnEnable()
    {
        inputListener.onMovementInput += OnMovementInput;
        CameraController.Instance.onCursorDirection += OnCursorDirection;
    }

    private void OnDisable()
    {
        inputListener.onMovementInput -= OnMovementInput;
        CameraController.Instance.onCursorDirection -= OnCursorDirection;
    }
}