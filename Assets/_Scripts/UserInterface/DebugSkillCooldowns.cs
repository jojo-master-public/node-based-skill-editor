namespace Project.UserInterface
{
    using Project.Skills;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    [System.Serializable]
    public class SkillCooldownIcon
    {
        public SkillMapping mapping;
        public Image targetIcon;
    }

    public class DebugSkillCooldowns : MonoBehaviour
    {
        [SerializeField] private SkillCooldownIcon[] icons;

        private PlayerSkillStateMachine skillStateMachine;
        private Dictionary<SkillMapping, Image> cooldownIconsDic;



        private void OnSkillCooldown(SkillMapping skill, float currentCooldown)
        {
            cooldownIconsDic[skill].fillAmount = currentCooldown;
        }

        private Dictionary<SkillMapping, Image> CreateIconsDictionary()
        {
            Dictionary<SkillMapping, Image>  temp = new Dictionary<SkillMapping, Image>();

            foreach (SkillCooldownIcon icon in icons)
            {
                temp.Add(icon.mapping, icon.targetIcon);
            }

            return temp;
        }

        private void OnPlayerCreatedEvent(RPG.Core.Player obj)
        {
            skillStateMachine = obj.PlayerObject.GetComponent<PlayerSkillStateMachine>();

            cooldownIconsDic = CreateIconsDictionary();
            skillStateMachine.onCooldown += OnSkillCooldown;
        }

        private void OnEnable()
        {
            GameManager.onPlayerCreatedEvent += OnPlayerCreatedEvent;
        }

        private void OnDisable()
        {
            //cooldownIconsDic.Clear();

            GameManager.onPlayerCreatedEvent -= OnPlayerCreatedEvent;

            //foreach (SkillToButton skill in targetSkillSet.Skills)
            //{
            //    skill.SkillBehaviour.onCooldown -= SkillBehaviour_onCooldown;
            //}
        }
    }
}