using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Core;
using UnityEngine;

public class DebugHealth : MonoBehaviour
{
    private PlayerContainer player;



    private void OnPlayerCreatedEvent(Player obj)
    {
        player = obj.PlayerObject.GetComponent<PlayerContainer>();
    }

    private void OnEnable()
    {
        GameManager.onPlayerCreatedEvent += OnPlayerCreatedEvent;
    }

    private void OnDisable()
    {
        GameManager.onPlayerCreatedEvent -= OnPlayerCreatedEvent;
    }
}
