using RPG.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static event Action<Player> onPlayerCreatedEvent;

    [SerializeField] private CharacterClass debugPlayer;
    [SerializeField] private GameObject playerPrefab;

    [SerializeField] private Vector3 startingPosition;



    public void StartGame()
    {
        Player p = CreatePlayer(debugPlayer);
    }

    public Player CreatePlayer(CharacterClass selectedClass)
    {
        GameObject newPlayer = Instantiate(playerPrefab, startingPosition, Quaternion.identity);
        Player p = new Player();
        p.PlayerObject = newPlayer;
        p.Init(selectedClass);

        PlayerContainer playerContainer = newPlayer.GetComponent<PlayerContainer>();
        playerContainer.Init(p);

        PlayerBehaviourBase[] playerBehaviours = newPlayer.GetComponents<PlayerBehaviourBase>();

        for (int i = 0; i < playerBehaviours.Length; i++)
        {
            playerBehaviours[i].Init(playerContainer, selectedClass);
        }

        onPlayerCreatedEvent?.Invoke(p);

        return p;
    }
}