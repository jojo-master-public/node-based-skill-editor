using Project.Skills;
using Project.SkillSystem;
using RPG.Core;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkillToButton
{
    [SerializeField] private SkillMapping targetSkillMapping;
    [SerializeField] private Skill targetSkill;

    public SkillMapping TargetSkillMapping { get => targetSkillMapping; }
    public Skill TargetSkill { get => targetSkill; }
    public PlayerSkillStateMachine StateMachine { get; set; }


    public SkillToButton(SkillMapping mapping, Skill skill)
    {
        targetSkillMapping = mapping;
        targetSkill = skill;
    }

    public void OnButtonDown()
    {
        StateMachine.CastSkill(TargetSkillMapping, TargetSkill);
    }

    public void OnButton()
    {
        //StateMachine.CastSkill(TargetSkill);
    }

    public void OnButtonUp() { }
}

public class PlayerSkill : PlayerBehaviourBase
{
    public SkillToButton[] Skills { get; private set; }



    public override void Init(PlayerContainer player, CharacterClass character)
    {
        Skills = new SkillToButton[character.StartingSkills.Length];

        var stateMachine = GetComponent<PlayerSkillStateMachine>();

        for (int i = 0; i < Skills.Length; i++)
        {
            Skills[i] = new SkillToButton(character.StartingSkills[i].TargetSkillMapping, character.StartingSkills[i].TargetSkill);
            Skills[i].StateMachine = character.StartingSkills[i].StateMachine;

            targetListener.SubscribeToButtonDownAction(Skills[i].TargetSkillMapping, Skills[i].OnButtonDown);
            targetListener.SubscribeToButtonAction(Skills[i].TargetSkillMapping, Skills[i].OnButton);
            targetListener.SubscribeToButtonUpAction(Skills[i].TargetSkillMapping, Skills[i].OnButtonUp);

            Skills[i].StateMachine = stateMachine;
        }
    }

    private void OnDisable()
    {
        foreach (SkillToButton skill in Skills)
        {
            targetListener.UnsubscribeToButtonDownAction(skill.TargetSkillMapping, skill.OnButtonDown);
            targetListener.UnsubscribeToButtonAction(skill.TargetSkillMapping, skill.OnButton);
            targetListener.UnsubscribeToButtonUpAction(skill.TargetSkillMapping, skill.OnButtonUp);
        }
    }
}