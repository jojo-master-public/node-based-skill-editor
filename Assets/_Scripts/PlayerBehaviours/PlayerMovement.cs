using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : PlayerBehaviourBase
{
    public static Vector3 movementInput;

    [SerializeField] private PlayerSkillStateMachine skillStateMachine;
    [SerializeField] private float movementSpeedBase;

    private Transform cachedTransform;



    private void OnPlayerMovementInput(Vector2 input)
    {
        movementInput = new Vector3(input.x, 0, input.y);
        float movementSpeed = GetMovementSpeed();

        cachedTransform.position += movementInput * movementSpeed;
    }

    private float GetMovementSpeed()
    {
        float playerMovementStat = StatUtility.SPD_MULTIPLIER * playerContainer.GetStat(Stat.SPD);

        return (movementSpeedBase + playerMovementStat) * Time.deltaTime /* skillStateMachine.GetCurrentMovementPenalty()*/;
    }

    private void OnCursorDirection(Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
        cachedTransform.rotation = rotation;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        cachedTransform = transform;
        targetListener.onMovementInput += OnPlayerMovementInput;
        CameraController.Instance.onCursorDirection += OnCursorDirection;
    }

    private void OnDisable()
    {
        targetListener.onMovementInput -= OnPlayerMovementInput;
        CameraController.Instance.onCursorDirection -= OnCursorDirection;
    }

    public override void Init(PlayerContainer player, CharacterClass character) { }
}