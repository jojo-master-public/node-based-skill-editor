using RPG.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerBehaviourBase : MonoBehaviour
{
    protected InputListener targetListener;
    protected PlayerContainer playerContainer;


    public abstract void Init(PlayerContainer player, CharacterClass character);

    protected virtual void OnEnable()
    {
        targetListener = GetComponent<InputListener>();
        playerContainer = GetComponent<PlayerContainer>();
    }
}