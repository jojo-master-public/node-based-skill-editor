using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Skills;
using Project.SkillSystem;
using RPG.Core;
using System;

/// <summary> Skill state machine that is ran on the player gameObject </summary>
public class PlayerSkillStateMachine : MonoBehaviour, ISkillMonobehaviour
{
    // used for managing execution cooldowns on certain skill buttons
    public event Action<SkillMapping, float> onCooldown;

    [SerializeField] private Animator targetAnimator;
    [SerializeField] private Transform meleeAttackPosition;

    // data containers that are used by skill tree nodes
    public Dictionary<string, NodeDataContainer> DataContainers { get; private set; }

    // currently executed skill trees
    private Dictionary<SkillMapping, SkillTree> ActiveTrees { get; set; }

    // currently executed skill
    private Skill ActiveSkill { get; set; }

    // coroutines for handling cooldowns
    private Dictionary<SkillMapping, Coroutine> coroutine_Cooldowns;



    // casts a target skill on this gameObject
    public void CastSkill(SkillMapping mapping, Skill skill)
    {
        if (coroutine_Cooldowns.ContainsKey(mapping))
            return;

        ActiveSkill = skill;

        if (ActiveTrees.ContainsKey(mapping))
            ActiveTrees[mapping] = skill.TargetSkillTree;
        else
            ActiveTrees.Add(mapping, skill.TargetSkillTree);

        // create data container to hold all the executed nodes data
        NodeDataContainer dataContainer = new NodeDataContainer();
        dataContainer.guid = Guid.NewGuid().ToString();

        dataContainer.nodeData.Add(dataContainer.guid, new Dictionary<string, NodeData>());
        DataContainers.Add(dataContainer.guid, dataContainer);

#if UNITY_EDITOR
        onContainerAdded?.Invoke(skill.TargetSkillTree.name, dataContainer.guid, dataContainer.guid);
        ActiveTree = ActiveTrees[mapping];
#endif

        NodeData rootNodeData = new NodeData();
        rootNodeData.subTreeGuid = dataContainer.guid;
        rootNodeData.containerGuid = dataContainer.guid;
        rootNodeData.targetSkill = skill;
        rootNodeData.skillCaster = this;

        ActiveTrees[mapping].Init();
        ActiveTrees[mapping].Execute(rootNodeData);

        coroutine_Cooldowns.Add(mapping, StartCoroutine(CoroutineCooldown(mapping, skill.Cooldown)));
    }

    public void PlayAnimation(string name)
    {
        if(targetAnimator.HasState(0, Animator.StringToHash(name)))
            targetAnimator.Play(name, 0);
        if(targetAnimator.HasState(1, Animator.StringToHash(name)))
            targetAnimator.Play(name, 1);
    }

    #region INFO CHARACTER/GAMEOBJECT

    public float GetCurrentMovementPenalty()
    {
        if (ActiveSkill != null)
            return ActiveSkill.MovementPenalty;
        else
            return 1f;
    }

    public Vector3 GetMeleePosition()
    {
        return meleeAttackPosition.position;
    }

    public Quaternion GetMeleeDirection()
    {
        return meleeAttackPosition.rotation;
    }

    public Vector3 GetCursorPosition()
    {
        return CameraController.CursorPosition;
    }

    public GameEntity GetSource()
    {
        return GetComponent<GameEntity>();
    }

    public GameObject GetCharacterGameobject()
    {
        return gameObject;
    }

    public Transform GetTargetCharacter()
    {
        return transform;
    }

    public Vector3 GetMovementInputDirection()
    {
        return PlayerMovement.movementInput;
    }

    #endregion

    public NodeData GetDataForNextNode(NodeData current, string nextNodeGuid)
    {
        string containerGuid = current.containerGuid;
        NodeData nextNodeData = DataContainers[containerGuid].GetData(nextNodeGuid, current.subTreeGuid);

        nextNodeData.targetSkill = current.targetSkill;
        nextNodeData.skillCaster = current.skillCaster;

        return nextNodeData;
    }

    private IEnumerator CoroutineCooldown(SkillMapping skill, float cooldown)
    {
        float timer = 0;

        while(timer < cooldown)
        {
            timer += Time.deltaTime;

            onCooldown?.Invoke(skill, timer / cooldown);

            yield return null;
        }

        onCooldown?.Invoke(skill, 1);

        coroutine_Cooldowns[skill] = null;
        coroutine_Cooldowns.Remove(skill);
    }

    private void OnEnable()
    {
        coroutine_Cooldowns = new Dictionary<SkillMapping, Coroutine>();
        ActiveTrees = new Dictionary<SkillMapping, SkillTree>();
        DataContainers = new Dictionary<string, NodeDataContainer>();
    }

#if UNITY_EDITOR

    // currently executed skill tree
    public SkillTree ActiveTree { get; private set; }

    // executed when skill data container was added to properly display in editor window
    public static event Action<string, string, string> onContainerAdded;

#endif
}