using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    public event Action<Vector3> onCursorDirection;
    public static CameraController Instance;
    public static Vector3 CursorPosition { get; private set; }

    [SerializeField] private Camera targetCamera;
    [SerializeField] private float maxCameraOffsetDistance;
    [SerializeField] private float directionToCursorScale;
    [SerializeField] private float cameraFollowSpeed;
    [SerializeField] private Vector3 cameraMainOffset;

    private InputListener inputListener;
    private Transform targetCharacter;
    private Vector3 mousePosition;



    private void Update()
    {
        if (inputListener == null || targetCharacter == null)
            return;

        Vector3 characterPosition = targetCharacter.position;
        Plane characterPlane = new Plane(characterPosition, characterPosition + Vector3.right, characterPosition + Vector3.back);
        Ray mouseRay = targetCamera.ScreenPointToRay(mousePosition);

        if(characterPlane.Raycast(mouseRay, out float enterDistance))
        {
            CursorPosition = mouseRay.origin + mouseRay.direction * enterDistance;
            Vector3 directionToMouse = CursorPosition - characterPosition;
            PositionCamera(directionToMouse);
            onCursorDirection?.Invoke(directionToMouse);
            Debug.DrawLine(characterPosition, CursorPosition);
            Debug.DrawLine(CursorPosition, CursorPosition + Vector3.up);
        }
    }

    private void PositionCamera(Vector3 cursorOffset)
    {
        Vector3 cameraPosition = targetCharacter.position + cameraMainOffset;
        Vector3 directionToCursorScaled = cursorOffset * directionToCursorScale;
        directionToCursorScaled = Vector3.ClampMagnitude(directionToCursorScaled, maxCameraOffsetDistance);
        cameraPosition += directionToCursorScaled;
        targetCamera.transform.position = Vector3.Lerp(targetCamera.transform.position, cameraPosition, cameraFollowSpeed * Time.deltaTime);
    }

    private void OnViewInput(Vector2 input)
    {
        mousePosition = input;
    }

    private void OnPlayerCreatedEvent(RPG.Core.Player obj)
    {
        inputListener = obj.PlayerObject.GetComponent<InputListener>();
        targetCharacter = obj.PlayerObject.transform;
        inputListener.onViewInput += OnViewInput;
    }

    private void OnEnable()
    {
        Instance = this;
        GameManager.onPlayerCreatedEvent += OnPlayerCreatedEvent;
    }

    private void OnDisable()
    {
        GameManager.onPlayerCreatedEvent -= OnPlayerCreatedEvent;
        if(inputListener != null)
            inputListener.onViewInput -= OnViewInput;
    }
}