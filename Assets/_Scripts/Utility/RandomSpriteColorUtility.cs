﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpriteColorUtility : MonoBehaviour
{
    public SpriteRenderer[] targets;
    public Color min, max;



    [ContextMenu("Randomize")]
    public void Randomize()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            float rnd = Random.Range(0, 1f);

            targets[i].color = Color.Lerp(min, max, rnd);
        }
    }
}
