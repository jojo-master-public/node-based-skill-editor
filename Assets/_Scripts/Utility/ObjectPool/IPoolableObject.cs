﻿using System;

public interface IPoolable
{
    event Action<IPoolable> onReturnToPool;

    void ReturnToPool();
}