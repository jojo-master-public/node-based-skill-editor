namespace Project.SkillSystem
{
    using RPG.Core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public enum ValueType
    {
        Vector,
        Quaternion,
        Float,
        Int,
        Uint,
        Bool,
        String,
        GameObject,
        Transform,
        LayerMask,
        GameEntity
    }
    public class BoxedValue
    {
        public ValueType type;
        public object value;

        public BoxedValue(object value, ValueType type)
        {
            this.value = value;
            this.type = type;
        }
    }

    [Serializable]
    public class SkillInputContainer : ISerializationCallbackReceiver
    {
        // used for serialization/deserealization
        [SerializeField] private List<string> keys;
        [SerializeField] private List<int> indexes;
        [SerializeField] private List<ValueType> types;

        // actual data
        [SerializeField] private List<float> floatValues;
        [SerializeField] private List<int> intValues;
        [SerializeField] private List<uint> uintValues;
        [SerializeField] private List<bool> boolValues;
        [SerializeField] private List<string> stringValues;
        [SerializeField] private List<Vector3> vectorValues;
        [SerializeField] private List<LayerMask> layerValues;
        [SerializeField] private List<Quaternion> quaternionValues;
        [SerializeField] private List<GameObject> gameobjectValues;
        [SerializeField] private List<Transform> transformValues;
        [SerializeField] private List<GameEntity> entityValues;

        // 
        private Dictionary<string, BoxedValue> dictionary;

        public int Count { get => dictionary.Count; }



        public BoxedValue this[string key]
        {
            get
            {
                if (!ContainsKey(key))
                    throw new ArgumentException("Container does not contain such skill input!");

                return dictionary[key];
            }
            set
            {
                if (!ContainsKey(key))
                    throw new ArgumentException("Container does not contain such skill input!");

                this.dictionary[key] = value;
            }
        }

        public bool ContainsKey(string key)
        {
            return dictionary.ContainsKey(key);
        }

        public void Add(string key, object value, ValueType type)
        {
            if (ContainsKey(key))
                throw new ArgumentException($"Container already contains such key! {key}");

            dictionary.Add(key, new BoxedValue(value, type));
        }

        public void Remove(string key)
        {
            if (!ContainsKey(key))
                throw new ArgumentException($"Container does not contains such key! {key}");

            dictionary.Remove(key);
        }

        public int GetIndex(string key)
        {
            int indexOfKey = keys.IndexOf(key);
            return indexes[indexOfKey];
        }

        public void Clear()
        {
            dictionary.Clear();
        }

        public void OnBeforeSerialize()
        {
            keys = new List<string>();
            types = new List<ValueType>();
            indexes = new List<int>();
            floatValues = new List<float>();
            intValues = new List<int>();
            uintValues = new List<uint>();
            boolValues = new List<bool>();
            stringValues = new List<string>();
            vectorValues = new List<Vector3>();
            layerValues = new List<LayerMask>();
            quaternionValues = new List<Quaternion>();
            gameobjectValues = new List<GameObject>();
            transformValues = new List<Transform>();
            entityValues = new List<GameEntity>();

            foreach (var kvp in dictionary)
            {
                object value = kvp.Value.value;
                string key = kvp.Key;
                ValueType type = kvp.Value.type;

                keys.Add(key);
                types.Add(type);

                switch (type)
                {
                    case ValueType.Float:
                        indexes.Add(floatValues.Count);
                        floatValues.Add((float)value);
                        break;
                    case ValueType.Int:
                        indexes.Add(intValues.Count);
                        intValues.Add((int)value);
                        break;
                    case ValueType.Uint:
                        indexes.Add(uintValues.Count);
                        uintValues.Add((uint)value);
                        break;
                    case ValueType.Bool:
                        indexes.Add(boolValues.Count);
                        boolValues.Add((bool)value);
                        break;
                    case ValueType.String:
                        indexes.Add(stringValues.Count);
                        stringValues.Add((string)value);
                        break;
                    case ValueType.Vector:
                        indexes.Add(vectorValues.Count);
                        vectorValues.Add((Vector3)value);
                        break;
                    case ValueType.Quaternion:
                        indexes.Add(quaternionValues.Count);
                        quaternionValues.Add((Quaternion)value);
                        break;
                    case ValueType.GameObject:
                        indexes.Add(gameobjectValues.Count);
                        gameobjectValues.Add((GameObject)value);
                        break;
                    case ValueType.Transform:
                        indexes.Add(transformValues.Count);
                        transformValues.Add((Transform)value);
                        break;
                    case ValueType.LayerMask:
                        indexes.Add(layerValues.Count);
                        layerValues.Add((LayerMask)value);
                        break;
                    case ValueType.GameEntity:
                        indexes.Add(entityValues.Count);
                        entityValues.Add((GameEntity)value);
                        break;
                    default:
                        break;
                }
            }
        }

        public void OnAfterDeserialize()
        {
            dictionary = new Dictionary<string, BoxedValue>();

            foreach (var key in keys)
            {
                int indexOfKey = keys.IndexOf(key);
                int indexOfValue = indexes[indexOfKey];

                switch (types[indexOfKey])
                {
                    case ValueType.Vector:
                        dictionary.Add(key, new BoxedValue(vectorValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.Quaternion:
                        dictionary.Add(key, new BoxedValue(quaternionValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.Float:
                        dictionary.Add(key, new BoxedValue(floatValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.Int:
                        dictionary.Add(key, new BoxedValue(intValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.Uint:
                        dictionary.Add(key, new BoxedValue(uintValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.Bool:
                        dictionary.Add(key, new BoxedValue(boolValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.String:
                        dictionary.Add(key, new BoxedValue(stringValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.GameObject:
                        dictionary.Add(key, new BoxedValue(gameobjectValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.Transform:
                        dictionary.Add(key, new BoxedValue(transformValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.LayerMask:
                        dictionary.Add(key, new BoxedValue(layerValues[indexOfValue], types[indexOfKey]));
                        break;
                    case ValueType.GameEntity:
                        dictionary.Add(key, new BoxedValue(entityValues[indexOfValue], types[indexOfKey]));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}