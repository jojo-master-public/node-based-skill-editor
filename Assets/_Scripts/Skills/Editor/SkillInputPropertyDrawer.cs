namespace Project.SkillSystem
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

    [CustomPropertyDrawer(typeof(SkillInputContainer))]
    public class SkillInputPropertyDrawer : PropertyDrawer
    {
        public const int PROPERTY_HEIGHT = 150;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            Rect labelRect = new Rect(position.x, position.y, position.width, 25);

            GUIContent labelContent = new GUIContent(label.text);
            EditorGUI.LabelField(labelRect, label);

            var indent = EditorGUI.indentLevel;

            DrawGUI(position, property);

            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty keys = property.FindPropertyRelative("keys");

            return keys.arraySize * 20 + 25 + 20;

            return PROPERTY_HEIGHT;
        }

        private void DrawGUI(Rect position, SerializedProperty property)
        {
            EditorGUI.BeginChangeCheck();

            Rect buttonAddPos = new Rect(position.x + 150, position.y, 85, 20);
            Rect buttonRemovePos = new Rect(position.x + 150 + 85, position.y, 85, 20);
            Rect buttonPopulateInputsPos = new Rect(position.x + 150 + 170, position.y, (position.width - 150 - 170), 20);

            SerializedProperty keys = property.FindPropertyRelative("keys");
            SerializedProperty valueTypes = property.FindPropertyRelative("types");
            SerializedProperty indexes = property.FindPropertyRelative("indexes");

            if (GUI.Button(buttonAddPos, "Add"))
            {
                int arraySize = keys.arraySize;

                keys.InsertArrayElementAtIndex(arraySize);
                valueTypes.InsertArrayElementAtIndex(arraySize);
                indexes.InsertArrayElementAtIndex(arraySize);

                var newKey = keys.GetArrayElementAtIndex(arraySize);
                newKey.stringValue = "new-input-" + arraySize;

                var index = indexes.GetArrayElementAtIndex(arraySize);
                index.intValue = property.FindPropertyRelative("vectorValues").arraySize;
                property.FindPropertyRelative("vectorValues").InsertArrayElementAtIndex(index.intValue);
                var valueType = valueTypes.GetArrayElementAtIndex(arraySize);
                valueType.enumValueIndex = (int)ValueType.Vector;
            }

            if (GUI.Button(buttonRemovePos, "Remove"))
            {
                if (keys.arraySize > 0)
                {
                    int arraySize = keys.arraySize;
                    keys.DeleteArrayElementAtIndex(arraySize - 1);
                    valueTypes.DeleteArrayElementAtIndex(arraySize - 1);
                    indexes.DeleteArrayElementAtIndex(arraySize - 1);
                }
            }

            if (GUI.Button(buttonPopulateInputsPos, "Populate Inputs From Tree"))
            {
                Debug.LogError("IMPLEMENT THIS TO QUICKLY POPULATE INPUTS FROM CURRENTLY SELECTED TREE!");
            }

            int count = keys.arraySize;

            float width = position.width;

            for (int i = 0; i < count; i++)
            {
                float yPosition = position.y + 25 + (i * 20);

                SerializedProperty key = keys.GetArrayElementAtIndex(i);
                SerializedProperty valueType = valueTypes.GetArrayElementAtIndex(i);
                SerializedProperty index = indexes.GetArrayElementAtIndex(i);

                ValueType oldTypeValue = (ValueType)valueType.enumValueIndex;

                Rect propertyValuePosition = new Rect(position.x + width / 3f + 50, yPosition, width / 3f - 20, 20);

                EditorGUI.LabelField(new Rect(position.x + 2f * width / 3f + 35, yPosition, 45, 20), "Type: ");
                ValueType newTypeValue = (ValueType)EditorGUI.EnumPopup(new Rect(position.x + 2f * width / 3f + 70, yPosition, width / 3f - 70, 20), oldTypeValue);

                if (newTypeValue != oldTypeValue)
                {
                    RemoveAddElement(newTypeValue, oldTypeValue, property, index);
                }

                valueType.enumValueIndex = (int)newTypeValue;

                EditorGUI.LabelField(new Rect(position.x, yPosition, 45, 20), "Name: ");
                EditorGUI.PropertyField(new Rect(position.x + 50, yPosition, width / 3f - 50, 20), key, GUIContent.none);

                EditorGUI.LabelField(new Rect(propertyValuePosition.x - 45, yPosition, 50, 20), "Value: ");
                DrawPropertyValue(propertyValuePosition, property, newTypeValue, index.intValue);
            }

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();
        }

        private void DrawPropertyValue(Rect propertyValuePosition, SerializedProperty property, ValueType t, int index)
        {
            switch (t)
            {
                case ValueType.Vector:
                    if (property.FindPropertyRelative("vectorValues").arraySize <= index)
                        Debug.Log("");
                    property.FindPropertyRelative("vectorValues").GetArrayElementAtIndex(index).vector3Value = EditorGUI.Vector3Field(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("vectorValues").GetArrayElementAtIndex(index).vector3Value);
                    break;
                case ValueType.Quaternion:
                    property.FindPropertyRelative("quaternionValues").GetArrayElementAtIndex(index).quaternionValue = Quaternion.Euler(EditorGUI.Vector3Field(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("quaternionValues").GetArrayElementAtIndex(index).quaternionValue.eulerAngles));
                    break;
                case ValueType.Float:
                    property.FindPropertyRelative("floatValues").GetArrayElementAtIndex(index).floatValue = EditorGUI.FloatField(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("floatValues").GetArrayElementAtIndex(index).floatValue);
                    break;
                case ValueType.Int:
                    property.FindPropertyRelative("intValues").GetArrayElementAtIndex(index).intValue = EditorGUI.IntField(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("intValues").GetArrayElementAtIndex(index).intValue);
                    break;
                case ValueType.Uint:
                    property.FindPropertyRelative("uintValues").GetArrayElementAtIndex(index).intValue = Mathf.Clamp(EditorGUI.IntField(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("uintValues").GetArrayElementAtIndex(index).intValue), 0, int.MaxValue);
                    break;
                case ValueType.String:
                    property.FindPropertyRelative("stringValues").GetArrayElementAtIndex(index).stringValue = EditorGUI.TextField(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("stringValues").GetArrayElementAtIndex(index).stringValue);
                    break;
                case ValueType.GameObject:
                    EditorGUI.ObjectField(propertyValuePosition, property.FindPropertyRelative("gameobjectValues").GetArrayElementAtIndex(index), GUIContent.none);
                    break;
                case ValueType.Transform:
                    EditorGUI.ObjectField(propertyValuePosition, property.FindPropertyRelative("transformValues").GetArrayElementAtIndex(index), GUIContent.none);
                    break;
                case ValueType.LayerMask:
                    property.FindPropertyRelative("layerValues").GetArrayElementAtIndex(index).intValue = EditorGUI.MaskField(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("layerValues").GetArrayElementAtIndex(index).intValue, GetLayerNames());
                    break;
                case ValueType.GameEntity:
                    EditorGUI.ObjectField(propertyValuePosition, property.FindPropertyRelative("entityValues").GetArrayElementAtIndex(index), GUIContent.none);
                    break;
                case ValueType.Bool:
                    property.FindPropertyRelative("boolValues").GetArrayElementAtIndex(index).boolValue = EditorGUI.Toggle(propertyValuePosition, GUIContent.none, property.FindPropertyRelative("boolValues").GetArrayElementAtIndex(index).boolValue);
                    break;
                default:
                    break;
            }
        }

        private void RemoveAddElement(ValueType newType, ValueType oldType, SerializedProperty property, SerializedProperty index)
        {
            //switch (oldType)
            //{
            //    case ValueType.Vector:
            //        property.FindPropertyRelative("vectorValues").GetArrayElementAtIndex(index.intValue).;
            //        break;
            //    case ValueType.Quaternion:
            //        property.FindPropertyRelative("quaternionValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.Float:
            //        property.FindPropertyRelative("floatValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.Int:
            //        property.FindPropertyRelative("intValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.Uint:
            //        property.FindPropertyRelative("uintValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.String:
            //        property.FindPropertyRelative("stringValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.GameObject:
            //        property.FindPropertyRelative("gameobjectValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.Transform:
            //        property.FindPropertyRelative("transformValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.LayerMask:
            //        property.FindPropertyRelative("layerValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.GameEntity:
            //        property.FindPropertyRelative("entityValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    case ValueType.Bool:
            //        property.FindPropertyRelative("boolValues").DeleteArrayElementAtIndex(index.intValue);
            //        break;
            //    default:
            //        break;
            //}

            switch (newType)
            {
                case ValueType.Vector:
                    index.intValue = property.FindPropertyRelative("vectorValues").arraySize;
                    property.FindPropertyRelative("vectorValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.Quaternion:
                    index.intValue = property.FindPropertyRelative("quaternionValues").arraySize;
                    property.FindPropertyRelative("quaternionValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.Float:
                    index.intValue = property.FindPropertyRelative("floatValues").arraySize;
                    property.FindPropertyRelative("floatValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.Int:
                    index.intValue = property.FindPropertyRelative("intValues").arraySize;
                    property.FindPropertyRelative("intValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.Uint:
                    index.intValue = property.FindPropertyRelative("uintValues").arraySize;
                    property.FindPropertyRelative("uintValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.String:
                    index.intValue = property.FindPropertyRelative("stringValues").arraySize;
                    property.FindPropertyRelative("stringValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.GameObject:
                    index.intValue = property.FindPropertyRelative("gameobjectValues").arraySize;
                    property.FindPropertyRelative("gameobjectValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.Transform:
                    index.intValue = property.FindPropertyRelative("transformValues").arraySize;
                    property.FindPropertyRelative("transformValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.LayerMask:
                    index.intValue = property.FindPropertyRelative("layerValues").arraySize;
                    property.FindPropertyRelative("layerValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.GameEntity:
                    index.intValue = property.FindPropertyRelative("entityValues").arraySize;
                    property.FindPropertyRelative("entityValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                case ValueType.Bool:
                    index.intValue = property.FindPropertyRelative("boolValues").arraySize;
                    property.FindPropertyRelative("boolValues").InsertArrayElementAtIndex(index.intValue);
                    break;
                default:
                    break;
            }
        }

        private string[] GetLayerNames()
        {
            var tagManager = AssetDatabase.LoadMainAssetAtPath("ProjectSettings/TagManager.asset");

            var so = new SerializedObject(tagManager);
            var layers = so.FindProperty("layers");

            var layersCount = layers.arraySize;

            List<string> layerNames = new List<string>();

            for (int i = 0; i < layersCount; i++)
            {
                string lName = layers.GetArrayElementAtIndex(i).stringValue;
                if (!string.IsNullOrEmpty(lName))
                    layerNames.Add(lName);
            }

            return layerNames.ToArray();
        }
    }
}