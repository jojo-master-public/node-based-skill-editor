namespace Project.Skills
{
    using Project.SkillSystem;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class SkillInfoAttribute : Attribute
    {
        public readonly bool DisplayNode;

        public SkillInfoAttribute(bool displayNode)
        {
            this.DisplayNode = displayNode;
        }
    }

    [CreateAssetMenu(fileName = "Skill_", menuName = "Skills/New")]
    public class Skill : ScriptableObject
    {
        [field: SerializeField, SkillInfo(false)] public string SkillName { get; private set; }
        [field: SerializeField, TextArea(3, 6), SkillInfo(false)] public string SkillDescription { get; private set; }

        [field: SerializeField, Header("Parameters"), Space, SkillInfo(true)] public int Damage { get; private set; }
        [field: SerializeField, SkillInfo(true)] public uint ChargeCount { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float Cooldown { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float CancelCooldown { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float CastTime { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float Duration { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float MovementPenalty { get; private set; }
        [field: SerializeField, SkillInfo(true)] public bool IsInterruptable { get; private set; }
        [field: SerializeField, SkillInfo(true)] public bool IsQueueable { get; private set; }
        [field: SerializeField, SkillInfo(true)] public bool IsCancellable { get; private set; }
        [field: SerializeField, SkillInfo(true)] public LayerMask DamageMask { get; private set; }
        [field: SerializeField, SkillInfo(true)] public GameObject Projectile { get; private set; }
        [field: SerializeField, SkillInfo(true)] public uint ProjectileCount { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float ProjectileSpeed { get; private set; }
        [field: SerializeField, SkillInfo(true)] public uint ComboCount { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float ComboDamageMult { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float StunDuration { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float KnockBackPower { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float AreaOfEffect { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float Range { get; private set; }
        [field: SerializeField, SkillInfo(true)] public LayerMask CollisionMask { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float KnockBackDuration { get; private set; }
        [field: SerializeField, SkillInfo(true)] public float ProjectileLifetime { get; private set; }

        [field: SerializeField, SkillInfo(false)] public SkillTree TargetSkillTree { get; private set; }

        public SkillInputContainer customInputs = new SkillInputContainer();



        public static string GetPropertyName(string propertyName)
        {
            string splitName = propertyName.Split('>')[0];
            return splitName.Substring(1, splitName.Length - 1);
        }

        public T GetValue<T>(string inputName, out bool isValid)
        {
            isValid = customInputs.ContainsKey(inputName);

            if (isValid)
            {
                var input = customInputs[inputName];

                return (T)input.value;
            }

            return default(T);
        }
    }
}