namespace Project.Skills
{
    using UnityEngine;

    public static class SkillManager
    {
        private const bool debugEnabled = false;



        //public static ISkillBehaviour<ISkillInfo> GetSkill(ISkillMonobehaviour target, ISkillInfo source)
        //{
        //    ISkillBehaviour<ISkillInfo> newSkill = SkillFactory.CreateSkill(target, source);

        //    newSkill.onCastBegan += OnSkillCastBegan;
        //    newSkill.onCasting += OnSkillCasting;
        //    newSkill.onCastFinished += OnSkillCastFinished;
        //    newSkill.onCancelled += OnSkillCastCancelled;
        //    newSkill.onInterrupted += OnSkillCastInterrupted;
        //    newSkill.onCooldown += OnSkillCooldown;
        //    newSkill.onCooldownFinished += OnSkillCooldownFinished;

        //    return newSkill;
        //}

        //private static void OnSkillCastBegan<T>(SkillEventArgs<T> args) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = args.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = args.sourceBehaviour;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCastBegan] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName}");
        //}

        //private static void OnSkillCastFinished<T>(SkillEventArgs<T> args) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = args.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = args.sourceBehaviour;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCastFinished] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName}");
        //}

        //private static void OnSkillCasting<T>(SkillEventArgs<T, float> args) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = args.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = args.sourceBehaviour;
        //    float castingProgress = args.value;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCasting] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName} " +
        //            $"\nSkillBehaviour.Casting progress: {castingProgress.ToString("0.0")}");
        //}

        //private static void OnSkillCastInterrupted<T>(SkillEventArgs<T, object> interruptionArgs) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = interruptionArgs.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = interruptionArgs.sourceBehaviour;
        //    object interruptionSource = interruptionArgs.value;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCastInterrupted] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName} \nInterruption source: {interruptionSource}");
        //}

        //private static void OnSkillCastCancelled<T>(SkillEventArgs<T, ISkillBehaviour<T>> args) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = args.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = args.sourceBehaviour;
        //    ISkillBehaviour<T> cancelSource = args.value;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCastCancelled] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName} \nInterruption source: {cancelSource}");
        //}

        //private static void OnSkillCooldown<T>(SkillEventArgs<T, float, float> args) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = args.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = args.sourceBehaviour;
        //    float remainingCooldown = args.value1;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCooldown] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName} " +
        //            $"\nSkillBehaviour.Remaining cooldown: {remainingCooldown.ToString("0.0")}");
        //}

        //private static void OnSkillCooldownFinished<T>(SkillEventArgs<T> args) where T : ISkillInfo
        //{
        //    ISkillInfo skillInfoThatWasBeingCasted = args.source;
        //    ISkillBehaviour<T> skillBehaviourThatWasBeingCasted = args.sourceBehaviour;

        //    if(debugEnabled)
        //        Debug.Log($"[SkillManager.OnSkillCooldownFinished] \nSkillInfo.SkillName: {skillInfoThatWasBeingCasted.SkillName}");
        //}
    }
}