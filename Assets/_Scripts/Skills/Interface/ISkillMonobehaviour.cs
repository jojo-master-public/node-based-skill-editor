namespace Project.Skills
{
    using RPG.Core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary> Required for editor sorting and proper serialization if ISkillMonobehaviour structure changes</summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MethodSortAttribute : Attribute
    {
        public readonly int SortOrder = 0;

        public MethodSortAttribute(int sortOrder)
        {
            this.SortOrder = sortOrder;
        }
    }

    /// <summary> Interface for running skill trees on any gameObject </summary>
    public interface ISkillMonobehaviour
    {
        Dictionary<string, NodeDataContainer> DataContainers { get; }

        void PlayAnimation(string name);
        [MethodSort(0)] Vector3 GetCursorPosition();
        [MethodSort(1)] Vector3 GetMeleePosition();
        [MethodSort(2)] Quaternion GetMeleeDirection();
        [MethodSort(3)] GameEntity GetSource();
        [MethodSort(4)] GameObject GetCharacterGameobject();
        [MethodSort(5)] Transform GetTargetCharacter();
        [MethodSort(6)] Vector3 GetMovementInputDirection();
        Coroutine StartCoroutine(IEnumerator coroutine);
        void StopCoroutine(Coroutine coroutine);
        NodeData GetDataForNextNode(NodeData current, string nextNodeGuid);
    }
}