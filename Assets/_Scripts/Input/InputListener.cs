using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public enum SkillMapping
{
    Action_1,
    Action_2,
    Action_3,
    Action_4,
    Action_5,
    Action_6,
    Action_7
}

public class ActionMapping
{
    public event Action onAction;
    public event Action<SkillMapping> onActionInternal;

    private SkillMapping mapping;



    public ActionMapping(SkillMapping mapping)
    {
        this.mapping = mapping;
    }

    public void Invoke()
    {
        onAction?.Invoke();
        onActionInternal?.Invoke(mapping);
    }

    public void Invoke(InputAction.CallbackContext context)
    {
        onAction?.Invoke();
        onActionInternal?.Invoke(mapping);
    }
}

public class InputListener : MonoBehaviour
{
    public event Action<Vector2> onMovementInput;
    public event Action<Vector2> onViewInput;

    private Dictionary<SkillMapping, ActionMapping> onActionButtonDown;
    private Dictionary<SkillMapping, ActionMapping> onActionButton;
    private Dictionary<SkillMapping, ActionMapping> onActionButtonUp;
    private Dictionary<SkillMapping, Coroutine> coroutine_continuous;

    [SerializeField] private PlayerInput targetInput;

    private InputAction moveAction;
    private InputAction viewAction;



    public void SubscribeToButtonDownAction(SkillMapping skill, Action callback)
    {
        if(onActionButtonDown.ContainsKey(skill))
            onActionButtonDown[skill].onAction += callback;
    }

    public void UnsubscribeToButtonDownAction(SkillMapping skill, Action callback)
    {
        if(onActionButtonDown.ContainsKey(skill))
            onActionButtonDown[skill].onAction -= callback;
    }

    public void SubscribeToButtonAction(SkillMapping skill, Action callback)
    {
        if (onActionButton.ContainsKey(skill))
            onActionButton[skill].onAction += callback;
    }

    public void UnsubscribeToButtonAction(SkillMapping skill, Action callback)
    {
        if (onActionButton.ContainsKey(skill))
            onActionButton[skill].onAction -= callback;
    }

    public void SubscribeToButtonUpAction(SkillMapping skill, Action callback)
    {
        if (onActionButtonUp.ContainsKey(skill))
            onActionButtonUp[skill].onAction += callback;
    }

    public void UnsubscribeToButtonUpAction(SkillMapping skill, Action callback)
    {
        if (onActionButtonUp.ContainsKey(skill))
            onActionButtonUp[skill].onAction -= callback;
    }

    private void Update()
    {
        onMovementInput?.Invoke(moveAction.ReadValue<Vector2>());
        onViewInput?.Invoke(viewAction.ReadValue<Vector2>());
    }

    private void PopulateActionsDictionary()
    {
        onActionButtonDown = new Dictionary<SkillMapping, ActionMapping>();
        onActionButton = new Dictionary<SkillMapping, ActionMapping>();
        onActionButtonUp = new Dictionary<SkillMapping, ActionMapping>();
        coroutine_continuous = new Dictionary<SkillMapping, Coroutine>();

        string[] actionNames = Enum.GetNames(typeof(SkillMapping));

        foreach (var action in actionNames)
        {
            Enum.TryParse(action, out SkillMapping mapping);

            onActionButtonDown.Add(mapping, new ActionMapping(mapping));
            onActionButton.Add(mapping, new ActionMapping(mapping));
            onActionButtonUp.Add(mapping, new ActionMapping(mapping));

            targetInput.currentActionMap.FindAction(action).performed += onActionButtonDown[mapping].Invoke;
            targetInput.currentActionMap.FindAction(action).canceled += onActionButtonUp[mapping].Invoke;

            onActionButtonDown[mapping].onActionInternal += OnButtonDown;
            onActionButtonUp[mapping].onActionInternal += OnButtonUp;
        }
    }

    private void ClearActionsDictionary()
    {
        onActionButtonDown.Clear();
        onActionButton.Clear();
        onActionButtonUp.Clear();
        coroutine_continuous.Clear();
    }

    private void OnButtonDown(SkillMapping button)
    {
        if (coroutine_continuous.ContainsKey(button))
        {
            StopCoroutine(coroutine_continuous[button]);

            coroutine_continuous[button] = StartCoroutine(CoroutineOnButton(button, onActionButton[button].Invoke));
        }
        else
            coroutine_continuous.Add(button, StartCoroutine(CoroutineOnButton(button, onActionButton[button].Invoke)));
    }

    private void OnButtonUp(SkillMapping button)
    {
        if (coroutine_continuous.ContainsKey(button))
            StopCoroutine(coroutine_continuous[button]);
    }

    private IEnumerator CoroutineOnButton(SkillMapping mapping, Action callback)
    {
        while (true)
        {
            callback.Invoke();

            yield return null;
        }
    }

    private void OnEnable()
    {
        moveAction = targetInput.currentActionMap.FindAction("MovementInput");
        viewAction = targetInput.currentActionMap.FindAction("ViewInput");

        PopulateActionsDictionary();
    }

    private void OnDisable()
    {
        ClearActionsDictionary();
    }
}