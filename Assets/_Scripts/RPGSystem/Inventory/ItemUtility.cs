﻿using RPG.Equipment;

namespace RPG.Inventory
{
    public static class ItemUtility
    {
        public static ActualItem CreateItem(ItemTemplate template)
        {
            ActualItem newItem = null;

            if (template is EquippableItemBase)
                newItem = new ActualEquippableItem(template);
            else if (template is UsableItem)
                newItem = new ActualUsableItem(template);
            else
                newItem = new ActualItem(template);

            return newItem;
        }
    }
}