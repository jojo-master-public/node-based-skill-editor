﻿namespace RPG.Inventory
{
    using UnityEngine;

    public enum ItemCategory
    {
        Usable,
        Key,
        Equippable
    }

    [CreateAssetMenu(menuName = "Equipment/Item")]
    public class ItemTemplate : ScriptableObject
    {
        [SerializeField] protected string itemName;
        [SerializeField] protected string itemDesc;
        [SerializeField] protected ItemCategory itemCategory;
        [SerializeField] protected Sprite sprite;
        [SerializeField] protected GameObject prefab;
        [SerializeField] protected bool isIdentifiedOnStart;



        public string GetName()
        {
            return itemName;
        }

        public string GetDesc()
        {
            return itemDesc;
        }

        public Sprite GetSprite()
        {
            return sprite;
        }

        public ItemCategory GetItemCategory()
        {
            return itemCategory;
        }

        public bool GetInitialIdentifiedState()
        {
            return isIdentifiedOnStart;
        }

        public GameObject GetPrefab()
        {
            return prefab;
        }
    }
}