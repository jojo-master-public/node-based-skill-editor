﻿namespace RPG.Inventory
{
    using RPG.InteractionSystem;
    using RPG.Core;

    public class PickupableItem : InteractableObjectBase
    {
        private ActualItem targetItem;


        
        public void InitItem(ItemTemplate template)
        {
            targetItem = ItemUtility.CreateItem(template);
        }

        public void InitItem(ActualItem item)
        {
            targetItem = item;
        }

        public override void Interact(Player sender)
        {
            sender.Inventory.PickUpItem(targetItem);
            PickUp();
        }

        private void PickUp()
        {
            gameObject.SetActive(false);
        }
    }
}