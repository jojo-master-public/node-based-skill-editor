﻿namespace RPG.Inventory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Inventory/Scrolls/IdentifyScroll", fileName = "Scroll_Identify")]
    public class IdentifyScrollItem : UsableItem
    {
        public override bool UseItem(object sender, object target)
        {
            if (target is ActualItem)
            {
                ActualItem targetItem = target as ActualItem;

                if (targetItem.IsIdentified())
                {
                    return false;
                }
                else
                {
                    targetItem.Identify();
                    return true;
                }
            }
            else
                return false;
        }
    }
}