﻿namespace RPG.Inventory
{
    using RPG.Core;
    using RPG.Equipment;
    using System;
    using System.Collections.Generic;

    public class ActualEquippableItem : ActualItem
    {
        public List<CharacterStat> Modifiers { get; protected set; }



        public ActualEquippableItem(ActualItem copy) : base(copy)
        {
            if (copy.ItemReferense is EquippableItemBase)
            {
                Modifiers = new List<CharacterStat>();
                EquippableItemBase itemBase = ItemReferense as EquippableItemBase;
                Modifiers.AddRange(itemBase.GetModifiers());
            }
            else
            {
                throw new Exception("Cannot create an ActualEquippableItem from not EquippableItemBase class");
            }
        }

        public ActualEquippableItem(ItemTemplate template) : base(template)
        {
            if (template is EquippableItemBase)
            {
                Modifiers = new List<CharacterStat>();
                EquippableItemBase itemBase = ItemReferense as EquippableItemBase;
                Modifiers.AddRange(itemBase.GetModifiers());
            }
            else
            {
                throw new Exception("Cannot create an ActualEquippableItem from not EquippableItemBase class");
            }
        }

        public void AddModifier(CharacterStat newModifier)
        {
            Modifiers.Add(newModifier);
        }

        public void BoostModifier(Stat statType, int modificationValue)
        {
            for (int i = 0; i < Modifiers.Count; i++)
            {
                if (Modifiers[i].statType == statType)
                {
                    Modifiers[i].value += modificationValue;
                    return;
                }
            }
        }

        public int GetStat(Stat stat)
        {
            int statTotal = 0;

            if (Modifiers != null && Modifiers.Count > 0)
            {
                for (int i = 0; i < Modifiers.Count; i++)
                {
                    if (Modifiers[i].statType == stat)
                        statTotal += Modifiers[i].value;
                }
            }

            return statTotal;
        }

        public EquipmentCategory GetCategory()
        {
            return ((EquippableItemBase)ItemReferense).GetCategory();
        }
    }
}