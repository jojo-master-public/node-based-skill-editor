﻿namespace RPG.Inventory
{
    public class ActualItem
    {
        public readonly ItemTemplate ItemReferense;

        protected bool isIdentified;



        public ActualItem (ActualItem copy)
        {
            isIdentified = copy.isIdentified;
            ItemReferense = copy.ItemReferense;
        }

        public ActualItem (ItemTemplate template)
        {
            ItemReferense = template;
            isIdentified = template.GetInitialIdentifiedState();
        }

        public virtual void Identify()
        {
            isIdentified = true;
        }

        public virtual bool IsIdentified()
        {
            return isIdentified;
        }
    }
}