﻿namespace RPG.Inventory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class UsableItem : ItemTemplate
    {
        public bool targetRequired;
        public abstract bool UseItem(object sender, object target);
    }
}