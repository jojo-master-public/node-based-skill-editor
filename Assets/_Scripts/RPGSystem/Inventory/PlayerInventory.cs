﻿namespace RPG.Inventory
{
    using RPG.Core;
    using System;

    public class InventorySlot
    {
        public ActualItem item;



        public bool IsTaken
        {
            get
            {
                return item != null;
            }
        }
    }

    public class InventoryChangedEventArgs
    {
        // item used
        // item moved
        // item equipped
    }

    public class PlayerInventory
    {
        public event Action<PlayerInventory> onInventoryChangedEvent;

        private int inventorySizeX = 4;
        private int inventorySizeY = 7;

        public InventorySlot[,] InventorySlots { get; private set; }

        public InventorySlot this [int indexX, int indexY]
        {
            get
            {
                return InventorySlots[indexX, indexY];
            }
        }



        public PlayerInventory(CharacterClass character)
        {
            InventorySlots = CreateInventory(inventorySizeX, inventorySizeY);

            ActualItem[] startingItems = character.GetStartingItems();

            for (int i = 0; i < startingItems.Length; i++)
            {
                PickUpItem(startingItems[i]);
            }
        }

        private InventorySlot[,] CreateInventory(int sizeX, int sizeY)
        {
            InventorySlot[,] newInventory = new InventorySlot[sizeX, sizeY];

            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    newInventory[x, y] = new InventorySlot();
                }
            }

            return newInventory;
        }

        public void PickUpItem(ActualItem item)
        {
            for (int x = 0; x < InventorySlots.GetLength(0); x++)
            {
                for (int y = 0; y < InventorySlots.GetLength(1); y++)
                {
                    if(!InventorySlots[x, y].IsTaken)
                    {
                        InventorySlots[x, y].item = item;
                        onInventoryChangedEvent?.Invoke(this);
                        return;
                    }
                }
            }
        }

        public void RemoveItem(int x, int y)
        {
            if (InventorySlots[x, y].IsTaken)
            {
                InventorySlots[x, y].item = null;
                onInventoryChangedEvent?.Invoke(this);
            }
        }

        public void MoveItem(int oldX, int oldY, int newX, int newY)
        {
            if (!InventorySlots[oldX, oldY].IsTaken && !InventorySlots[newX, newY].IsTaken)
                return;

            ActualItem temp = InventorySlots[newX, newY].item;

            InventorySlots[newX, newY].item = InventorySlots[oldX, oldY].item;
            InventorySlots[oldX, oldY].item = temp;

            onInventoryChangedEvent?.Invoke(this);
        }
    }
}