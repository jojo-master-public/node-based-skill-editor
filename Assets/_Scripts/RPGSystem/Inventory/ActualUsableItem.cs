﻿namespace RPG.Inventory
{
    using RPG.Core;

    public class ActualUsableItem : ActualItem
    {
        public ActualUsableItem(ActualItem copy) : base(copy) { }

        public ActualUsableItem(ItemTemplate template) : base(template)
        {
            bool isIdentified = template.GetInitialIdentifiedState() || PlayerPrefsManager.GetItemPref(ItemReferense.GetName());
            isIdentified = template.GetInitialIdentifiedState();
        }

        public override void Identify()
        {
            // since usable items are potions and scrolls
            // identify this item and all the items of the same type
            isIdentified = true;

            PlayerPrefsManager.SaveItemPref(ItemReferense.GetName(), 1);
        }

        public override bool IsIdentified()
        {
            return isIdentified || PlayerPrefsManager.GetItemPref(ItemReferense.GetName());
        }
    }
}