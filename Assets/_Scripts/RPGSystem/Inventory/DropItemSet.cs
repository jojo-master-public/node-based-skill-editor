﻿namespace RPG.Inventory
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class Drop
    {
        public ItemTemplate target;
        [Range(0, 1f)] public float dropChance;
    }

    [CreateAssetMenu(menuName = "Items/DropItemSet", fileName = "ItemSet_")]
    public class DropItemSet : ScriptableObject
    {
        public Drop[] targetItems;
    }
}