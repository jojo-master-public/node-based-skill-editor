﻿namespace RPG.Inventory
{
    using UnityEngine;
    using RPG.Core;

    [CreateAssetMenu(menuName = "Inventory/Potions/StatPotion", fileName = "StatPotion_")]
    public class StatPotionItem : UsableItem
    {
        public Stat targetStat;
        public int value;



        public override bool UseItem(object sender, object target = null)
        {
            if (sender is Player)
            {
                Player p = sender as Player;
                p.AddModifier(targetStat, value);
                return true;
            }
            else
                return false;
        }
    }
}