using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StatUtility
{
    public const float STR_MULTIPLIER = 0.1f;
    public const float SPD_MULTIPLIER = 0.1f;
    public const float INT_MULTIPLIER = 0.1f;
    public const float VIT_MULTIPLIER = 0.1f;
    public const float DEF_MULTIPLIER = 0.1f;
    public const float MDF_MULTIPLIER = 0.1f;
    public const float LCK_MULTIPLIER = 0.1f;
}
