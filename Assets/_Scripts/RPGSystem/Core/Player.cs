﻿namespace RPG.Core
{
    using System;
    using System.Collections.Generic;
    using RPG.Equipment;
    using RPG.Inventory;
    using UnityEngine;

    public enum Stat
    {
        STR = 0,
        DEX = 1,
        INT = 2,
        SPD = 7,
        VIT = 3,
        LCK = 4,
        DEF = 5,
        MDF = 6
    }

    /// <summary>
    /// Class that contains base stats of currently inited character
    /// </summary>
    public class Player
    {
        private Dictionary<Stat, int> playerStats;
        private Dictionary<Stat, int> modifiers;

        public PlayerEquipment Equipment { get; private set; }
        public PlayerInventory Inventory { get; private set; }
        public GameObject PlayerObject { get; set; }



        public void Init(CharacterClass playerClass)
        {
            Inventory = new PlayerInventory(playerClass);
            Equipment = new PlayerEquipment(playerClass);

            for (int x = 0; x < Inventory.InventorySlots.GetLength(0); x++)
            {
                for (int y = 0; y < Inventory.InventorySlots.GetLength(1); y++)
                {
                    if (Inventory.InventorySlots[x, y].IsTaken && Inventory.InventorySlots[x, y].item is ActualEquippableItem)
                        Equipment.EquipItem(Inventory.InventorySlots[x, y].item as ActualEquippableItem);
                }
            }

            playerStats = new Dictionary<Stat, int>();
            modifiers = new Dictionary<Stat, int>();

            CharacterStat[] startingStats = playerClass.Stats;
            for (int i = 0; i < startingStats.Length; i++)
            {
                playerStats.Add(startingStats[i].statType, startingStats[i].value);
            }
        }

        public int GetBaseStat(Stat stat)
        {
            return playerStats[stat];
        }

        public int GetTotalStat(Stat stat)
        {
            int playerBaseStat = GetBaseStat(stat);
            int modifierAmount = 0;

            if (modifiers.ContainsKey(stat))
                modifierAmount = modifiers[stat];

            int equipmentTotalStat = Equipment.GetEquipmentTotalStat(stat);
            return playerBaseStat + equipmentTotalStat + modifierAmount;
        }

        public int GetDamage(EquipmentCategory category, Stat modifier)
        {
            int modifierStat = GetTotalStat(modifier);
            int minDamage = 0;
            int maxDamage = 0;

            ActualEquippableItem weapon = Equipment.GetItem(category);

            if (weapon.ItemReferense is WeaponItem)
            {
                WeaponItem wItem = weapon.ItemReferense as WeaponItem;
                minDamage = wItem.GetMinDamage() + modifierStat;
                maxDamage = wItem.GetMaxDamage() + modifierStat;
            }

            return UnityEngine.Random.Range(minDamage, maxDamage + 1);
        }

        public void AddModifier(Stat stat, int value)
        {
            if (modifiers.ContainsKey(stat))
            {
                modifiers[stat] += value;

                if (modifiers[stat] == 0)
                    modifiers.Remove(stat);
            }
            else
            {
                modifiers.Add(stat, value);
            }
        }

        public CharacterStat[] GetStats()
        {
            Dictionary<Stat, int>.KeyCollection keys = playerStats.Keys;
            CharacterStat[] statContainer = new CharacterStat[keys.Count];
            int index = 0;

            foreach (var key in keys)
            {
                statContainer[index] = new CharacterStat();
                statContainer[index].statType = key;
                statContainer[index].value = GetTotalStat(key);
                index++;
            }

            return statContainer;
        }
    }
}