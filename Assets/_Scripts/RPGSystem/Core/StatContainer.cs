﻿namespace RPG.Core
{
    public interface StatContainer
    {
        CharacterStat[] Stats { get; }
    }
}