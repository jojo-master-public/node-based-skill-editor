﻿namespace RPG.Core
{
    using RPG.Damage;
    using System;

    public class PlayerContainer : GameEntity
    {
        public event Action<Player> onPlayerInited;
        public event Action<Player> onPlayerDied;

        public Player Player { get; private set; }
        public override CharacterStat[] Stats { get => Player.GetStats(); }



        public void Init(Player target)
        {
            Player = target;
            Init();
        }

        public override void Init()
        {
            base.Init();

            Health = new ObservableParameter(Player.GetTotalStat(Stat.VIT));
            Mana = new ObservableParameter(Player.GetTotalStat(Stat.INT));

            Health.onParameterChangedEvent += OnHealthChangedEvent;
            Mana.onParameterChangedEvent += OnManaChangedEvent;

            onPlayerInited?.Invoke(this.Player);
        }

        protected override void OnDestroyed()
        {
            onPlayerDied?.Invoke(Player);
        }
    }
}