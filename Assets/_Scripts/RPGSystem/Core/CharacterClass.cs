﻿namespace RPG.Core
{
    using Project.Skills;
    using RPG.Inventory;
    using System.Linq;
    using UnityEngine;

    [System.Serializable]
    public class CharacterStat
    {
        public Stat statType;
        public int value;
    }

    [CreateAssetMenu(menuName = "CharacterClass/New")]
    public class CharacterClass : ScriptableObject, StatContainer
    {
        [SerializeField] private string className;
        [TextArea(3, 6)]
        [SerializeField] private string classDesc;

        [SerializeField] private CharacterStat[] stats;
        [SerializeField] private ItemTemplate[] startingItems;
        [SerializeField] private SkillToButton[] startingSkills; 



        public ActualItem[] GetStartingItems()
        {
            ActualItem[] items = new ActualItem[startingItems.Length];

            for (int i = 0; i < items.Length; i++)
            {
                items[i] = ItemUtility.CreateItem(startingItems[i]);
            }

            return items;
        }

        public SkillToButton[] StartingSkills { get => startingSkills; }
        public CharacterStat[] Stats { get => stats; }
    }
}