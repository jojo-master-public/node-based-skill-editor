﻿namespace RPG.Core
{
    using System.Collections.Generic;
    using UnityEngine;
    
    public class GameEntity : MonoBehaviour, StatContainer
    {
        [SerializeField] protected CharacterStat[] stats;

        [SerializeField] private bool initOnAwake = true;
        [field: SerializeField] public bool IsDestructable { get; protected set; }

        protected Dictionary<Stat, int> statsDictionry;
        protected bool isDead = false;

        public ObservableParameter Health { get; protected set; }
        public ObservableParameter Mana { get; protected set; }



        public virtual void Init()
        {
            statsDictionry = new Dictionary<Stat, int>();

            for (int i = 0; i < stats.Length; i++)
            {
                statsDictionry.Add(stats[i].statType, stats[i].value);
            }

            if(statsDictionry.ContainsKey(Stat.VIT))
                Health = new ObservableParameter(statsDictionry[Stat.VIT]);
            else
                Health = new ObservableParameter(1);

            if(statsDictionry.ContainsKey(Stat.INT))
                Mana = new ObservableParameter(statsDictionry[Stat.INT]);
            else
                Mana = new ObservableParameter(1);

            Health.onParameterChangedEvent += OnHealthChangedEvent;
            Mana.onParameterChangedEvent += OnManaChangedEvent;
        }

        public int GetStat(Stat sType)
        {
            if (statsDictionry.ContainsKey(sType))
                return statsDictionry[sType];
            else
                return 0;
        }

        public virtual void DealDamage(int amount)
        {
            Health.Decrease(amount);
        }

        protected virtual void OnHealthChangedEvent(ParameterChangedEventArgs args)
        {
            if(args.newValue == 0 && !isDead)
            {
                isDead = true;
                OnDestroyed();
            }
        }

        protected virtual void OnManaChangedEvent(ParameterChangedEventArgs args)
        {
            // notify anyone that mana has changed if they need it!
        }

        protected virtual void OnDestroyed() { }

        public virtual CharacterStat[] Stats { get => stats; }

        protected virtual void Awake()
        {
            if (initOnAwake)
                Init();
        }
    }
}