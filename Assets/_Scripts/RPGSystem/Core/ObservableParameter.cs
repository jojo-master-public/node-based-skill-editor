﻿namespace RPG.Core
{
    using System;
    using UnityEngine;

    public class ParameterChangedEventArgs
    {
        public bool isDamage;
        public int max, oldValue, newValue;
    }

    public class ObservableParameter
    {
        public event Action<ParameterChangedEventArgs> onParameterChangedEvent;

        [SerializeField] private int valuePerStatMultiplier = 10;

        public int MaxValue { get; private set; }
        public int CurrentValue { get; private set; }



        public ObservableParameter (int targetStat)
        {
            MaxValue = valuePerStatMultiplier * targetStat;
            CurrentValue = MaxValue;
        }

        public void Decrease(int amount)
        {
            ChangeValue(-amount);
        }

        public void Increase(int amount)
        {
            ChangeValue(amount);
        }

        private void ChangeValue(int amount)
        {
            ParameterChangedEventArgs args = new ParameterChangedEventArgs();
            args.isDamage = amount < 0;
            args.max = MaxValue;
            args.oldValue = CurrentValue;

            CurrentValue = Mathf.Clamp(CurrentValue + amount, 0, MaxValue);
            args.newValue = CurrentValue;

            onParameterChangedEvent?.Invoke(args);
        }
    }
}