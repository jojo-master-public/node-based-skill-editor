﻿namespace RPG.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using RPG.Core;
    using TMPro;
    using UnityEngine.Playables;

    public class PlayerMenu : MonoBehaviour
    {
        public TMP_Text headerText;
        public MenuBase[] allMenus;
        public PlayableDirector timelineOpen;
        public PlayableDirector timelineClose;

        private int currentMenuIndex;



        public void InitPlayerMenu(Player target)
        {
            for (int i = 0; i < allMenus.Length; i++)
            {
                allMenus[i].InitMenu(target);
            }
        }

        private void OpenPlayerMenu()
        {
            //InputManager.Instance.ChangeInputState(InputState.Menu);
            timelineClose.Stop();
            timelineOpen.Play();
            ChangeMenu(0);
        }

        private void ClosePlayerMenu()
        {
            //InputManager.Instance.ChangeInputState(InputState.Gameplay);
            timelineClose.Play();
            timelineOpen.Stop();
            allMenus[currentMenuIndex].CloseMenu();
        }

        private void ChangeMenuLeft()
        {
            ChangeMenu(-1);
        }

        private void ChangeMenuRight()
        {
            ChangeMenu(1);
        }

        private void ChangeMenu(int direction)
        {
            allMenus[currentMenuIndex].CloseMenu();

            currentMenuIndex += direction;

            if (currentMenuIndex > allMenus.Length - 1)
                currentMenuIndex = 0;
            else if (currentMenuIndex < 0)
                currentMenuIndex = allMenus.Length - 1;

            allMenus[currentMenuIndex].OpenMenu();
            headerText.text = allMenus[currentMenuIndex].headerText;
        }

        private void OnPlayerCreated(int playerIndex, Player target)
        {
            InitPlayerMenu(target);
        }

        private void OnEnable()
        {
            //InputManager.onInventoryButtonPressedEvent += OpenPlayerMenu;
            //InputManager.onMenuSelectButtonPressedEvent += ClosePlayerMenu;
            //InputManager.onMenuButtonPageLeftPressedEvent += ChangeMenuLeft;
            //InputManager.onMenuButtonPageRightPressedEvent += ChangeMenuRight;

            //GameManager.onPlayerCreatedEvent += OnPlayerCreated;
        }

        private void OnDisable()
        {
            //InputManager.onInventoryButtonPressedEvent -= OpenPlayerMenu;
            //InputManager.onMenuSelectButtonPressedEvent -= ClosePlayerMenu;
            //InputManager.onMenuButtonPageLeftPressedEvent -= ChangeMenuLeft;
            //InputManager.onMenuButtonPageRightPressedEvent -= ChangeMenuRight;

            //GameManager.onPlayerCreatedEvent -= OnPlayerCreated;
        }
    }
}