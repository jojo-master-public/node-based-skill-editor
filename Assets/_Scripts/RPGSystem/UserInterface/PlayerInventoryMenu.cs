﻿namespace RPG.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using RPG.Core;
    using RPG.Equipment;
    using RPG.Inventory;
    using TMPro;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class PlayerInventoryMenu : MenuBase
    {
        public RectTransform inventoryRoot;
        public GameObject inventorySlotPrefab;

        public TMP_Text itemName;
        public TMP_Text itemDesc;
        public TMP_Text itemStats;

        private int selectedX = -1, selectedY = -1;
        private int cursorX, cursorY;
        private int inventorySizeX, inventorySizeY;
        private PlayerInventory targetInventory;
        private Selectable[,] inventoryUISlots;
        private ActualUsableItem selectedItem;



        #region OVERRIDES

        public override void InitMenu(Player player)
        {
            base.InitMenu(player);

            targetInventory = player.Inventory;
            player.Inventory.onInventoryChangedEvent += OnInventoryChanged;
            InitInventory(player.Inventory.InventorySlots);
        }

        public override void OpenMenu()
        {
            base.OpenMenu();

            selectedX = selectedY = -1;
            cursorX = cursorY = 0;
            OnItemSelected(cursorX, cursorY);
        }

        #endregion


        #region HANDLING INFO DISPLAY

        private void InitInventory(InventorySlot[,] newInventory)
        {
            inventorySizeX = newInventory.GetLength(0);
            inventorySizeY = newInventory.GetLength(1);

            inventoryUISlots = new Selectable[newInventory.GetLength(0), newInventory.GetLength(1)];

            int childCount = inventoryRoot.childCount;

            for (int i = 0; i < childCount; i++)
            {
                Destroy(inventoryRoot.GetChild(childCount - i - 1).gameObject);
            }

            for (int x = 0; x < newInventory.GetLength(0); x++)
            {
                for (int y = 0; y < newInventory.GetLength(1); y++)
                {
                    GameObject newSlot = Instantiate(inventorySlotPrefab, inventoryRoot);
                    newSlot.name = string.Format("[{0}]-[{1}:{2}]", inventorySlotPrefab.name, x, y);
                    newSlot.SetActive(true);

                    if (newInventory[x, y].IsTaken)
                    {
                        newSlot.GetComponent<Image>().sprite = newInventory[x, y].item.ItemReferense.GetSprite();
                    }

                    inventoryUISlots[x, y] = newSlot.GetComponent<Selectable>();
                }
            }
        }

        private void OnInventoryChanged(PlayerInventory target)
        {
            InitInventory(target.InventorySlots);
        }

        private void DisplayItemInfo(ActualItem targetItem)
        {
            if (targetItem == null || targetItem.ItemReferense == null)
            {
                itemName.text = "Nothing selected";
                itemDesc.text = string.Empty;
                itemStats.text = string.Empty;
            }
            else
            {
                if (targetItem.IsIdentified())
                {
                    ItemTemplate item = targetItem.ItemReferense;
                    StringBuilder statsString = new StringBuilder();

                    itemName.text = string.Format("{0} : {1}\n", "Name", item.GetName());
                    itemDesc.text = string.Format("{0} : {1}\n", "Desc", item.GetDesc());

                    if (item is EquippableItemBase)
                    {
                        if (item is WeaponItem)
                        {
                            WeaponItem weapon = item as WeaponItem;

                            statsString.AppendFormat("{0} : {1}-{2}\n", "Damage", weapon.GetMinDamage(), weapon.GetMaxDamage());
                        }

                        EquippableItemBase equipment = item as EquippableItemBase;

                        statsString.AppendFormat("{0} : {1}\n", "Item Type", equipment.GetCategory());

                        var modifiers = equipment.GetModifiers();

                        for (int i = 0; i < modifiers.Count; i++)
                        {
                            if (modifiers[i].value != 0)
                                statsString.AppendFormat("{0} : {1}\n", modifiers[i].statType, modifiers[i].value);
                        }
                    }

                    itemStats.text = statsString.ToString();
                }
                else
                {
                    itemName.text = "Unknown Item";
                    itemDesc.text = "Effects unknown";
                    itemStats.text = string.Empty;
                }
            }
        }

        private void OnItemSelected(int x, int y)
        {
            inventoryUISlots[x, y].Select();
            DisplayItemInfo(targetInventory.InventorySlots[x, y].item);
        }

        #endregion


        #region HANDLING ITEM USAGE

        private void SelectItem(ActualItem target)
        {
            selectedItem = target as ActualUsableItem;
        }

        private void DeselectItem()
        {
            selectedItem = null;
        }

        private void TryUseItem(ActualItem sender, ActualItem target = null)
        {
            if(sender is ActualEquippableItem)
            {
                targetPlayer.Equipment.EquipItem(sender as ActualEquippableItem);
            }

            if(sender is ActualUsableItem)
            {
                if(sender.ItemReferense is UsableItem)
                {
                    UsableItem usableItem = sender.ItemReferense as UsableItem;

                    if (usableItem.targetRequired)
                    {
                        if(selectedItem != null)
                        {
                            if (usableItem.UseItem(sender, target))
                            {
                                if (!sender.IsIdentified())
                                    sender.Identify();

                                targetInventory.RemoveItem(selectedX, selectedY);
                            }

                            DeselectItem();
                        }
                        else
                        {
                            SelectItem(sender);
                        }
                    }
                    else
                    {
                        if(usableItem.UseItem(targetPlayer, null))
                        {
                            if (!sender.IsIdentified())
                                sender.Identify();

                            targetInventory.RemoveItem(cursorX, cursorY);
                        }
                    }
                }
            }
        }

        #endregion


        #region HANDLING ITEM THROW

        private void ThrowItem()
        {
            InventorySlot slot = targetInventory.InventorySlots[cursorX, cursorY];

            if (slot.IsTaken)
            {
                if(slot.item is ActualEquippableItem)
                {
                    ActualEquippableItem equipment = slot.item as ActualEquippableItem;
                    EquipmentCategory category = equipment.GetCategory();

                    ActualEquippableItem equippedItem = targetPlayer.Equipment.GetItem(category);

                    if (equipment.Equals(equippedItem))
                        targetPlayer.Equipment.UnequipItem(category);
                }

                GameObject droppedItem = Instantiate(slot.item.ItemReferense.GetPrefab(), targetPlayer.PlayerObject.transform.position, Quaternion.identity);

                PickupableItem pickUp = droppedItem.GetComponent<PickupableItem>();
                pickUp.InitItem(slot.item);

                targetInventory.RemoveItem(cursorX, cursorY);

                OnItemSelected(cursorX, cursorY);   
            }
        }

        #endregion


        #region HANDLING INPUTS

        private void OnMoveButtonPressed()
        {
            if (!IsActive)
                return;

            if (selectedX == -1 || selectedY == -1)
            {
                // if there is an item in this slot
                if (!targetInventory[cursorX, cursorY].IsTaken)
                    return;

                // item is now selected
                selectedX = cursorX;
                selectedY = cursorY;
            }
            else
            {
                // Update item position
                targetInventory.MoveItem(selectedX, selectedY, cursorX, cursorY);
                OnItemSelected(cursorX, cursorY);
                // and deselect item
                selectedX = selectedY = -1;
            }
        }

        private void OnUseButtonPressed()
        {
            if (!IsActive)
                return;

            InventorySlot slot = targetInventory.InventorySlots[cursorX, cursorY];

            if (slot.IsTaken)
            {
                if (selectedItem == null)
                {
                    TryUseItem(slot.item);
                }
                else
                {
                    TryUseItem(selectedItem, slot.item);
                }

                selectedX = cursorX;
                selectedY = cursorY;

                OnItemSelected(cursorX, cursorY);
            }
        }

        private void OnThrowButtonPressed()
        {
            if (!IsActive)
                return;

            ThrowItem();
        }

        private void OnDownButtonPressed()
        {
            if (!IsActive)
                return;

            cursorX++;

            if (cursorX > inventorySizeX - 1)
                cursorX = 0;

            OnItemSelected(cursorX, cursorY);
        }

        private void OnUpButtonPressed()
        {
            if (!IsActive)
                return;

            cursorX--;

            if (cursorX < 0)
                cursorX = inventorySizeX - 1;

            OnItemSelected(cursorX, cursorY);
        }

        private void OnRightButtonPressed()
        {
            if (!IsActive)
                return;

            cursorY++;

            if (cursorY > inventorySizeY - 1)
                cursorY = 0;

            OnItemSelected(cursorX, cursorY);
        }

        private void OnLeftButtonPressed()
        {
            if (!IsActive)
                return;

            cursorY--;

            if (cursorY < 0)
                cursorY = inventorySizeY - 1;

            OnItemSelected(cursorX, cursorY);
        }

        #endregion


        #region EVENTS SUBS

        private void OnEnable()
        {
            // TODO: Change this to InputListener
            //InputManager.onMenuButtonLeftPressedEvent += OnLeftButtonPressed;
            //InputManager.onMenuButtonRightPressedEvent += OnRightButtonPressed;
            //InputManager.onMenuButtonUpPressedEvent += OnUpButtonPressed;
            //InputManager.onMenuButtonDownPressedEvent += OnDownButtonPressed;

            //InputManager.onMenuButtonConfirmPressedEvent += OnMoveButtonPressed;
            //InputManager.onMenuButtonUsePressedEvent += OnUseButtonPressed;
            //InputManager.onMenuButtonThrowPressedEvent += OnThrowButtonPressed;
        }

        private void OnDisable()
        {
            //InputManager.onMenuButtonLeftPressedEvent -= OnLeftButtonPressed;
            //InputManager.onMenuButtonRightPressedEvent -= OnRightButtonPressed;
            //InputManager.onMenuButtonUpPressedEvent -= OnUpButtonPressed;
            //InputManager.onMenuButtonDownPressedEvent -= OnDownButtonPressed;

            //InputManager.onMenuButtonConfirmPressedEvent -= OnMoveButtonPressed;
            //InputManager.onMenuButtonUsePressedEvent -= OnUseButtonPressed;
            //InputManager.onMenuButtonThrowPressedEvent -= OnThrowButtonPressed;
        }

        #endregion
    }
}