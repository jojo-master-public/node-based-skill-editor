﻿namespace RPG.UI
{
    using System;
    using System.Text;
    using RPG.Core;
    using TMPro;

    /// <summary>
    /// Displays current player total stats and stuff
    /// </summary>
    public class PlayerInfoMenu : MenuBase
    {
        public TMP_Text targetText;



        #region OVERRIDES

        public override void InitMenu(Player player)
        {
            base.InitMenu(player);
            UpdateInfo(player);
        }

        public override void OpenMenu()
        {
            UpdateInfo(targetPlayer);

            base.OpenMenu();
        }

        #endregion


        #region HANDLING INFO

        private void UpdateInfo(Player player)
        {
            StringBuilder builder = new StringBuilder();

            var allStats = Enum.GetNames(typeof(Stat));

            for (int i = 0; i < allStats.Length; i++)
            {
                Stat selected;
                if (Enum.TryParse(allStats[i], out selected))
                {
                    int statValue = player.GetTotalStat(selected);

                    builder.AppendLine($"{selected.ToString()} : {statValue}");
                }
            }

            targetText.text = builder.ToString();
        }

        #endregion
    }
}