﻿namespace RPG.Equipment
{
    using RPG.Core;
    using System.Collections.Generic;

    public interface IEquippableItem
    {
        EquipmentCategory GetCategory();
        List<CharacterStat> GetModifiers();
    }
}