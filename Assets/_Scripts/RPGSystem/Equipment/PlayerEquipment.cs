﻿namespace RPG.Equipment
{
    using System;
    using System.Collections.Generic;
    using RPG.Core;
    using RPG.Inventory;

    public class ItemEquippedEventArgs
    {
        public ActualEquippableItem itemEquipped;
        public ActualEquippableItem itemUnequipped;
    }

    public class PlayerEquipment
    {
        public event Action<ItemEquippedEventArgs> onItemEquipped;

        private Dictionary<EquipmentCategory, ActualEquippableItem> equipment;



        public PlayerEquipment(CharacterClass playerClass)
        {
            equipment = new Dictionary<EquipmentCategory, ActualEquippableItem>();

            var allStats = Enum.GetNames(typeof(EquipmentCategory));

            for (int i = 0; i < allStats.Length; i++)
            {
                EquipmentCategory selected;
                if (Enum.TryParse(allStats[i], out selected))
                {
                    equipment.Add(selected, null);
                }
            }
        }

        public void EquipItem(ActualEquippableItem newItem)
        {
            if(equipment[newItem.GetCategory()] != null && equipment[newItem.GetCategory()].Equals(newItem))
            {
                UnequipItem(newItem.GetCategory());
            }
            else
            {
                ItemEquippedEventArgs e = new ItemEquippedEventArgs();
                e.itemUnequipped = equipment[newItem.GetCategory()];
                e.itemEquipped = newItem;

                equipment[newItem.GetCategory()] = newItem;

                if (!newItem.IsIdentified())
                    newItem.Identify();

                onItemEquipped?.Invoke(e);
            }
        }

        public void UnequipItem(EquipmentCategory category)
        {
            ItemEquippedEventArgs e = new ItemEquippedEventArgs();
            e.itemUnequipped = equipment[category];

            equipment[category] = null;

            onItemEquipped?.Invoke(e);
        }

        public int GetEquipmentTotalStat(Stat statType)
        {
            int statTotal = 0;

            foreach (ActualEquippableItem item in equipment.Values)
            {
                if (item == null)
                    continue;

                statTotal += item.GetStat(statType);
            }

            return statTotal;
        }

        public ActualEquippableItem GetItem(EquipmentCategory category)
        {
            if (equipment.ContainsKey(category))
                return equipment[category];

            return null;
        }
    }
}