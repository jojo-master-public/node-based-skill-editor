﻿namespace RPG.Equipment
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using RPG.Core;
    using RPG.Inventory;

    public enum EquipmentCategory
    {
        WEAPON,
        HEADGEAR,
        ARMOR,
        BOOTS,
        GLOVES,
        RING,
        AMULET,
        BELT
    }

    [CreateAssetMenu(menuName = "Equipment/Equipment")]
    public class EquippableItemBase : ItemTemplate
    {
        [SerializeField] protected EquipmentCategory category;
        [SerializeField] protected CharacterStat[] baseModifiers;



        public EquipmentCategory GetCategory()
        {
            return category;
        }

        public List<CharacterStat> GetModifiers()
        {
            List<CharacterStat> allModifiers = new List<CharacterStat>();
            Dictionary<Stat, CharacterStat> tempDic = new Dictionary<Stat, CharacterStat>();

            var allStats = Enum.GetNames(typeof(Stat));

            for (int i = 0; i < allStats.Length; i++)
            {
                Stat selected;

                if (Enum.TryParse(allStats[i], out selected))
                {
                    CharacterStat newModifier = new CharacterStat();

                    newModifier.statType = selected;
                    newModifier.value = 0;

                    tempDic.Add(selected, newModifier);
                }
            }

            if (baseModifiers != null)
            {
                for (int i = 0; i < baseModifiers.Length; i++)
                {
                    tempDic[baseModifiers[i].statType].value += baseModifiers[i].value;
                }
            }

            allModifiers.AddRange(tempDic.Values);

            return allModifiers;
        }
    }
}