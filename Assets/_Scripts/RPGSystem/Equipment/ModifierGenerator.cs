﻿namespace RPG.Equipment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using RPG.Core;

    /// <summary>
    /// Class used to generate X amount of modifiers based on input
    /// </summary>
    public static class ModifierGenerator
    {
        public const int statCap = 4;

        public static List<CharacterStat> GenerateModifiers(int modifiersAmount, int luckAmount)
        {
            List<CharacterStat> modifiers = new List<CharacterStat>();

            var allStats = Enum.GetNames(typeof(Stat)).ToList();

            for (int i = 0; i < modifiersAmount; i++)
            {
                int randomIndex = UnityEngine.Random.Range(0, allStats.Count);

                Stat selected;

                if (Enum.TryParse(allStats[randomIndex], out selected))
                {
                    allStats.RemoveAt(randomIndex);

                    CharacterStat newModifier = new CharacterStat();

                    newModifier.statType = selected;
                    newModifier.value = GenerateStatValue(luckAmount);

                    modifiers.Add(newModifier);
                }
            }

            return modifiers;
        }

        private static int GenerateStatValue(int luckAmount)
        {
            int capValue = Mathf.Clamp(luckAmount, 0, statCap);

            List<int> possibleValues = new List<int>();

            possibleValues.Add(-1);

            for (int i = 1; i < capValue; i++)
            {
                int index = i;
                possibleValues.Add(index);
            }

            return possibleValues.GetRandom();
        }
    }
}