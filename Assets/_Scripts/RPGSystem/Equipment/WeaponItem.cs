﻿namespace RPG.Equipment
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Equipment/Weapon")]
    public class WeaponItem : EquippableItemBase
    {
        [SerializeField] protected int minDamage, maxDamage;
        [SerializeField] protected float attackSpeed;
        [SerializeField] protected float attackRange;



        public int GetMinDamage()
        {
            return minDamage;
        }

        public int GetMaxDamage()
        {
            return maxDamage;
        }

        public float GetAttackSpeed()
        {
            return attackSpeed;
        }

        public float GetAttackRange()
        {
            return attackRange;
        }
    }
}