namespace RPG.Damage
{

    using System.Collections.Generic;
    using UnityEngine;
    using System;
    using RPG.Core;

    public struct DamageEventArgs
    {
        public Vector3 position;
        public float areaOfEffect;
        public int damage;
        public GameEntity source;
        public GameEntity[] targets;
    }

    public class DamageCalculationSystem : MonoBehaviour
    {
        public static event Action<DamageEventArgs> onDamageEvent;

        [SerializeField] private GameObject debugImpactPrefab;

        private static Collider[] damageBuffer = new Collider[32];
        private static Dictionary<int, List<GameEntity>> damageGuids = new Dictionary<int, List<GameEntity>>();




        public static void SendDamageEvent(DamageEventArgs args)
        {
            onDamageEvent?.Invoke(args);

            //Debug.Log("Some damage happened!");
            //Debug.Log($"Source: {args.source}");
            //Debug.Log($"Target Count: {args.targets.Length}");
            //Debug.Log($"Position: {args.position}");
            //Debug.Log($"AoE: {args.areaOfEffect}");
            //Debug.Log($"Damage: {args.damage}");
        }

        public static void SendDamageEvent(DamageEventArgs args, string skillTreeGuid)
        {
            if (!damageGuids.ContainsKey(skillTreeGuid.GetHashCode()))
            {
                damageGuids.Add(skillTreeGuid.GetHashCode(), new List<GameEntity>());
            }

            foreach (var entity in args.targets)
            {
                if (!damageGuids[skillTreeGuid.GetHashCode()].Contains(entity))
                {
                    damageGuids[skillTreeGuid.GetHashCode()].Add(entity);

                    DamageEventArgs currentDamageArg = new DamageEventArgs();
                    currentDamageArg.source = args.source;
                    currentDamageArg.damage = args.damage;
                    currentDamageArg.areaOfEffect = args.areaOfEffect;
                    currentDamageArg.position = args.position;
                    currentDamageArg.targets = new GameEntity[] { entity };

                    onDamageEvent?.Invoke(currentDamageArg);
                }
            }
        }

        private void OnEnable()
        {
            onDamageEvent += OnDamageEvent;
        }

        private void OnDisable()
        {
            onDamageEvent -= OnDamageEvent;
        }

        private void OnDamageEvent(DamageEventArgs args)
        {
            foreach (var entity in args.targets)
            {
                GameobjectPoolSystem.Instantiate(debugImpactPrefab, entity.transform.position, Quaternion.identity);
            }
        }

        //public static void CheckDamagedEntities(ISkillBehaviour<ISkillInfo> sourceSkillBehaviour, ISkillInfo skillInfo, Vector3 position, float radius, int damage, OnDamageImpact damageCallback)
        //{
        //    for (int i = 0; i < damageBuffer.Length; i++)
        //    {
        //        damageBuffer[i] = null;
        //    }

        //    if (Physics.OverlapSphereNonAlloc(position, radius, damageBuffer, skillInfo.DamageMask, QueryTriggerInteraction.Ignore) > 0)
        //    {
        //        // Check
        //        for (int i = 0; i < damageBuffer.Length; i++)
        //        {
        //            if (damageBuffer[i] != null)
        //            {
        //                GameEntity hitEntity = damageBuffer[i].GetComponent<GameEntity>();

        //                if (hitEntity != null)
        //                {
        //                    DamageImpactArgs damageArgs = new DamageImpactArgs
        //                    {
        //                        source = sourceSkillBehaviour,
        //                        impactRadius = radius,
        //                        position = position,
        //                        target = hitEntity,
        //                        damage = damage
        //                    };

        //                    damageCallback?.Invoke(damageArgs);
        //                }
        //            }
        //            else
        //                break;
        //        }
        //    }
        //}

        //private void DebugDrawRay(Vector3 position, Vector3 direction)
        //{
        //    Debug.DrawRay(position, direction, Color.green, 3);
        //}

        //private void DebugDrawSphere(Vector3 position, float scale)
        //{
        //    GameObject impact = GameobjectPoolSystem.Instantiate(debugImpactPrefab, position, Quaternion.identity);
        //    impact.transform.localScale = Vector3.one * scale * 2;
        //}

        //private void MeleeComboSkillBehaviour_onMeleeImpact(DamageImpactArgs args)
        //{
        //    ProcessDamage(args);
        //    DebugDrawSphere(args.position, args.impactRadius);
        //}

        //private void RangedSkillBehaviour_onRangedImpact(DamageImpactArgs args)
        //{
        //    ProcessDamage(args);
        //    DebugDrawSphere(args.position, args.impactRadius);
        //}

        //private void ProcessDamage(DamageImpactArgs args)
        //{
        //    int damage = args.damage;
        //    GameEntity target = args.target;

        //    if (target.IsDestructable)
        //        target.DealDamage(damage);
        //}

        //private void OnEnable()
        //{
        //    MeleeComboSkillBehaviour.onMeleeImpact += MeleeComboSkillBehaviour_onMeleeImpact;
        //    RangedSkillBehaviour.onRangedImpact += RangedSkillBehaviour_onRangedImpact;
        //}

        //private void OnDisable()
        //{
        //    MeleeComboSkillBehaviour.onMeleeImpact -= MeleeComboSkillBehaviour_onMeleeImpact;
        //    RangedSkillBehaviour.onRangedImpact -= RangedSkillBehaviour_onRangedImpact;
        //}
    }
}