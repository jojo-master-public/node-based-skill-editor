using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using System.Linq;
using Project.SkillSystem.Utility;
using Node = Project.SkillSystem.Node;
using Project.SkillSystem;

public class SkillTreeView : GraphView
{
    public new class UxmlFactory : UxmlFactory<SkillTreeView, GraphView.UxmlTraits> { }

    public event Action<NodeView> onNodeSelected;

    // currently viewed tree
    public SkillTree Tree { get; private set; }

    private EditorWindow editor;



    public SkillTreeView()
    {
        // basic editor stuff
        Insert(0, new GridBackground());
        var contentZoomer = new ContentZoomer();
        contentZoomer.minScale = 0.15f;
        contentZoomer.referenceScale = 2f;
        contentZoomer.maxScale = 3f;
        this.AddManipulator(contentZoomer);
        this.AddManipulator(new ContentDragger());
        this.AddManipulator(new SelectionDragger());
        this.AddManipulator(new RectangleSelector());

        var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/USS/SkillTreeEditor.uss");
        styleSheets.Add(styleSheet);

        Undo.undoRedoPerformed += OnUndoRedo;

        RegisterCallback<KeyDownEvent>(HandleKeyboard);
        this.serializeGraphElements += CopyOperation;
        this.unserializeAndPaste += PasteOperation;
    }

    public void Init(EditorWindow editor)
    {
        this.editor = editor;
    }

    public NodeView FindNodeView(string nodeGuid)
    {
        return GetNodeByGuid(nodeGuid) as NodeView;
    }

    // populates a node triee with node views for a selected skill tree
    public void PopulateView(SkillTree tree)
    {
        this.Tree = tree;

        Label skillName = this.Q<Label>("skill-name");
        skillName.text = tree.name;

        var elements = graphElements.ToList();
        Node.onValidateNode -= OnNodeValidate;
        Node.onValidateNode += OnNodeValidate;

        graphViewChanged -= OnGraphViewChanged;
        DeleteElements(elements);
        graphViewChanged += OnGraphViewChanged;

        provider = ScriptableObject.CreateInstance<SkillTreeSearchProvider>();
        provider.Init(this, editor);

        tree.nodes.ForEach(CreateNodeView);

        tree.nodes.ForEach(n =>
        {
            NodeView parentView = FindNodeView(n.guid);

            if (n.executionPorts != null)
            {
                for (int i = 0; i < n.executionPorts.Count; i++)
                {
                    Dictionary<OutputInfo, List<OutputInfo>> outputsToRemove = new Dictionary<OutputInfo, List<OutputInfo>>();

                    n.executionPorts[i].outputs.ForEach(o =>
                    {
                        NodeView childView = FindNodeView(o.guid);

                        if (childView != null)
                        {
                            Edge edge = parentView.outputExecutionPorts[i].ConnectTo(childView.inputExecutionPort);
                            AddElement(edge);
                        }
                        else
                        {
                            outputsToRemove.Add(o, n.executionPorts[i].outputs);
                        }
                    });

                    foreach (var output in outputsToRemove)
                    {
                        output.Value.Remove(output.Key);
                    }
                }
            }

            if (n.dataPorts != null)
                for (int i = 0; i < n.dataPorts.Count; i++)
                {
                    Dictionary<OutputInfo, List<OutputInfo>> outputsToRemove = new Dictionary<OutputInfo, List<OutputInfo>>();

                    n.dataPorts[i].outputs.ForEach(o =>
                    {
                        NodeView childView = FindNodeView(o.guid);

                        if (childView != null)
                        {
                            if (childView.inputs.Count > o.index)
                            {
                                Edge edge = parentView.outputs[i].ConnectTo(childView.inputs[o.index]);
                                AddElement(edge);
                                childView.SetPortsConnectionState(childView.inputs, o.index, true);
                                childView.SetPortsConnectionState(childView.outputs, o.index, true);
                            }
                            else
                            {
                                outputsToRemove.Add(o, n.dataPorts[i].outputs);
                            }
                        }
                        else
                        {
                            Debug.LogError($"{n.name} contains output with non-existant guid");
                            outputsToRemove.Add(o, n.dataPorts[i].outputs);
                        }
                    });

                    foreach (var output in outputsToRemove)
                    {
                        output.Value.Remove(output.Key);
                    }
                }
        });

        tree.nodes.ForEach(n => GetNodeByGuid(n.guid).expanded = n.isExpanded);

        ShowGuids();
    }

    public void CreateNodeView(Node node)
    {
        NodeView nodeView = new NodeView(node);
        nodeView.OnNodeSelected = onNodeSelected;

        if (node is SubGraphNode)
            ((SubGraphNode)node).InitOutputPorts();

        AddElement(nodeView);
    }

    // updates nodes states (idle, running, finished running etc.)
    public void UpdateNodeStates(string containerGuid, string subTreeGuid)
    {
        nodes.ForEach(n =>
        {
            NodeView view = n as NodeView;

            if (view != null)
            {
                NodeData data = SkillTreeEditor.GetNodeDataByGuid(containerGuid, subTreeGuid, view.node.guid);

                if (data != null)
                    view.UpdateState(data);
            }
        });
    }

    private GraphViewChange OnGraphViewChanged(GraphViewChange args)
    {
        args.elementsToRemove?.ForEach(OnGraphElementRemoved);

        args.edgesToCreate?.ForEach(OnEdgeCreated);

        return args;
    }

    private void OnUndoRedo()
    {
        PopulateView(Tree);
        AssetDatabase.SaveAssets();
    }

    // ctrl+D (duplication) and spacebar (quick node search)
    private void HandleKeyboard(KeyDownEvent callback)
    {
        switch (callback.keyCode)
        {
            case KeyCode.D:
                if (!callback.ctrlKey)
                    return;

                DuplicateSelection(selection);
                break;
            case KeyCode.Space:
                SearchWindow.Open(new SearchWindowContext(Event.current.mousePosition), provider);
                break;
        }
    }

    // updates sub tree nodes inputs if node has been invalidated
    private void OnNodeValidate(Node node)
    {
        NodeView parentView = GetNodeByGuid(node.guid) as NodeView;

        if (parentView != null)
            parentView.RecreateInputPorts();

        PopulateView(Tree);
    }

    #region ELEMENTS CREATION

    public void CreateNode(Type a, Vector2 position)
    {
        var worldMousePosition = editor.rootVisualElement.ChangeCoordinatesTo(editor.rootVisualElement.parent, position - editor.position.position);
        var localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition) + new Vector2(150, 80);

        Node node = SkillTreeEditor.CreateNode(a, Tree, localMousePosition);
        CreateNodeView(node);
    }

    private T CreateNode<T>(Vector2 position) where T : Node
    {
        var worldMousePosition = editor.rootVisualElement.ChangeCoordinatesTo(editor.rootVisualElement.parent, position - editor.position.position);
        var localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition) + new Vector2(150, 80);

        T node = (T)SkillTreeEditor.CreateNode(typeof(T), Tree, localMousePosition);
        CreateNodeView(node);

        return node;
    }

    private void OnEdgeCreated(Edge edge)
    {
        NodeView parentView = edge.output.node as NodeView;
        NodeView childView = edge.input.node as NodeView;

        if (parentView.outputExecutionPorts.Contains(edge.output))
        {
            int outputPortIndex = -1;
            int inputPortIndex = 0;

            outputPortIndex = parentView.outputExecutionPorts.IndexOf(edge.output);
            //inputPortIndex = childView.inputs.IndexOf(edge.input);

            SkillTreeEditor.ConnectExecuteNodes(parentView.node, childView.node, outputPortIndex, inputPortIndex);
        }
        else
        {
            int outputPortIndex = -1;
            int inputPortIndex = -1;

            outputPortIndex = parentView.outputs.IndexOf(edge.output);
            inputPortIndex = childView.inputs.IndexOf(edge.input);

            SkillTreeEditor.ConnectDataNodes(parentView.node, childView.node, outputPortIndex, inputPortIndex);
            childView.SetPortsConnectionState(childView.inputs, inputPortIndex, true);
            childView.SetPortsConnectionState(childView.outputs, inputPortIndex, true);
        }
    }

    private void OnGraphElementRemoved(GraphElement elem)
    {
        NodeView nodeView = elem as NodeView;

        if (nodeView != null)
        {
            SkillTreeEditor.DeleteNode(nodeView.node, Tree);
        }

        Edge edge = elem as Edge;

        if (edge != null)
        {
            NodeView parentView = edge.output.node as NodeView;
            NodeView childView = edge.input.node as NodeView;

            if (parentView.outputExecutionPorts.Contains(edge.output))
            {
                int index = parentView.outputExecutionPorts.IndexOf(edge.output);

                SkillTreeEditor.DisconnectExecuteNode(parentView.node, childView.node, index);
            }
            else
            {
                int index = parentView.outputs.IndexOf(edge.output);
                int inputPortIndex = childView.inputs.IndexOf(edge.input);

                SkillTreeEditor.DisconnectDataNode(parentView.node, childView.node, index);
                childView.SetPortsConnectionState(childView.inputs, inputPortIndex, false);
                childView.SetPortsConnectionState(childView.outputs, inputPortIndex, false);
            }
        }
    }

    #endregion

    #region COPY/PASTE OPERATIONS

    private List<Node> nodesToCopy = new List<Node>();

    private void DuplicateSelection(List<ISelectable> selection)
    {
        List<Node> nodes = new List<Node>();

        selection.ForEach(x =>
        {
            if (x is NodeView)
                nodes.Add((x as NodeView).node);
        });

        PasteNodes(nodes);
    }

    private string CopyOperation(IEnumerable<GraphElement> elements)
    {
        nodesToCopy.Clear();
        foreach (GraphElement n in elements)
        {
            NodeView nodeView = n as NodeView;
            if (nodeView != null)
            {
                nodesToCopy.Add(nodeView.node);
            }
        }
        return "Copy Nodes";
    }

    private void PasteOperation(string operationName, string data)
    {
        if (operationName != "Paste")
            return;
        PasteNodes(nodesToCopy);
    }

    private void PasteNodes(List<Node> nodesToDuplicate)
    {
        List<string> clonedGuids = new List<string>();

        Dictionary<string, string> clonedGuidToOldGuid = new Dictionary<string, string>();
        Dictionary<string, string> oldGuidToClonedGuid = new Dictionary<string, string>();

        List<Node> duplicatedNodes = new List<Node>();

        foreach (Node originalNode in nodesToDuplicate)
        {
            Node clone = SkillTreeEditor.CreateNode(originalNode.GetType(), Tree, originalNode.position);
            clone.guid = GUID.Generate().ToString();
            clone.name = originalNode.name;
            clone.position = clone.position + new Vector2(20, -20);
            clone.isExpanded = originalNode.isExpanded;

            clonedGuidToOldGuid.Add(clone.guid, originalNode.guid);
            oldGuidToClonedGuid.Add(originalNode.guid, clone.guid);

            clonedGuids.Add(clone.guid);
            duplicatedNodes.Add(clone);
        }

        foreach (var node in duplicatedNodes)
        {
            Node originalNode = Tree.GetNodeByGuid(clonedGuidToOldGuid[node.guid]);

            if (originalNode == null)
                originalNode = nodesToDuplicate.First(x => x.guid == clonedGuidToOldGuid[node.guid]);

            if (originalNode.executionPorts != null)
                for (int i = 0; i < originalNode.executionPorts.Count; i++)
                {
                    for (int x = 0; x < originalNode.executionPorts[i].outputs.Count; x++)
                    {
                        if (oldGuidToClonedGuid.ContainsKey(originalNode.executionPorts[i].outputs[x].guid))
                            node.executionPorts[i]
                                .outputs
                                .Add(new OutputInfo(oldGuidToClonedGuid[originalNode.executionPorts[i].outputs[x].guid],
                                originalNode.executionPorts[i].outputs[x].index));
                    }
                }

            if (originalNode.dataPorts != null)
                for (int i = 0; i < originalNode.dataPorts.Count; i++)
                {
                    for (int x = 0; x < originalNode.dataPorts[i].outputs.Count; x++)
                    {
                        if (oldGuidToClonedGuid.ContainsKey(originalNode.dataPorts[i].outputs[x].guid))
                            node.dataPorts[i]
                                .outputs
                                .Add(new OutputInfo(oldGuidToClonedGuid[originalNode.dataPorts[i].outputs[x].guid],
                                originalNode.dataPorts[i].outputs[x].index));
                    }
                }
        }

        PopulateView(Tree);

        foreach (var guid in clonedGuids)
        {
            NodeView nv = GetNodeByGuid(guid) as NodeView;
            AddToSelection(nv);
        }
        nodesToCopy.Clear();
    }

    #endregion

    #region SEARCH FUNCTIONALITY

    private SkillTreeSearchProvider provider;

    public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
    {
        var allTypes = TypeCache.GetTypesDerivedFrom<Node>();
        var concreteType = allTypes.Where(t => !t.IsAbstract);

        foreach (var t in concreteType)
        {
            NodeInfoAttribute metadata = (NodeInfoAttribute)t.GetCustomAttributes(typeof(NodeInfoAttribute), true).First(x => x is NodeInfoAttribute);
            evt.menu.AppendAction($"{metadata.MenuName}", a => CreateNode(t, a.eventInfo.mousePosition));
        }

        var subTreeAssets = AssetDatabase.FindAssets("SubTree_");

        foreach (var subtreepath in subTreeAssets)
        {
            var subtree = AssetDatabase.LoadAssetAtPath<SubSkillTree>(AssetDatabase.GUIDToAssetPath(subtreepath));
            evt.menu.AppendAction($"SubTree/Nodes/{subtree.name.Substring(8)}", a =>
            {
                var node = CreateNode<SubGraphNode>(a.eventInfo.mousePosition);
                node.targetTree = subtree;
                PopulateView(Tree);
            });
        }
    }

    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
    {
        CustomNodeAdapter nda = new CustomNodeAdapter();

        var validPorts = ports
            .ToList()
            .Where(endPort =>
                endPort.direction != startPort.direction &&
                endPort.node != startPort.node &&
                ((nda.CanAdapt(startPort.source, endPort.source) && nda.Connect(startPort.source, endPort.source)) ||
                (endPort.portType == startPort.portType)))
                .ToList();

        return validPorts;
    }

    #endregion

    #region DEBUG

    private bool debugGuids;

    public void SetDebug(bool value)
    {
        debugGuids = value;
        ShowGuids();
    }

    private void ShowGuids()
    {
        nodes.ForEach(n =>
        {
            Label l = n.Q<Label>("guid");

            if (l != null)
            {
                if (debugGuids)
                    l.RemoveFromClassList("hidden");
                else
                {
                    l.RemoveFromClassList("hidden");
                    l.AddToClassList("hidden");
                }
            }
        });
    }

    #endregion
}