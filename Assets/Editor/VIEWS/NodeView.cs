using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Project.SkillSystem;
using Node = Project.SkillSystem.Node;

public class NodeView : UnityEditor.Experimental.GraphView.Node
{
    public Action<NodeView> OnNodeSelected;

    public Node node;

    public Port inputExecutionPort;
    public List<Port> outputExecutionPorts;

    public List<Port> inputs;
    public List<Port> outputs;

    private NodeInfoAttribute metadata;
    public override bool expanded 
    { 
        get => base.expanded;
        set
        {
            base.expanded = value;
            node.isExpanded = value;
        } 
    }



    public NodeView(Node node) : base("Assets/Editor/UXML/NodeView.uxml")
    {
        this.node = node;
        this.viewDataKey = node.guid;

        style.left = node.position.x;
        style.top = node.position.y;

        var customAttributes = node.GetType().GetCustomAttributes(typeof(NodeInfoAttribute), true);

        if (customAttributes.Any(x => x is NodeInfoAttribute))
            metadata = (NodeInfoAttribute)customAttributes.First(x => x is NodeInfoAttribute);

        CreateInputPorts();
        CreateOutputPorts();
        SetupClasses();

        this.title = metadata.NodeDisplayName;

        if(node is InputDataNode)
        {
            this.title = ((InputDataNode)node).inputName;
        }

        if(node is SubGraphNode)
        {
            SubGraphNode subNode = node as SubGraphNode;
            if(subNode.targetTree != null)
                this.title = ((SubGraphNode)node).targetTree.name.Substring(8);
        }

        VisualElement debugContainer = this.Q<VisualElement>("debug-container");
        if (debugContainer != null)
        {
            Label guidLabel = debugContainer.Q<Label>("guid");
            guidLabel.text = "guid: " + SkillTreeEditor.CropGuid(node.guid);
        }
    }

    public void UpdateState(NodeData data)
    {
        RemoveFromClassList("idle");
        RemoveFromClassList("running");
        RemoveFromClassList("success");
        RemoveFromClassList("error");

        if (Application.isPlaying)
        {
            switch (data.currentState)
            {
                case NodeData.State.WAITING:
                    AddToClassList("idle");
                    break;
                case NodeData.State.RUNNING:
                    AddToClassList("running");
                    break;
                case NodeData.State.FINISHED:
                    AddToClassList("success");
                    break;
                case NodeData.State.ERROR:
                    AddToClassList("error");
                    break;
            }
        }
    }

    public override void SetPosition(Rect newPos)
    {
        base.SetPosition(newPos);

        Undo.RecordObject(node, "SkillTree (Set Position)");
        node.position.x = newPos.xMin;
        node.position.y = newPos.yMin;

        EditorUtility.SetDirty(node);
    }

    public override void OnSelected()
    {
        base.OnSelected();
        OnNodeSelected?.Invoke(this);
    }

    public void SetPortsConnectionState(List<Port> ports, int portIndex, bool isHidden)
    {
        if (ports.Count <= portIndex)
            return;

        var children = ports[portIndex].Children();

        foreach (var child in children)
        {
            if (child is PropertyField)
            {
                if (isHidden)
                    child.AddToClassList("hidden-property-field");
                else
                    child.RemoveFromClassList("hidden-property-field");
            }
        }
    }

    public void RecreateInputPorts()
    {
        IEnumerable<VisualElement> inputPorts = inputContainer.Children();

        List<VisualElement> elementsToRemove = new List<VisualElement>();

        foreach (var item in inputPorts)
        {
            if (item is Port)
                elementsToRemove.Add(item);
        }

        foreach (var elem in elementsToRemove)
        {
            inputContainer.Remove(elem);
        }

        CreateInputPorts();

        //IEnumerable<VisualElement> outputPorts = outputContainer.Children();

        //foreach (var item in outputPorts)
        //{
        //    if (item is Port)
        //        Remove(item);
        //}
    }

    private void SetupClasses()
    {
        switch (metadata.Tag)
        {
            case "variable":
                AddToClassList("variable-node");
                break;
            case "execute":
                AddToClassList("execute-node");
                break;
            case "input":
                AddToClassList("input-node");
                break;
            case "sub-graph":
                AddToClassList("sub-graph-node");
                break;
        }
    }

    private void CreateInputPorts()
    {
        var nodeInfo = (NodeInfoAttribute)node.GetType().GetCustomAttribute(typeof(NodeInfoAttribute), true);

        if (nodeInfo.InputExecution)
        {
            inputExecutionPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, null);
            inputExecutionPort.portName = "input";
            inputExecutionPort.portColor = Color.red;

            inputContainer.Add(inputExecutionPort);
        }

        inputs = new List<Port>();
        node.GetInputInfo(out int[] index, out PortInfo[] portInfo);

        for (int i = 0; i < index.Length; i++)
        {
            if (portInfo.Length <= i)
                continue;

            Port p = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, portInfo[i].type);

            SerializedObject target = new SerializedObject(node);

            SerializedProperty property = target.FindProperty(portInfo[i].fieldName);

            if (property != null)
            {
                PropertyField field = new PropertyField(property, "");
                field.AddToClassList("input-data-port-field");
                field.Bind(target);
                p.Add(field);
            }

            p.portName = portInfo[i].displayName;
            inputs.Add(p);
        }

        if (inputs.Count > 0)
        {
            for (int i = 0; i < inputs.Count; i++)
            {
                inputContainer.Add(inputs[i]);
            }
        }
        else if (!nodeInfo.InputExecution)
        {
            inputContainer.parent.RemoveFromClassList("align-items-right");
            inputContainer.parent.AddToClassList("align-items-right");
        }
    }

    private void CreateOutputPorts()
    {
        var nodeInfo = (NodeInfoAttribute)node.GetType().GetCustomAttribute(typeof(NodeInfoAttribute), true);

        outputExecutionPorts = new List<Port>();

        if (nodeInfo.OutputExecution && node.executionPorts != null)
        {
            for (int i = 0; i < node.executionPorts.Count; i++)
            {
                node.GetOutputExecutionInfo(i, out PortInfo portInfo);
                Port p = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, null);
                p.portName = portInfo.displayName;
                p.portColor = Color.red;

                outputExecutionPorts.Add(p);
                outputContainer.Add(p);
            }
        }

        outputs = new List<Port>();

        if (node.dataPorts != null)
            for (int i = 0; i < node.dataPorts.Count; i++)
            {
                node.GetOutputInfo(i, out PortInfo portInfo);
                Port p = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, portInfo.type);

                SerializedObject target = new SerializedObject(node);

                SerializedProperty property = target.FindProperty(portInfo.fieldName);

                if (property != null)
                {
                    PropertyField field = new PropertyField(property, "");
                    field.AddToClassList("output-data-port-field");
                    field.Bind(target);
                    p.Add(field);
                }

                p.portName = portInfo.displayName;
                outputs.Add(p);
                outputContainer.Add(p);
            }
    }

    public void ToggleNodeExpandedState()
    {
        node.isExpanded = !node.isExpanded;
    }

    private VisualElement GetInputField(Type t)
    {
        Assembly ass = AppDomain.CurrentDomain.GetAssemblies().First(x => x.FullName.Contains("UnityEditor.UIElements"));

        Type[] types = ass.GetTypes();
        Type targetType = null;

        if (types.Any(x => x.Name.ToLower().Contains(t + "field")))
            targetType = types.First(m => m.Name.ToLower().Contains(t + "field"));

        if (targetType != null)
            return (VisualElement)Activator.CreateInstance(targetType, null);

        return new ObjectField();
    }
}