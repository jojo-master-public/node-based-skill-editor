using UnityEditor;
using UnityEngine.UIElements;

public class InspectorView : VisualElement
{
    public new class UxmlFactory : UxmlFactory<InspectorView, VisualElement.UxmlTraits> { }

    private Editor editor;



    public InspectorView() { }

    public void UpdateSelection(NodeView selectedNode)
    {
        Clear();

        UnityEngine.Object.DestroyImmediate(editor);
        editor = Editor.CreateEditor(selectedNode.node);

        IMGUIContainer container = new IMGUIContainer(() =>
        {
            if (editor.target != null)
                editor.OnInspectorGUI();
        });
        Add(container);
    }
}
