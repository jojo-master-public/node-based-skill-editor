// GENERATED AUTOMATICALLY FROM 'Assets/_Settings/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""PlayerControls"",
            ""id"": ""44fd8e77-89c8-49be-a370-231bf9b662e4"",
            ""actions"": [
                {
                    ""name"": ""MovementInput"",
                    ""type"": ""Value"",
                    ""id"": ""13c70bef-0356-4971-93a0-6a71d0c790bd"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Item_1"",
                    ""type"": ""Button"",
                    ""id"": ""2d9e6479-77f4-4d82-a52d-0015489278a6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Item_2"",
                    ""type"": ""Button"",
                    ""id"": ""72a110f4-1304-4c64-9968-4719bc1503d2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Item_3"",
                    ""type"": ""Button"",
                    ""id"": ""20833b78-aba4-4323-83b9-dda76a6b6bd6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Item_4"",
                    ""type"": ""Button"",
                    ""id"": ""62813ddf-0c49-4c14-bcda-f8f33030a818"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_1"",
                    ""type"": ""Button"",
                    ""id"": ""b361bd4c-5856-48ef-9db8-2a60185522a4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_2"",
                    ""type"": ""Button"",
                    ""id"": ""4f4154d4-ac55-43d1-baf4-a19775288080"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_3"",
                    ""type"": ""Button"",
                    ""id"": ""9771dd45-4f68-4348-9dee-dad774173504"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_4"",
                    ""type"": ""Button"",
                    ""id"": ""f0988fb4-7608-48a7-b842-251ddab8bea8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_5"",
                    ""type"": ""Button"",
                    ""id"": ""285c1447-9400-49ff-90da-bb4b58302ac2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_6"",
                    ""type"": ""Button"",
                    ""id"": ""09664d8d-eeed-4d61-9f9a-f3377056c586"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Action_7"",
                    ""type"": ""Button"",
                    ""id"": ""a5a9d34d-6294-46c9-b6d3-555b38e3188b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ViewInput"",
                    ""type"": ""Value"",
                    ""id"": ""5fb2280f-0ba8-46bf-8cf1-c47afecff275"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""93f7b571-a560-4b7c-bd72-49cb941c0d9e"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3c707f83-bdd3-4458-a58e-41c2bb821adf"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""39d2a497-f7a0-4a76-923b-91878b79fc68"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ad86eb89-29f2-4f9d-b847-191d8f68c0dc"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d69fa185-b17a-432a-ae94-045e7cb786e4"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Item_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2ef1c680-3e1b-4d4b-98ee-6bba57f6377b"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Item_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9fe79ff6-6648-4370-95ab-5b3e4a7a289a"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Item_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""128f5297-0d64-413c-87ca-db0cf73988dc"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Item_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f2b38592-676a-4e9f-861f-e68d275b9a66"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Item_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ff190d9-61e7-4287-b355-fe4fdf2fa9e3"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Item_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""994a4f4c-5dec-45fb-ba77-426fc62c983c"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Item_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4fc9df46-ae78-481e-9626-7f7b32876e4f"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Item_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0863dcd0-e0bd-4d42-aecf-1fd59d4c08af"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf44f57d-b100-4d0e-bb6c-c3923e80c9ec"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4bfaef13-3b2b-4c62-8ccf-fb94a29460e3"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_7"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""af8d6099-54d5-4303-9ac7-4282a05d8cee"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_7"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1f62f294-207f-4352-be7d-957ea8e2c293"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bd7c46da-54e2-4372-b60c-9b9e787d39d1"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""681824a3-e649-4f91-b80e-8106d91eb55a"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_5"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""281e340c-fb1a-4269-94f3-2baf8f1d8177"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_5"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9e9e7477-0501-4048-946c-58617bef8e82"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Action_6"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""15b70f59-7594-41d5-a92e-1b4910356ec7"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Action_6"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Input"",
                    ""id"": ""20e69598-6268-4871-8135-9e9508788ec6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovementInput"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""bc375fc3-ff3b-4b0b-99f9-b4acdf678e10"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MovementInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""543ced43-923c-4b81-a5d5-6faa5931946d"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MovementInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""322f1d3d-8118-442f-8f93-23be2b2f2d84"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MovementInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""77841bec-3b2a-4555-9508-a43f6d643d08"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""MovementInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""4a16eea7-beea-4933-9242-7d2125d29ba9"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""MovementInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4d2e2b7c-a014-4503-bc79-13fae6da1981"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""ViewInput"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // PlayerControls
        m_PlayerControls = asset.FindActionMap("PlayerControls", throwIfNotFound: true);
        m_PlayerControls_MovementInput = m_PlayerControls.FindAction("MovementInput", throwIfNotFound: true);
        m_PlayerControls_Item_1 = m_PlayerControls.FindAction("Item_1", throwIfNotFound: true);
        m_PlayerControls_Item_2 = m_PlayerControls.FindAction("Item_2", throwIfNotFound: true);
        m_PlayerControls_Item_3 = m_PlayerControls.FindAction("Item_3", throwIfNotFound: true);
        m_PlayerControls_Item_4 = m_PlayerControls.FindAction("Item_4", throwIfNotFound: true);
        m_PlayerControls_Action_1 = m_PlayerControls.FindAction("Action_1", throwIfNotFound: true);
        m_PlayerControls_Action_2 = m_PlayerControls.FindAction("Action_2", throwIfNotFound: true);
        m_PlayerControls_Action_3 = m_PlayerControls.FindAction("Action_3", throwIfNotFound: true);
        m_PlayerControls_Action_4 = m_PlayerControls.FindAction("Action_4", throwIfNotFound: true);
        m_PlayerControls_Action_5 = m_PlayerControls.FindAction("Action_5", throwIfNotFound: true);
        m_PlayerControls_Action_6 = m_PlayerControls.FindAction("Action_6", throwIfNotFound: true);
        m_PlayerControls_Action_7 = m_PlayerControls.FindAction("Action_7", throwIfNotFound: true);
        m_PlayerControls_ViewInput = m_PlayerControls.FindAction("ViewInput", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerControls
    private readonly InputActionMap m_PlayerControls;
    private IPlayerControlsActions m_PlayerControlsActionsCallbackInterface;
    private readonly InputAction m_PlayerControls_MovementInput;
    private readonly InputAction m_PlayerControls_Item_1;
    private readonly InputAction m_PlayerControls_Item_2;
    private readonly InputAction m_PlayerControls_Item_3;
    private readonly InputAction m_PlayerControls_Item_4;
    private readonly InputAction m_PlayerControls_Action_1;
    private readonly InputAction m_PlayerControls_Action_2;
    private readonly InputAction m_PlayerControls_Action_3;
    private readonly InputAction m_PlayerControls_Action_4;
    private readonly InputAction m_PlayerControls_Action_5;
    private readonly InputAction m_PlayerControls_Action_6;
    private readonly InputAction m_PlayerControls_Action_7;
    private readonly InputAction m_PlayerControls_ViewInput;
    public struct PlayerControlsActions
    {
        private @InputActions m_Wrapper;
        public PlayerControlsActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @MovementInput => m_Wrapper.m_PlayerControls_MovementInput;
        public InputAction @Item_1 => m_Wrapper.m_PlayerControls_Item_1;
        public InputAction @Item_2 => m_Wrapper.m_PlayerControls_Item_2;
        public InputAction @Item_3 => m_Wrapper.m_PlayerControls_Item_3;
        public InputAction @Item_4 => m_Wrapper.m_PlayerControls_Item_4;
        public InputAction @Action_1 => m_Wrapper.m_PlayerControls_Action_1;
        public InputAction @Action_2 => m_Wrapper.m_PlayerControls_Action_2;
        public InputAction @Action_3 => m_Wrapper.m_PlayerControls_Action_3;
        public InputAction @Action_4 => m_Wrapper.m_PlayerControls_Action_4;
        public InputAction @Action_5 => m_Wrapper.m_PlayerControls_Action_5;
        public InputAction @Action_6 => m_Wrapper.m_PlayerControls_Action_6;
        public InputAction @Action_7 => m_Wrapper.m_PlayerControls_Action_7;
        public InputAction @ViewInput => m_Wrapper.m_PlayerControls_ViewInput;
        public InputActionMap Get() { return m_Wrapper.m_PlayerControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerControlsActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerControlsActions instance)
        {
            if (m_Wrapper.m_PlayerControlsActionsCallbackInterface != null)
            {
                @MovementInput.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnMovementInput;
                @MovementInput.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnMovementInput;
                @MovementInput.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnMovementInput;
                @Item_1.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_1;
                @Item_1.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_1;
                @Item_1.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_1;
                @Item_2.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_2;
                @Item_2.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_2;
                @Item_2.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_2;
                @Item_3.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_3;
                @Item_3.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_3;
                @Item_3.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_3;
                @Item_4.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_4;
                @Item_4.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_4;
                @Item_4.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnItem_4;
                @Action_1.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_1;
                @Action_1.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_1;
                @Action_1.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_1;
                @Action_2.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_2;
                @Action_2.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_2;
                @Action_2.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_2;
                @Action_3.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_3;
                @Action_3.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_3;
                @Action_3.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_3;
                @Action_4.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_4;
                @Action_4.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_4;
                @Action_4.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_4;
                @Action_5.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_5;
                @Action_5.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_5;
                @Action_5.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_5;
                @Action_6.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_6;
                @Action_6.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_6;
                @Action_6.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_6;
                @Action_7.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_7;
                @Action_7.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_7;
                @Action_7.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnAction_7;
                @ViewInput.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnViewInput;
                @ViewInput.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnViewInput;
                @ViewInput.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnViewInput;
            }
            m_Wrapper.m_PlayerControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MovementInput.started += instance.OnMovementInput;
                @MovementInput.performed += instance.OnMovementInput;
                @MovementInput.canceled += instance.OnMovementInput;
                @Item_1.started += instance.OnItem_1;
                @Item_1.performed += instance.OnItem_1;
                @Item_1.canceled += instance.OnItem_1;
                @Item_2.started += instance.OnItem_2;
                @Item_2.performed += instance.OnItem_2;
                @Item_2.canceled += instance.OnItem_2;
                @Item_3.started += instance.OnItem_3;
                @Item_3.performed += instance.OnItem_3;
                @Item_3.canceled += instance.OnItem_3;
                @Item_4.started += instance.OnItem_4;
                @Item_4.performed += instance.OnItem_4;
                @Item_4.canceled += instance.OnItem_4;
                @Action_1.started += instance.OnAction_1;
                @Action_1.performed += instance.OnAction_1;
                @Action_1.canceled += instance.OnAction_1;
                @Action_2.started += instance.OnAction_2;
                @Action_2.performed += instance.OnAction_2;
                @Action_2.canceled += instance.OnAction_2;
                @Action_3.started += instance.OnAction_3;
                @Action_3.performed += instance.OnAction_3;
                @Action_3.canceled += instance.OnAction_3;
                @Action_4.started += instance.OnAction_4;
                @Action_4.performed += instance.OnAction_4;
                @Action_4.canceled += instance.OnAction_4;
                @Action_5.started += instance.OnAction_5;
                @Action_5.performed += instance.OnAction_5;
                @Action_5.canceled += instance.OnAction_5;
                @Action_6.started += instance.OnAction_6;
                @Action_6.performed += instance.OnAction_6;
                @Action_6.canceled += instance.OnAction_6;
                @Action_7.started += instance.OnAction_7;
                @Action_7.performed += instance.OnAction_7;
                @Action_7.canceled += instance.OnAction_7;
                @ViewInput.started += instance.OnViewInput;
                @ViewInput.performed += instance.OnViewInput;
                @ViewInput.canceled += instance.OnViewInput;
            }
        }
    }
    public PlayerControlsActions @PlayerControls => new PlayerControlsActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerControlsActions
    {
        void OnMovementInput(InputAction.CallbackContext context);
        void OnItem_1(InputAction.CallbackContext context);
        void OnItem_2(InputAction.CallbackContext context);
        void OnItem_3(InputAction.CallbackContext context);
        void OnItem_4(InputAction.CallbackContext context);
        void OnAction_1(InputAction.CallbackContext context);
        void OnAction_2(InputAction.CallbackContext context);
        void OnAction_3(InputAction.CallbackContext context);
        void OnAction_4(InputAction.CallbackContext context);
        void OnAction_5(InputAction.CallbackContext context);
        void OnAction_6(InputAction.CallbackContext context);
        void OnAction_7(InputAction.CallbackContext context);
        void OnViewInput(InputAction.CallbackContext context);
    }
}
