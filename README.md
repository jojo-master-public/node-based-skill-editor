### Node Based Skill System

This project is focused on implementing a node based skill system in Unity using GraphView (the node editor base that is used in Shader Graph).
The project is still Work In Progress and will take some time due to other projects taking up priorities.

Currently it looks like this

![Node Editor](/Previews/Preview_NodeEditor_00.png)

![Gameplay](/Previews/Preview_Gameplay_00.gif)
